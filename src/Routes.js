import React, { Component } from 'react';
import { AsyncStorage, View } from 'react-native';
import { Scene, Router, Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
// import {connect} from 'redux';
import Login from './Views/Login';
import AppIntro from './Views/Appintro';
import DaftarMasuk from './Views/DaftarMasuk';
import Daftar from './Views/Daftar';
import Verifikasi from './Views/Verifikasi';
import MenuUtama from './Views/MenuUtama';
import MenuAnak from './Views/MenuAnak';
//Menu Tabs Utama
import Home from './Views/Home';
import Notifikasi from './Views/TabsMama/Notifikasi';
import Pesanan from './Views/TabsMama/Pesanan';
import Tagihan from './Views/TabsMama/Tagihan';
import Profileanak from './Views/TabsAnak/Profileanak';

// Tabs Anak
import Aktivitas from './Views/TabsAnak/Aktifitas';
import Perkembangan from './Views/TabsAnak/Perkembangan';
// import Profilmama from './Views/TabsMama/Profilemama';

import Physic from './Views/TabsAnak/Perkembangan/Physic';
import SocialBahasa from './Views/TabsAnak/Perkembangan/SocialBahasa';
import Kognitif from './Views/TabsAnak/Perkembangan/Kognitif';
import TumbuhKembang from './Views/TabsAnak/Perkembangan/TumbuhKembang';
import DaycareTerdekat from './Views/DaycareTerdekat';
import DaycareTerdekatDetail from './Views/DaycareTerdekatDetail';
import DetailPembayaran from './Views/DetailPembayaran';
import DuniaAnak from './Views/DuniaAnak';
import DuniaAnakDetail from './Views/DuniaAnakDetail';
import SCAN from './Views/Scan';
import Toko from './Views/Toko';
import Tracking from './Views/Tracking';
import AlarmList from './Views/AlarmList';
import FormInputAnak from './Views/FormInputAnak';
import HistoricalMarker from './Views/HistoricalMarker';
import PesananHistori from './Views/PesananHistory';
import QrcodeUser from './Views/QrcodeUser';
import StatusTransaksi from './Views/StatusTransaksi';

// const GLOBAL = require('./config/Services');

const TabIcon = ({ selected, iconic }) => {
    return (
        <View>
            {
                selected ?
                <Icon name={iconic} size={25} color={'grey'}/>:
                <Icon name={iconic} size={25} color={'#294198'}/>
            }
        </View>
    )
}

class Routes extends Component {
    render() {
        return (
            <Router>
                <Scene key='auth'>
                    <Scene key='login' component={Login} hideNavBar={true} title="" />
                    <Scene key='menuutama' component={MenuUtama} hideNavBar={true} title="" />
                    <Scene key='appintro' component={AppIntro} hideNavBar={true} title="" />
                    <Scene key='daftarmasuk' component={DaftarMasuk} hideNavBar={true} title="" />
                    <Scene key='daftar' component={Daftar} hideNavBar={true} title="" />
                    <Scene key='verifikasi' component={Verifikasi} hideNavBar={true} title="" />
                    {/* <Scene key='menuutama' hideNavBar={true} tabs={true} tabBarStyle={{ backgroundColor: '#FFFFFF' }}>
                        <Scene key='Beranda' hideNavBar={true} iconic="home" icon={TabIcon}>
                            <Scene
                                key="Beranda"
                                component={Home}
                            />
                        </Scene>
                        <Scene key="Pesanan" hideNavBar={true} iconic="bell" icon={TabIcon}>
                            <Scene
                                key="Notifikasi"
                                component={Pesanan}
                            />
                        </Scene>
                        <Scene key="Profile" hideNavBar={true} iconic="user" icon={TabIcon}>
                            <Scene
                                key="Profile"
                                component={Profileanak}
                            />
                        </Scene>
                        <Scene key="Tagihan" hideNavBar={true} iconic="bantuan" icon={TabIcon}>
                            <Scene
                                key="Tagihan"
                                component={Tagihan}
                            />
                        </Scene>
                        <Scene key="Notifikasi" hideNavBar={true} iconic="bell" icon={TabIcon}>
                            <Scene
                                key="Notifikasi"
                                component={Notifikasi}
                            />
                        </Scene>
                    </Scene> */}
                    <Scene key='menuanak' component={MenuAnak} hideNavBar={true} title="" />
                    {/* scene mama */}
                    {/* <Scene key='home' component={Home} hideNavBar={true} title=""/>
                    <Scene key='pesanan' component={Pesanan} hideNavBar={true} title=""/>
                    <Scene key='tagihan' component={Tagihan} hideNavBar={true} title=""/>
                    <Scene key='notifikasi' component={Notifikasi} hideNavBar={true} title=""/>
                    <Scene key='profilemama' component={Profilmama} hideNavBar={true} title=""/> */}
                    {/* scene anak */}
                    <Scene key='aktifitas' component={Aktivitas} hideNavBar={true} title="" />
                    <Scene key='perkembangan' component={Perkembangan} hideNavBar={true} title="" />
                    <Scene key='profileanak' component={Profileanak} hideNavBar={true} title="" />
                    <Scene key='physic' component={Physic} hideNavBar={true} title="" />
                    <Scene key='sosialbahasa' component={SocialBahasa} hideNavBar={true} title="" />
                    <Scene key='kognitif' component={Kognitif} hideNavBar={true} title="" />
                    <Scene key='tumbuhkembang' component={TumbuhKembang} hideNavBar={true} title="" />
                    <Scene key='daycareterdekat' component={DaycareTerdekat} hideNavBar={true} title="" />
                    <Scene key='daycareterdekatdetail' component={DaycareTerdekatDetail} hideNavBar={true} title="" />
                    <Scene key='detailpembayaran' component={DetailPembayaran} hideNavBar={true} title="" />
                    <Scene key='duniaanak' component={DuniaAnak} hideNavBar={true} title="" />
                    <Scene key='duniaanakdetail' component={DuniaAnakDetail} hideNavBar={true} title="" />
                    <Scene key='scan' component={SCAN} hideNavBar={true} title="" />
                    <Scene key='qrcodeuser' component={QrcodeUser} hideNavBar={true} title="" />
                    <Scene key='toko' component={Toko} hideNavBar={true} title="" />
                    <Scene key='tracking' component={Tracking} hideNavBar={true} title="" />
                    <Scene key='alarmlist' component={AlarmList} hideNavBar={true} title="" />
                    <Scene key='forminputanak' component={FormInputAnak} hideNavBar={true} title="" />
                    <Scene key='historical' component={HistoricalMarker} hideNavBar={true} title="" />
                    {/* <Scene key='testmap' component={Testmap} hideNavBar={true} title="" /> */}
                    <Scene key='pesananhistory' component={PesananHistori} hideNavBar={true} title=""/>
                    <Scene key='statustransaksi' component={StatusTransaksi} hideNavBar={true} title=""/>
                </Scene>
            </Router>
        );
    }
}

export default Routes;