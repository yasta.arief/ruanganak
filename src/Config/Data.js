export default {
    data: [
        {
            title: 'SARAPAN',
            body: 'Nasi, Sayur Bayam, Sosis, Susu'
        },
        {
            title: 'BERMAIN',
            body: 'Bermain Bersama teman'
        },
        {
            title: 'BELAJAR',
            body: 'Membentuk Kelompok Belajar'
        },
        {
            title: 'TIDUR SIANG',
            body: 'Waktunya tidur siang'
        },
        {
            title: 'BERMAIN',
            body: 'Waktunya bermain bersama teman'
        }
    ]
}