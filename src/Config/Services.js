import DeviceInfo from 'react-native-device-info';
import { AsyncStorage } from 'react-native';
const MAIN_HTTP_URL = 'http://www.ruanganak.id:8090'; 
// const MAIN_HTTP_URL = 'http://10.107.94.103:8090';
const PORTAL = '/portal/v1'
const FAMILY = '/family/v1'
const DAYCARE = '/daycare/v1'
const MARKETPLACE = '/market-place/v1'
const GPS = '/gps/v1'
const MEDIA = '/media/v1'
const NOTIF = '/notif/v1'

module.exports = {
    IMEI_PHONE: DeviceInfo.getUniqueID(),
    /* Storage Key */
    APPUSERID: 'appuserid',
    APPUSERIDCHILD: 'appuseridchild',
    CHECKPAGE: 'checkpage',
    //PORTAL
    REGISTRASI_URL: MAIN_HTTP_URL+PORTAL+'/register',
    LOGIN_URL: MAIN_HTTP_URL+PORTAL+'/login',
    USERINFO_URL : MAIN_HTTP_URL+PORTAL+'/user-info',
    ACTIVATION_URL : MAIN_HTTP_URL+PORTAL+'/activation',
    FORGET_PASSWORD_URL : MAIN_HTTP_URL+PORTAL+'forget-password',
    //FAMILY
    ADDCHILD_URL: MAIN_HTTP_URL+FAMILY+'/addchild',
    EDITCHILD_URL: MAIN_HTTP_URL+FAMILY+'/editchild',
    EDITPARENT_URL: MAIN_HTTP_URL+FAMILY+'/editparent',
    BIODATA_PARENT_URL: MAIN_HTTP_URL+FAMILY+'/bio/parent',
    BIODATA_CHILD_URL: MAIN_HTTP_URL+FAMILY+'/bio/child',
    //DAYCARE
    NEARBY_DAYCARE_URL: MAIN_HTTP_URL+DAYCARE+'/nearby',
    AKTIVITAS_URL: MAIN_HTTP_URL+DAYCARE+'/activity',
    INVOICE_URL: MAIN_HTTP_URL+DAYCARE+'/invoice',
    DETAIL_INVOICE_URL: MAIN_HTTP_URL+DAYCARE+'/invoice/detail',
    PESANAN_URL: MAIN_HTTP_URL+DAYCARE+'/order/active',
    HISTORY_PESANAN_URL: MAIN_HTTP_URL+DAYCARE+'/order/history',
    DETAIL_DAYCARE_URL: MAIN_HTTP_URL+DAYCARE+'/detail',
    PAYMENT_DAYCARE_URL: MAIN_HTTP_URL+DAYCARE+'/detail'+'/payment',
    DEVELOPMENT_PHYSIC_URL : MAIN_HTTP_URL+DAYCARE+'/development/physic/',
    DEVELOPMENT_SOCIAL_URL : MAIN_HTTP_URL+DAYCARE+'/development/social/',
    DEVELOPMENT_COGNITIF_URL : MAIN_HTTP_URL+DAYCARE+'/development/cognitif/',
    DEVELOPMENT_GROWTH_URL : MAIN_HTTP_URL+DAYCARE+'/growth',
    ORDER_DAYCARE_URL : MAIN_HTTP_URL+DAYCARE+'/order',
    REVIEW_DAYCARE_URL : MAIN_HTTP_URL+DAYCARE+'/detail'+'/review',
    //GPS
    HISTORY_TRACKER_URL: MAIN_HTTP_URL+GPS+'/history',
    REGISTER_DEVICE_URL: MAIN_HTTP_URL+GPS+'/register-device',
    GPS_INFO_URL: MAIN_HTTP_URL+GPS+'/info',
    //MARKET PLACE
    MARKET_SHOP_URL: MAIN_HTTP_URL+MARKETPLACE+'/shop',
    MARKET_PLAYGROUND_URL: MAIN_HTTP_URL+MARKETPLACE+'/playground',
    // MEDIA
    UPLOAD_PHOTO_URL: MAIN_HTTP_URL+MEDIA+'/upload/photo-profile',
    GET_PHOTO_URL: MAIN_HTTP_URL+MEDIA+'/photo-profile',
    GET_PHOTO_MENU: MAIN_HTTP_URL+MEDIA+'/view/code',
    //NOTIF
    INBOX_URL: MAIN_HTTP_URL+NOTIF+'/inbox'
};