//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, ScrollView, AsyncStorage, ActivityIndicator, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import Selected_Items_Array from '../Components/Selected_Items_Array';
import SegmentedControlTab from "react-native-segmented-control-tab";
import { Actions } from 'react-native-router-flux';
import { thisExpression } from '@babel/types';
import Accounting from '../Components/Accounting';
import Modal from 'react-native-modal';
var moment = require('moment');
import DatePicker from 'react-native-date-picker';
import DateTimePicker from "react-native-modal-datetime-picker";
import { TextInput } from 'react-native-gesture-handler';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

const GLOBAL = require('../Config/Services');

var radio_props = [
    { label: 'Harian', value: 0, harga: 'Rp 100.000' },
    { label: 'Mingguan', value: 1, harga: 'Rp 600.000' },
    { label: 'Bulanan', value: 2, harga: 'Rp 2.500.000' },
];

// create a component
class DetailPembayaran extends Component {
    constructor(props) {
        super(props);
        selectedArrayOBJ = new Selected_Items_Array();
        this.state = {
            starCount: 0,
            selectedItems: '',
            prize: 0,
            selectedIndex: 0,
            data: [],
            date: new Date(),
            showDatePicker: false,
            tempIdParent: '',
            tempIdPrize: '',
            tempDate: '',
            isDateTimePickerVisible: false,
            tempDay:''
        };
    }
    getSelectedItems = () => {
        if (selectedArrayOBJ.getArray().length == 0) {
            // Alert.alert('No Items Selected!');
        }
        else {
            // console.log(selectedArrayOBJ.getArray().map(item => item.label).join());
            this.setState(() => {
                return {
                    selectedItems: selectedArrayOBJ.getArray().map(item => item.value).join()
                }
            });
        }
    }
    handleIndexChange = index => {
        this.setState({
            ...this.state,
            selectedIndex: index
        });
    };
    componentDidMount() {
        this.setState({
            tempDate:moment(this.state.date).format('YYYY-MMMM-DD')
        })
        this.funcGetUserid();
        this.getIdDaycare();
    }
    funcGetUserid() {
        AsyncStorage.getItem(GLOBAL.APPUSERID).then((value) => {
            var data = JSON.parse(value);
            this.funcUserinfo(data);
            this.setState({ tempIdParent: data });
        }).done();
    }
    funcUserinfo(getId) {
        this.setState({ isVisible: true });
        fetch(GLOBAL.USERINFO_URL, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': '0',
                'user-id': getId
                // 'UserCode': this.state.usercode
            },
        }).then((response) => response.json()).then((responseData) => {
            if (responseData.status == true) {
                this.setState({
                    isVisible: false,
                    tempIdChild: responseData.data.famiMstBioChildren[0].bioChildId
                });
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    getIdDaycare() {
        AsyncStorage.getItem('daycareId').then((value) => {
            getValue = JSON.parse(value);
            this.setState({
                getIdDaycare: getValue
            })
            this.funcGetData(getValue);
            // console.log('id ajah',getValue)
        }).done();
    }
    funcGetData(getValue) {
        fetch(GLOBAL.DETAIL_DAYCARE_URL + '/' + 'payment' + '/' + 'dc211aef-c42f-48f2-85bb-dfe44fa2acd2', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((response) => response.json()).then((responseData) => {
            if (responseData.status == true) {
                this.setState({
                    isVisible: false,
                    data: responseData.data
                });
                console.log('response data aj', responseData.data)
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    formatValue(value) {
        return Accounting.formatMoney(parseFloat(value), "Rp ", 0);
    }
    clickBack() {
        Actions.menuutama();
    }
    clickProsesPesanan() {
        if (this.state.tempIdPrize != '' || this.state.tempIdPrize != null && tempDateNew != '' || tempDateNew != null) {
            this.setState({ isVisible: true })
            var tempDateNew = moment(this.state.tempDate).format('YYYY-MM-DD HH:MM:SS')
            var tempDateSum = new Date(this.state.tempDate);
            var tempDateSum2 = tempDateSum.setDate(tempDateSum.getDate() + parseInt(this.state.tempDay));
            var tempDateSumFinal = moment(tempDateSum2).format('YYYY-MM-DD HH:MM:SS')
            // console.log('date', new Date(tempDateSum2))
            // console.log('date', moment(tempDateSum2).format('YYYY-MM-DD HH:MM:SS'))
            // console.log(this.state.getIdDaycare, this.state.tempIdChild, this.state.tempIdPrize, tempDateNew, this.state.tempDate);
            fetch(GLOBAL.ORDER_DAYCARE_URL, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'user-id': this.state.tempIdParent
                },
                body: JSON.stringify({
                    "daycareId": this.state.getIdDaycare,
                    "bioChildId": this.state.tempIdChild,
                    "priceId": this.state.tempIdPrize,
                    "orderStart": tempDateNew,
                    "orderEnd": tempDateSumFinal
                })
            }).then((response) => response.json()).then((responseData) => {
                // debugger
                if (responseData.status == true) {
                    console.log('hasil kirim pesan daycare', responseData)
                    this.setState({ isVisible: false });
                    Alert.alert(responseData.message);
                    Actions.menuutama({ getTemp: 'tagihan' });
                } else {
                    this.setState({ isVisible: false });
                    Alert.alert('Ruang Anak App', responseData.message);
                }
            }).done();
        } else {
            Alert.alert('Ruang Anak App', 'Data yang anda masukkan belum sempurna');
        }
    }
    radioClick(id, getPrize, getDay) {
        // if (id == null) {
        //     this.setState({
        //         radioSelected: id
        //     })
        // } else {
        //     this.setState({
        //         radioSelected: id
        //     })
        // }
        this.setState({
            radioSelected: id,
            prize: getPrize,
            tempIdPrize: id,
            tempDay:getDay
        })
    }
    radioButtonView() {
        return (
            this.state.data.map((val) => {
                return (
                    <View style={{ flexDirection: 'row', marginLeft: width / 20, marginTop: width / 30 }}>
                        <TouchableOpacity key={val.priceId}
                            onPress={this.radioClick.bind(this, val.priceId, val.priceAmount, val.configMstFrequency.freqAmount)}>
                            <View style={{
                                height: 24,
                                width: 24,
                                borderRadius: 12,
                                borderWidth: 2,
                                borderColor: '#A494EB',
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                                {
                                    val.priceId == this.state.radioSelected ?
                                        <View style={{
                                            height: 12,
                                            width: 12,
                                            borderRadius: 6,
                                            backgroundColor: '#A494EB',
                                        }} />
                                        : null
                                }
                            </View>
                        </TouchableOpacity>
                        <Text style={{ marginLeft: 10 }}>{val.priceName}</Text>
                    </View>
                )
            })
        )
    }
    getPicker() {
        try {
            return (
                <View style={{ backgroundColor: 'white', marginLeft: 15, marginRight: 15 }}>
                    <DatePicker
                        style={{ height: 150, color: 'white' }}
                        date={this.state.date} onDateChange={(date) => this.setState({ date })}
                        mode="date" />
                </View>
            )
        } catch {
            console.error(e.message);
        }
    }
    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };

    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    };

    handleDatePicked = date => {
        this.setState({
            tempDate: moment(date).format('YYYY-MMMM-DD')
        })
        // console.log("A date has been picked: ", date);
        this.hideDateTimePicker();
    };
    render() {
        var showDatePicker = this.state.showDatePicker ?
            this.getPicker()
            :
            <View />
        return (
            <View style={styles.container} >
                <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: width / 10 }}>
                        <View>
                            <TouchableOpacity onPress={() => this.clickBack()}>
                                <Icon name='arrow-left' size={25} color="white" />
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, color: 'white' }}>DETAIL PEMBAYARAN</Text>
                        </View>
                        <View>
                            <TouchableOpacity>
                                {/* <Image source={require('../../Assets/history.png')} /> */}
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <ScrollView>
                    <View>
                        <View
                            style={{
                                width: width / 1,
                                height: height / 2,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#A494EB",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                            }}>
                            <View style={{ marginTop: width / 40, marginLeft: width / 20 }}>
                                <Text style={{ color: '#FD8B7F', fontSize: 20 }}>Kapan Bunda Ingin Mulai ?</Text>
                            </View>
                            <View style={{ width: width / 1, height: 2, backgroundColor: '#dbdbdb', marginTop: width / 35 }} />
                            <View style={{ marginTop: width / 40, marginLeft: width / 20 }}>
                                <Text style={{ color: '#898989' }}>Pilih paket</Text>
                            </View>
                            {/* <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: Dimensions.get('window').width / 25 }}>
                                <RadioForm
                                    radio_props={this.state.data}
                                    initial={0}
                                    buttonSize={width/30}
                                    formHorizontal={true}
                                    onPress={(value) => { this.setState({ value: value }) }}
                                />
                            </View> */}
                            {/* <View>
                                <RadioForm
                                    formHorizontal={true}
                                    animation={true}>
                                    {this.state.data.map((obj, i) => {
                                        <RadioButton labelHorizontal={true} key={i}>
                                            <RadioButtonInput
                                                obj={obj}
                                                index={i}
                                                // isSelected={this.state.value3Index === i}
                                                // onPress={onPress}
                                                borderWidth={1}
                                                buttonInnerColor={'#e74c3c'}
                                                buttonOuterColor={this.state.value3Index === i ? '#2196f3' : '#000'}
                                                buttonSize={40}
                                                buttonOuterSize={80}
                                                buttonStyle={{}}
                                                buttonWrapStyle={{ marginLeft: 10 }} />
                                            <RadioButtonLabel
                                                obj={obj.priceName}
                                                index={i}
                                                labelHorizontal={true}
                                                // onPress={onPress}
                                                labelStyle={{ fontSize: 20, color: '#2ecc71' }}
                                                labelWrapStyle={{}} />
                                        </RadioButton>
                                    })}
                                </RadioForm>
                            </View> */}
                            <View style={{ flexDirection: 'row' }}>
                                {
                                    this.radioButtonView()
                                }
                            </View>
                            <View style={{ marginTop: width / 30, marginLeft: width / 20 }}>
                                <Text style={{ color: '#898989' }}>Tanggal Masuk</Text>
                                {/* <TouchableOpacity onPress={() => this.setState({ showDatePicker: !this.state.showDatePicker, tempDate: this.state.date })} style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', borderWidth: 2, borderColor: '#A494EB', width: width / 2.5, height: height / 20, marginTop: width / 40 }}>
                                    <Text style={{ color: '#FD8B7F' }}>{moment(this.state.date).format('YYYY-MMMM-DD')}</Text>
                                    <Icon name="caret-down" size={20} color={'#A494EB'} />
                                </TouchableOpacity> */}
                                <TouchableOpacity onPress={this.showDateTimePicker} style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', borderWidth: 2, borderColor: '#A494EB', width: width / 2.5, height: height / 18, marginTop:width/40 }}>
                                    <TextInput style={{ color: '#FD8B7F' }} editable={false} value={this.state.tempDate}/>
                                    <Icon name="caret-down" size={20} color={'#A494EB'} />
                                </TouchableOpacity>
                            </View>
                            <DateTimePicker
                                isVisible={this.state.isDateTimePickerVisible}
                                onConfirm={this.handleDatePicked}
                                onCancel={this.hideDateTimePicker}
                            />
                            {/* {showDatePicker} */}
                            <View style={{ marginTop: width / 30, marginLeft: width / 10 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ color: '#898989' }}>Biaya Paket : </Text>
                                    <Text style={{ color: '#898989' }}>{this.formatValue(this.state.prize)}</Text>
                                </View>
                                {/* <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ color: '#898989' }}>Biaya Tambahan : </Text>
                                    <Text style={{ color: '#898989' }}>Rp 0</Text>
                                </View> */}
                                <View style={{ marginTop: width / 50, height: 1, width: width / 1.5, backgroundColor: '#A494EB' }} />
                                <View style={{ flexDirection: 'row', marginTop: width / 40 }}>
                                    <Text style={{ fontSize: width / 25, color: '#FD8B7F' }}>TOTAL : </Text>
                                    <Text style={{ fontSize: width / 25, color: '#FD8B7F' }}>{this.formatValue(this.state.prize)}</Text>
                                </View>
                            </View>
                        </View>
                        <View
                            style={{
                                width: width / 1,
                                height: height / 6,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#A494EB",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                            }}>
                            <View style={{ marginTop: width / 40, marginLeft: width / 20 }}>
                                <Text style={{ color: '#FD8B7F', fontSize: 20 }}>Pilihan Pembayaran</Text>
                            </View>
                            <View style={{ width: width / 1, height: 2, backgroundColor: '#dbdbdb', marginTop: width / 35 }} />
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <View style={{ width: width / 1.5, marginTop: width / 45 }}>
                                    <SegmentedControlTab
                                        // values={["MY WALLET", "CASH"]}
                                        values={["CASH"]}
                                        selectedIndex={this.state.selectedIndex}
                                        onTabPress={this.handleIndexChange}
                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <View style={{ position: 'relative', bottom: width / 10, justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.clickProsesPesanan()} style={{ justifyContent: 'center', alignItems: 'center', backgroundColor: '#FD8B7F', width: width / 1.5, height: height / 15, borderRadius: width / 10 }}>
                        <Text style={{ color: 'white' }}>PROSES PESANAN</Text>
                    </TouchableOpacity>
                </View>
                {
                    this.state.isVisible == true ?
                        <Modal isVisible={this.state.isVisible}>
                            <ActivityIndicator size={25} color={"#A494EB"} />
                        </Modal>
                        :
                        <View />
                }
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default DetailPembayaran;
