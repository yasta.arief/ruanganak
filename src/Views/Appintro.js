//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, ImageBackground } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import { Actions } from 'react-native-router-flux';

const styles = StyleSheet.create({
    mainContent: {
        flex: 1,
    },
    image: {
        marginBottom: Dimensions.get('window').width / 1,
        width: Dimensions.get('window').width / 1.8,
        height: Dimensions.get('window').height / 2.5,
    },
    image2: {
        marginBottom: Dimensions.get('window').width / 1,
        width: Dimensions.get('window').width / 1.5,
        height: Dimensions.get('window').height / 2.45,
    },
    image3: {
        marginBottom: Dimensions.get('window').width / 1,
        width: Dimensions.get('window').width / 1.5,
        height: Dimensions.get('window').height / 2.3,
    },
    image4: {
        marginBottom: Dimensions.get('window').width / 1,
        width: Dimensions.get('window').width / 1.5,
        height: Dimensions.get('window').height / 2.3,
    },
    text: {
        color: 'black',
        backgroundColor: 'transparent',
        textAlign: 'center',
        paddingHorizontal: 10,
    },
    title: {
        fontSize: 22,
        color: 'black',
        backgroundColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 16,
    }
});

const slides = [
    {
        key: 'somethun',
        title: 'Hai, Ayah Bunda',
        title2: 'Selamat Datang Di Ruang Anak',
        text: 'Kami Menghadirkan Kemudahan Mencari Daycare',
        image: require('../Assets/AyahBunda1.png'),
        imageStyle: styles.image,
        backgroundColor: '#fffff8',
    },
    {
        key: 'somethun-dos',
        title: 'Hai, Ayah Bunda',
        title2: 'Ditengah Kesibukan, Ayah Bunda',
        text: 'Tetap Dapat Memantau Perkembangan Anak',
        image: require('../Assets/AyahBunda2.png'),
        imageStyle: styles.image2,
        backgroundColor: '#fffff8',
    },
    {
        key: 'somethun-des',
        title: 'Hai, Ayah Bunda',
        title2: 'Keamanan Anak Kami Utamakan',
        text: 'Pantau Dengan GPS dan Teknologi Terkini',
        image: require('../Assets/AyahBunda3.png'),
        imageStyle: styles.image3,
        backgroundColor: '#fffff8',
    },
    {
        key: 'somethun-des',
        title: 'Hai, Ayah Bunda',
        title2: 'Kami Berkomitmen Menjaga Privasi',
        text: 'Privasi Orangtua dan juga Anak',
        image: require('../Assets/AyahBunda4.png'),
        imageStyle: styles.image4,
        backgroundColor: '#fffff8',
    }
];

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default class AppIntro extends React.Component {
    _renderItem = props => (
        // <ImageBackground source={props.image} style={[styles.mainContent, {height:props.height, width:props.width}]}>
        //     <View style={{justifyContent:'center', alignItems:'center'}}>
        //         <Text style={{color:'white', fontSize:35, padding:60}}>Different recipes that will help you prepare your own.</Text>
        //     </View>
        // </ImageBackground>
        <View style={[styles.mainContent, { backgroundColor: props.backgroundColor, height: props.height, width: props.width }]}>
            <View style={{alignItems:'center', width:width/1}}>
                <Text style={{ color: '#A494EB', fontSize: width/15, paddingLeft: 60, paddingRight: 60, paddingTop: 80 }}>{props.title}</Text>
                <Text style={{ color: '#A494EB', fontSize: width/20, paddingLeft: 60, paddingRight: 60, paddingTop: 10 }}>{props.title2}</Text>
            </View>
            <ImageBackground source={require('../Assets/onboardBackground.png')} style={{ width: Dimensions.get('window').width / 1, height:Dimensions.get('window').height/2.4,position:'absolute', bottom:0 }}>
            <View style={{justifyContent:'center', alignContent:'center', alignItems:'center', marginBottom:Dimensions.get('window').width/3}}>
                <Image source={props.image} style={props.imageStyle} />
                <Text style={{ color: 'black', fontSize: 15, paddingLeft: 60, paddingRight: 60, marginTop: 20 }}>{props.text}</Text>
            </View>
            </ImageBackground>
        </View>
    );

    clickFirstPage() {
        Actions.daftarmasuk();
    }

    render() {
        return (
            <AppIntroSlider
                slides={slides}
                renderItem={this._renderItem}
                onDone={this.clickFirstPage}
            />
        );
    }
}
