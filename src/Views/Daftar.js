//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, ImageBackground, TextInput, ActivityIndicator, Alert, Picker, AsyncStorage } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
import Modal from 'react-native-modal';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Dropdown } from 'react-native-material-dropdown';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const GLOBAL = require('../Config/Services');
// create a component
class Daftar extends Component {
    constructor() {
        super();
        // this.onChangeText = this.onChangeText.bind(this);
        // this.nameRef = this.updateRef.bind(this, 'name');
        this.state = {
            tempUsername: '',
            tempUserfullname: '',
            tempUseremail: '',
            tempUserphone: '',
            tempUsergender: 'Hubungan anda',
            tempUserpin: '',
            isVisible: false
        }
    }
    clickBack() {
        Actions.daftarmasuk();
    }
    clickDaftar() {
        // Actions.verifikasi();
        if (this.state.tempUsername == '' && this.state.tempUseremail == '' && this.state.tempUserphone == '' && this.state.tempUserpin == '' && this.state.tempUserfullname == '' && this.state.tempUsergender == '') {
            Alert.alert('Ruang Anak App', 'Lengkapi data yang Kosong');
        } else {
            fetch(GLOBAL.REGISTRASI_URL, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': '0',
                    // 'UserCode': this.state.usercode
                },
                body: JSON.stringify({
                    'userName': this.state.tempUsername,
                    'userFullName': this.state.tempUserfullname,
                    "bioParentType" : this.state.tempUsergender,
                    'userEmail': this.state.tempUseremail,
                    'userPhone': this.state.tempUserphone,
                    'userPin': parseInt(this.state.tempUserpin)
                })
            }).then((response) => response.json()).then((responseData) => {
                if (responseData.status == true) {
                    this.setState({ isVisible: false });
                    AsyncStorage.setItem(GLOBAL.APPUSERID, JSON.stringify(responseData.data.userId));
                    Actions.verifikasi({getUserid:responseData.data.userId, getNohp:this.state.tempUserphone, getEmail:this.state.tempUseremail});
                    Alert.alert('Ruang Anak App', responseData.message);
                    // Alert.alert('Ruang Anak App', responseData.data.userId);
                } else {
                    this.setState({ isVisible: false });
                    Alert.alert('Ruang Anak App', responseData.message);
                }
            }).done();
        }
    }
    render() {
        let data = [{
            label: 'Ibu',
            value: '00000000-0000-0000-0000-000000000000',
        }, {
            label: 'Ayah',
            value: '00000000-0000-0000-0000-000000000001',
        }, {
            label: 'Baby Sister',
            value: '00000000-0000-0000-0000-000000000002',
        }, {
            label: 'Saudara',
            value: '00000000-0000-0000-0000-000000000003',
        }, {
            label: 'Sopir',
            value: '00000000-0000-0000-0000-000000000004',
        }];
        return (
            <View style={styles.container}>
                <KeyboardAwareScrollView style={{ flex: 1 }} behavior="padding">
                    <View style={{ width: width / 1, height: height / 1 }}>
                        <View style={{ backgroundColor: '#a163ff', width: width / 1, height: height / 8.5 }}>
                            <TouchableOpacity onPress={() => this.clickBack()} style={{ marginLeft: width / 15, marginTop: width / 15 }}>
                                <Icon name="arrow-left" size={30} color="white" />
                            </TouchableOpacity>
                        </View>
                        <View style={{ marginTop: width / 10, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
                            <Text style={{ color: 'white', fontSize: 20 }}>Daftar Sebagai Orang Tua</Text>
                            <View style={{ flexDirection: 'row', marginTop: width / 30, backgroundColor: 'white', borderRadius: 20, borderWidth: 1, borderColor: '#FD8B7F', width: width / 1.2, height: height / 15 }}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: width / 10, height: height / 20, backgroundColor: '#FD8B7F', borderRadius: 50, marginTop: width / 60, marginLeft: width / 30 }}>
                                    <Icon name="user" color="white" size={20} />
                                </View>
                                <View>
                                    <TextInput onChangeText={(text) => this.setState({ tempUsername: text })} style={{ color: '#FD8B7F', marginLeft: 30, fontSize:width/20 }} placeholder={'Username'} placeholderTextColor={'#FD8B7F'} />
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: width / 30, backgroundColor: 'white', borderRadius: 20, borderWidth: 1, borderColor: '#FD8B7F', width: width / 1.2, height: height / 15 }}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: width / 10, height: height / 20, backgroundColor: '#FD8B7F', borderRadius: 50, marginTop: width / 60, marginLeft: width / 30 }}>
                                    <Icon name="user" color="white" size={20} />
                                </View>
                                <View>
                                    <TextInput onChangeText={(text) => this.setState({ tempUserfullname: text })} style={{ color: '#FD8B7F', marginLeft: 30, fontSize:width/20 }} placeholder={'Nama Lengkap'} placeholderTextColor={'#FD8B7F'} />
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: width / 30, backgroundColor: 'white', borderRadius: 20, borderWidth: 1, borderColor: '#FD8B7F', width: width / 1.2, height: height / 15 }}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: width / 10, height: height / 20, backgroundColor: '#FD8B7F', borderRadius: 50, marginTop: width / 60, marginLeft: width / 30 }}>
                                    <Icon name="mobile" color="white" size={30} />
                                </View>
                                <View>
                                    <TextInput onChangeText={(text) => this.setState({ tempUserphone: text })} style={{ color: '#FD8B7F', marginLeft: 30, fontSize:width/20 }} placeholder={'Nomor Handphone'} placeholderTextColor={'#FD8B7F'} keyboardType={'phone-pad'} />
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: width / 30, backgroundColor: 'white', borderRadius: 20, borderWidth: 1, borderColor: '#FD8B7F', width: width / 1.2, height: height / 15 }}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: width / 10, height: height / 20, backgroundColor: '#FD8B7F', borderRadius: 50, marginTop: width / 60, marginLeft: width / 30 }}>
                                    <Icon name="envelope" color="white" size={20} />
                                </View>
                                <View>
                                    <TextInput onChangeText={(text) => this.setState({ tempUseremail: text })} style={{ color: '#FD8B7F', marginLeft: 30, fontSize:width/20 }} placeholder={'Email'} placeholderTextColor={'#FD8B7F'} keyboardType={'email-address'} />
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', width: width / 1.2, height:height/15, marginTop: width / 30, backgroundColor: 'white', borderRadius: 20, borderWidth: 1, borderColor: '#FD8B7F' }}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: width / 10, height: height / 20, backgroundColor: '#FD8B7F', borderRadius: 50, marginTop: width / 60, marginLeft: width / 30 }}>
                                    <Icon name="users" color="white" size={20} />
                                </View>
                                <View style={{width:width/1.8, justifyContent:'center', marginLeft:width/10.5}}>
                                    <Dropdown
                                        // ref={this.nameRef}
                                        value={this.state.tempUsergender}
                                        onChangeText={(text) => this.setState({ tempUsergender: text })}
                                        // label='Hubungan anda dengan anak'
                                        data={data}
                                        baseColor={'#FD8B7F'}
                                        textColor={'#FD8B7F'}
                                        itemColor={'#FD8B7F'}
                                        fontSize={width/20}
                                        inputContainerStyle={{ borderBottomColor: 'transparent' }}
                                    />
                                    <View style={{height:height/30}}/>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: width / 30, backgroundColor: 'white', borderRadius: 20, borderWidth: 1, borderColor: '#FD8B7F', width: width / 1.2, height: height / 15 }}>
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: width / 10, height: height / 20, backgroundColor: '#FD8B7F', borderRadius: 50, marginTop: width / 60, marginLeft: width / 30 }}>
                                    <Icon name="key" color="white" size={20} />
                                </View>
                                <View>
                                    <TextInput secureTextEntry={true} onChangeText={(text) => this.setState({ tempUserpin: text })} style={{ color: '#FD8B7F', marginLeft: 30, fontSize:width/20 }} placeholder={'PIN'} placeholderTextColor={'#FD8B7F'} keyboardType={'number-pad'} />
                                </View>
                            </View>
                            <TouchableOpacity onPress={() => this.clickDaftar()} style={{ borderRadius: 20, marginTop: width / 4.5, backgroundColor: '#FD8B7F', width: width / 1.2, height: height / 15, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: 'white', fontSize: 20 }}>DAFTAR</Text>
                            </TouchableOpacity>
                        </View>
                        <Modal isVisible={this.state.isVisible}>
                            <ActivityIndicator size="large" color="#0000ff" />
                        </Modal>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#A494EB',
    },
});

//make this component available to the app
export default Daftar;
