//import liraries
import React, { Component } from 'react';
import { Platform, View, Text, StyleSheet, Dimensions, TouchableOpacity, ImageBackground, TextInput, Alert, ScrollView, AsyncStorage, KeyboardAvoidingView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import SMSVerifyCode from 'react-native-sms-verifycode'
import { Actions } from 'react-native-router-flux';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const GLOBAL = require('../Config/Services');
// create a component
class Verifikasi extends Component {
    constructor(props) {
        super(props);
        this.state = { text: '', tempUserid:'' };
    }

    componentDidMount(){
        // this.funcGetUserid();
    }

    onInputCompleted = (text) => {
        
        // Alert.alert('response', this.props.getUserid)
    }

    // funcGetUserid(){
    //     AsyncStorage.getItem(GLOBAL.APPUSERID).then((value)=>{
    //         var data = JSON.parse(value);
    //         console.log(data);
    //         // this.funcUserinfo(data);
    //         this.setState({tempUserid:data});
    //     }).done();
    // }

    onInputChangeText = (text) => {
        this.setState({ text });
    }

    blur = () => this.verifycode.blur()

    reset = () => this.verifycode.reset()

    bindRef = (ref) => { this.verifycode = ref; }

    clickVerifikasi() {
        fetch(GLOBAL.ACTIVATION_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'user-id': this.props.getUserid
            },
            body: JSON.stringify({
                "activationCode" : parseInt(this.state.text)
            })
        }).then((response) => response.json()).then((responseData) => {
            if (responseData.status == true) {
                this.setState({ isVisible: false });
                Actions.login();
                Alert.alert('Ruang Anak App', responseData.message);
                console.log('data', responseData)
            } else {
                this.setState({ isVisible: false });
                Alert.alert('Ruang Anak App', responseData.message);
            }
        }).done();
    }
    render() {
        return (
            <View style={styles.container}>
                {/* <KeyboardAvoidingView behavior={'padding'}> */}
                    <ScrollView>
                <View style={{ width: width / 1, height: height / 1 }}>
                    <View style={{ backgroundColor: '#a163ff', width: width / 1, height: height / 8.5 }}>
                        {/* <TouchableOpacity style={{ marginLeft: width / 15, marginTop: width / 15 }}>
                            <Icon name="arrow-left" size={30} color="white" />
                        </TouchableOpacity> */}
                    </View>
                    <View style={{ marginTop: width / 10, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
                        <Text style={{ color: 'white', fontSize: 20 }}>Daftar Sebagai Orang Tua</Text>
                        <View style={{ marginTop: width / 15, justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: 'white', fontSize: 15 }}>Masukkan 4 Digit Kode Verifikasi</Text>
                            <Text style={{ color: 'white', fontSize: 15 }}>yang dikirimkan ke nomer</Text>
                            <Text style={{ color: 'white', fontSize: 15 }}>{this.props.getNohp} / email : {this.props.getEmail}</Text>
                        </View>
                        <View style={{ marginTop: 50 }}>
                            
                            <View style={styles.container2}>
                                <SMSVerifyCode
                                    ref={this.bindRef}
                                    verifyCodeLength={4}
                                    // containerStyle={}
                                    // containerPaddingVertical={30}
                                    containerPaddingBottom={50}
                                    // containerPaddingHorizontal={}
                                    containerPaddingLeft={50}
                                    containerPaddingRight={50}
                                    // containerBackgroundColor="blue"

                                    // codeViewStyle={}
                                    codeViewBorderColor="#FD8B7F"
                                    focusedCodeViewBorderColor="red"
                                    codeViewWidth={35}
                                    codeViewHeight={40}
                                    codeViewBorderWidth={2}
                                    codeViewBorderRadius={20}
                                    codeViewBackgroundColor="white"

                                    // codeStyle={}
                                    codeFontSize={20}
                                    codeColor="yellow"

                                    secureTextEntry
                                    // coverStyle={{width:width/1.2}}
                                    coverRadius={21}
                                    coverColor="black"

                                    onInputCompleted={this.onInputCompleted}
                                    onInputChangeText={this.onInputChangeText}

                                // warningTitle="haha"
                                // warningContent="no number"
                                // warningButtonText="okok"
                                />
                                {/* <TouchableOpacity
                                    onPress={this.reset}
                                    style={styles.button}
                                    activeOpcity={0}
                                >
                                    <Text style={styles.welcome}>Reset</Text>
                                </TouchableOpacity> */}
                            </View>
                        </View>
                        <TouchableOpacity onPress={() => this.clickVerifikasi()} style={{ borderRadius: 20, marginTop: width / 4.5, backgroundColor: '#FD8B7F', width: width / 1.2, height: height / 15, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: 'white', fontSize: 20 }}>VERIFIKASI</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                </ScrollView>
                {/* </KeyboardAvoidingView> */}
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#A494EB',
    },
    container2: {
        backgroundColor: '#FEFFFE',
        // paddingTop: Platform.OS === 'ios' ? 50 : 20,
        paddingTop: 50,
    },
    welcome: {
        color: '#191E24',
        fontSize: 25,
        textAlign: 'center',
        margin: 10,
    },
    button: {
        justifyContent: 'center',
        backgroundColor: '#1193F6',
        paddingHorizontal: 25,
        borderRadius: 10,
        marginTop: 40,
    },
    codeBorderStyle: {
        borderBottomColor: 'black',
        borderBottomWidth: 2,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderRadius: 0,
    },
});

//make this component available to the app
export default Verifikasi;
