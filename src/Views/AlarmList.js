//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, ScrollView } from 'react-native';
// import Icon from 'react-native-fontawesome';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

// create a component
class AlarmList extends Component {
    state = {
        aktivitas: [
            {
                id: 0,
                name: 'PANGGILAN DARURAT',
                image: '../Assets/call.png',
                tanggal: '12 April, 11:00 AM',
                deskripsi: 'Driver sedang menuju lokasimu untuk mengambil Asi'
            },
            {
                id: 1,
                name: 'PANGGILAN DARURAT',
                image: '../Assets/call.png',
                tanggal: '12 April, 11:00 AM',
                deskripsi: 'Driver sedang menjemputmu'
            },
            {
                id: 2,
                name: 'PANGGILAN DARURAT',
                image: '../Assets/call.png',
                tanggal: '12 April, 11:00 AM',
                deskripsi: 'Pesanan kamu sedang di proses'
            },
        ]
    }
    alertItemName = (item) => {
        alert(item.name)
    }
    clickBack() {
        Actions.tracking();
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
                    <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: width / 10 }}>
                            <View style={{ marginLeft: width / 30 }}>
                                <TouchableOpacity onPress={() => this.clickBack()}>
                                    <Icon name='arrow-left' size={25} color="white" />
                                </TouchableOpacity>
                            </View>
                            <View>
                                <Text style={{ fontSize: 20, color: 'white' }}>ALARM LIST</Text>
                            </View>
                            <View>
                                <TouchableOpacity>
                                    {/* <Image source={require('../../Assets/history.png')} /> */}
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
                <ScrollView>
                    <View>
                        {
                            this.state.aktivitas.map((item, index) => (
                                <View
                                    key={item.id}
                                    style={{
                                        width: width / 1,
                                        height: height / 7,
                                        marginTop: width / 20,
                                        backgroundColor: 'white',
                                        shadowColor: "#000",
                                        shadowOffset: {
                                            width: 0,
                                            height: 3,
                                        },
                                        shadowOpacity: 0.27,
                                        shadowRadius: 4.65,
                                        elevation: 6,
                                        flexDirection: 'row'
                                    }}
                                    onPress={() => this.alertItemName(item)}>
                                    <View style={{ marginLeft: width / 20 }}>
                                        <Image source={require('../Assets/call.png')} style={{ marginTop: width / 30 }} />
                                    </View>
                                    <View>
                                        <View style={{ flexDirection: 'row', marginTop: width / 30, marginLeft: width / 20 }}>
                                            <View>
                                                <Text style={{ fontSize: 15, color: '#A494EB' }}>{item.name}</Text>
                                            </View>
                                            <View style={{ marginLeft: width / 20 }}>
                                                <Text>{item.tanggal}</Text>
                                            </View>
                                        </View>
                                        <View style={{ width: width / 2, marginLeft: width / 20 }}>
                                            <Text numberOfLines={3}>{item.deskripsi}</Text>
                                        </View>
                                        {/* <View style={{ flexDirection: 'row', marginLeft: width / 20, marginTop: 10 }}>
                                            <TouchableOpacity style={{ flexDirection: 'row', backgroundColor: '#955BA5', width: width / 6, height: height / 35, justifyContent: 'center', alignItems: 'center', borderRadius: 15 }}>
                                                <Image source={require('../Assets/phone-call-putih.png')} />
                                                <Text style={{ color: 'white', marginLeft: 5 }}>CALL</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity style={{ marginLeft: 20, flexDirection: 'row', backgroundColor: '#FD8B7F', width: width / 6, height: height / 35, justifyContent: 'center', alignItems: 'center', borderRadius: 15 }}>
                                                <Image source={require('../Assets/chat-putih.png')} />
                                                <Text style={{ color: 'white', marginLeft: 5 }}>CHAT</Text>
                                            </TouchableOpacity>
                                        </View> */}
                                    </View>
                                </View>
                            ))
                        }
                    </View>
                </ScrollView>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default AlarmList;
