//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, ScrollView, AsyncStorage } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Drawer, Accordion } from 'native-base';
import { Actions } from 'react-native-router-flux';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const GLOBAL = require('../../Config/Services');

// create a component
class Perkembangan extends Component {
    constructor(){
        super();
        this.state = {
            tempIdParent:'',
            tempIdChild:''
        }
    }
    clickPhysic(){
        Actions.physic({getIdParent:this.state.tempIdParent, getIdChild:this.state.tempIdChild});
    }
    clickSocial(){
        Actions.sosialbahasa({getIdParent:this.state.tempIdParent, getIdChild:this.state.tempIdChild});
    }
    clickKognitif(){
        Actions.kognitif({getIdParent:this.state.tempIdParent, getIdChild:this.state.tempIdChild});
    }
    clickTumbuh(){
        Actions.tumbuhkembang({getIdParent:this.state.tempIdParent, getIdChild:this.state.tempIdChild});
    }
    componentDidMount(){
        this.funcGetUserid();
    }
    funcGetUserid(){
        AsyncStorage.getItem(GLOBAL.APPUSERID).then((value)=>{
            var data = JSON.parse(value);
            this.funcUserinfo(data);
            this.setState({tempIdParent:data});
            console.log(data)
        }).done();
    }
    funcUserinfo(getId){
        fetch(GLOBAL.USERINFO_URL, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization':'0',
                'user-id': getId
                // 'UserCode': this.state.usercode
            },
        }).then((response) => response.json()).then((responseData) => {
            console.log('respon userinfo perkembangan', responseData)
            if (responseData.status == true) {
                this.setState({
                    isVisible: false, 
                    tempIdChild: responseData.data.famiMstBioChildren[0].bioChildId
                });
                this.funcBiodataParent(responseData.data.famiMstBioChildren[0].bioChildId, getId);
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    funcBiodataParent(getIdChild, getIdParent) {
        fetch(GLOBAL.BIODATA_CHILD_URL + '/' + getIdChild, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'user-id': getIdParent
                // 'UserCode': this.state.usercode
            },
        }).then((response) => response.json()).then((responseData) => {
            console.log('respon anak', responseData)
            if (responseData.status == true) {
                this.setState({
                    tempNamaPanggilan: responseData.data.bioChildNickname,
                })
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={require('../../Assets/Header.png')} style={{ width: width / 1, height: height / 3.3 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 10 }}>
                        <Text style={{ marginTop: 10, color: 'white', fontSize: 15, fontWeight: '800' }}>PERKEMBANGAN ANAK</Text>
                        <Image source={{
                                uri: GLOBAL.GET_PHOTO_URL + '/' + this.state.tempIdChild,
                                method: 'GET',
                                headers: {
                                    'Content-type': 'application/x-www-form-urlencoded',
                                    'user-id': this.state.tempId
                                },
                                cache: 'reload'
                            }} style={{ width: width / 4.9, height: height / 8.5, borderRadius: width / 10, marginTop:10 }} />
                        <Text style={{ marginTop: 10, color: 'white', fontSize: 20, fontWeight: '800' }}>{this.state.tempNamaPanggilan}</Text>
                    </View>
                </ImageBackground>
                <View style={{ marginTop: width / 10 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                        <View style={{justifyContent:'center', alignItems:'center'}}>
                            <TouchableOpacity 
                                onPress={()=>this.clickPhysic()}
                                style={{
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 2,
                                },
                                shadowOpacity: 0.25,
                                shadowRadius: 3.84,
                                elevation: 5,
                                width: width / 4,
                                height: height / 8,
                                backgroundColor:'white',
                                justifyContent:'center',
                                alignItems:'center',
                                borderRadius:10
                            }}>
                                <Image source={require('../../Assets/physic.png')} />
                            </TouchableOpacity>
                            <Text style={{marginTop:10, color:'#FD8B7F', fontSize:15}}>PHYSIC</Text>
                        </View>
                        <View style={{justifyContent:'center', alignItems:'center'}}>
                            <TouchableOpacity 
                                onPress={()=>this.clickSocial()}
                                style={{
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 2,
                                },
                                shadowOpacity: 0.25,
                                shadowRadius: 3.84,
                                elevation: 5,
                                width: width / 4,
                                height: height / 8,
                                backgroundColor:'white',
                                justifyContent:'center',
                                alignItems:'center',
                                borderRadius:10
                            }}>
                                <Image source={require('../../Assets/social-bahasa.png')} />
                            </TouchableOpacity>
                            <Text style={{marginTop:10, color:'#FD8B7F', fontSize:15}}>BAHASA</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: width / 8 }}>
                    <View style={{justifyContent:'center', alignItems:'center'}}>
                            <TouchableOpacity 
                                onPress={()=>this.clickKognitif()}
                                style={{
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 2,
                                },
                                shadowOpacity: 0.25,
                                shadowRadius: 3.84,
                                elevation: 5,
                                width: width / 4,
                                height: height / 8,
                                backgroundColor:'white',
                                justifyContent:'center',
                                alignItems:'center',
                                borderRadius:10
                            }}>
                                <Image source={require('../../Assets/kognitif.png')} />
                            </TouchableOpacity>
                            <Text style={{marginTop:10, color:'#FD8B7F', fontSize:15}}>KOGNITIF</Text>
                        </View>
                        <View style={{justifyContent:'center', alignItems:'center'}}>
                            <TouchableOpacity 
                                onPress={()=>this.clickTumbuh()}
                                style={{
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 2,
                                },
                                shadowOpacity: 0.25,
                                shadowRadius: 3.84,
                                elevation: 5,
                                width: width / 4,
                                height: height / 8,
                                backgroundColor:'white',
                                justifyContent:'center',
                                alignItems:'center',
                                borderRadius:10
                            }}>
                                <Image source={require('../../Assets/tumbuhkembang.png')} />
                            </TouchableOpacity>
                            <Text style={{marginTop:10, color:'#FD8B7F', fontSize:15}}>TUMBUH</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
});

//make this component available to the app
export default Perkembangan;
