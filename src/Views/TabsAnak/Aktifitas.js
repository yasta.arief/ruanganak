import React, { Component } from 'react';
import { View, Text, StyleSheet, ListView, TouchableOpacity, Dimensions, Image, AsyncStorage } from 'react-native';
import StepIndicator from 'react-native-step-indicator';
import dummyData from '../../Config/Data';
import Icon from 'react-native-fontawesome';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const GLOBAL = require('../../Config/Services');

const stepIndicatorStyles = {
    stepIndicatorSize: 30,
    currentStepIndicatorSize: 40,
    separatorStrokeWidth: 3,
    currentStepStrokeWidth: 5,
    stepStrokeCurrentColor: '#fe7013',
    separatorFinishedColor: '#fe7013',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#fe7013',
    stepIndicatorUnFinishedColor: '#aaaaaa',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 15,
    currentStepIndicatorLabelFontSize: 15,
    stepIndicatorLabelCurrentColor: '#000000',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: 'rgba(255,255,255,0.5)',
    labelColor: '#666666',
    labelSize: 15,
    currentStepLabelColor: '#fe7013'
}
const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
export default class Aktifitas extends Component {
    constructor() {
        super();
        this.state = {
            dataSource: ds.cloneWithRows(dummyData.data),
            currentPage: 0
        };
    }
    componentDidMount() {
        this.funcGetUserid();
    }
    funcGetUserid() {
        AsyncStorage.getItem(GLOBAL.APPUSERID).then((value) => {
            var data = JSON.parse(value);
            this.funcUserinfo(data);
            // this.funcGetAktivitas(data);
            this.setState({ tempId: data });
        }).done();
    }
    funcUserinfo(getId) {
        fetch(GLOBAL.USERINFO_URL, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': '0',
                'user-id': getId.toString()
                // 'UserCode': this.state.usercode
            },
        }).then((response) => response.json()).then((responseData) => {
            console.log('respon aktivitas', responseData)
            if (responseData.status == true) {
                if (responseData.data.saldo != null) {
                    this.setState({
                        isVisible: false,
                        tempWallet: responseData.data.saldo,
                        tempPoint: responseData.data.point,
                        tempIdChild: responseData.data.famiMstBioChildren[0].bioChildId
                    });
                    this.funcGetAktivitas(responseData.data.famiMstBioChildren[0].bioChildId);
                } else {
                    this.setState({
                        tempWallet: 0,
                        tempPoint: 0
                    })
                }
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    funcGetAktivitas(getId) {
        // fetch(GLOBAL.AKTIVITAS_URL+'/'+this.state.tempIdChild, {
        fetch(GLOBAL.AKTIVITAS_URL + '/aefe2c1c-4fc3-4146-beab-ad563e8f699c', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'user-id': getId
                // 'UserCode': this.state.usercode
            },
        }).then((response) => response.json()).then((responseData) => {
            console.log('respon aktivitas anak', responseData)
            if (responseData.status == true) {
                this.setState({
                    dataSource: ds.cloneWithRows(responseData.data)
                })
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    render() {
        return (
            <View style={styles.container2}>
                <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: width / 10 }}>
                        <View>
                            <TouchableOpacity>
                                <Icon name="arrow-left" size={30} color="white" style={{ marginTop: 40, marginLeft: 10 }} />
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, color: 'white' }}>EVENT DAN AKTIFITAS</Text>
                        </View>
                        <View>
                            <TouchableOpacity>
                                {/* <Image source={require('../../Assets/history.png')} /> */}
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={styles.container}>
                    <View style={styles.stepIndicator}>
                        <StepIndicator
                            customStyles={stepIndicatorStyles}
                            stepCount={5}
                            direction='vertical'
                            currentPosition={this.state.currentPage}
                        // labels={dummyData.data.map(item => item.title)}
                        />
                    </View>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderRow={this.renderPage}
                        onChangeVisibleRows={this.getVisibleRows}
                    />
                </View>
            </View>
        );
    }

    renderPage = (rowData) => {
        return (
            <View style={styles.rowItem}>
                <View style={{ marginLeft: width / 20, marginTop: width / 30, height: height / 4 }}>
                    <Text style={styles.title}>{rowData.activityName}</Text>
                    <Text style={styles.body}>{rowData.activityDesc}</Text>
                    <Image source={{
                        uri: GLOBAL.GET_PHOTO_URL+'/'+rowData.activityPhoto,
                        method: 'GET',
                        headers: {
                            'Content-type': 'application/x-www-form-urlencoded',
                            'user-id': this.state.tempId
                        },
                        cache: 'reload'
                    }}
                        style={{ height: width / 5.5, width: width / 2, borderRadius: width / 12.5 }} />
                </View>
                <View style={{ height: 500 }}></View>
            </View>
        )
    }

    getVisibleRows = (visibleRows) => {
        const visibleRowNumbers = Object.keys(visibleRows.s1).map((row) => parseInt(row));
        this.setState({ currentPage: visibleRowNumbers[0] })
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#ffffff'
    },
    container2: {
        flex: 1,
    },
    stepIndicator: {
        marginVertical: 50,
        paddingHorizontal: 20
    },
    rowItem: {
        // height:200,
        marginLeft: width / 20,
        height: height / 1.3,
        width: width / 1.5,
        backgroundColor: 'white',
        marginTop: 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    title: {
        fontSize: 20,
        color: '#333333',
        paddingVertical: 16,
        fontWeight: '600'
    },
    body: {
        fontSize: 15,
        color: '#606060',
        lineHeight: 24,
        marginRight: 8,
        marginBottom:width/20
    }
});