//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, ScrollView, AsyncStorage, ActivityIndicator, TextInput, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Drawer, Accordion } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import DateTimePicker from "react-native-modal-datetime-picker";
import Modal from 'react-native-modal';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
var moment = require('moment');
import { Actions } from 'react-native-router-flux';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

const GLOBAL = require('../../Config/Services');

var radio_props = [
    { label: 'Laki-laki', value: 'L' },
    { label: 'Perempuan', value: 'P' }
];

// create a component
class Profileanak extends Component {
    constructor() {
        super();
        this.state = {
            photoAnak: false,
            tempNamaLengkap: '',
            tempNamaPanggilan: '',
            tempTanggalLahir: '',
            tempJenisKelamin: '',
            tempNamaAyah: '',
            tempNamaIbu: '',
            tempHeight: '',
            tempWeight: '',
            statusEditabel: false,
            isDateTimePickerVisible: false,
            isVisible: false
        }
    }
    componentDidMount() {
        this.funcGetUserid();
    }

    funcGetUserid() {
        AsyncStorage.getItem(GLOBAL.APPUSERID).then((value) => {
            var data = JSON.parse(value);
            this.funcUserinfo(data);
            this.setState({ tempId: data });
        }).done();
    }
    funcUserinfo(getId) {
        this.setState({ isVisible: true });
        fetch(GLOBAL.USERINFO_URL, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': '0',
                'user-id': getId
                // 'UserCode': this.state.usercode
            },
        }).then((response) => response.json()).then((responseData) => {
            if (responseData.status == true) {
                this.setState({
                    isVisible: false,
                    tempWallet: responseData.data.saldo,
                    child: responseData.data.child,
                    tempIdChild: responseData.data.famiMstBioChildren[0].bioChildId
                });
                if (responseData.data.child != null) {
                    this.funcBiodataParent(responseData.data.famiMstBioChildren[0].bioChildId);
                }
                // this.funcBiodataParent();
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    funcBiodataParent(getValue) {
        fetch(GLOBAL.BIODATA_CHILD_URL + '/' + getValue, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'user-id': this.state.tempId
                // 'UserCode': this.state.usercode
            },
        }).then((response) => response.json()).then((responseData) => {
            console.log('respon anak', responseData)
            if (responseData.status == true) {
                this.setState({
                    tempNamaPanggilan: responseData.data.bioChildNickname,
                    tempNamaLengkap: responseData.data.bioChildFullname,
                    tempTanggalLahir: responseData.data.bioChildDob,
                    tempJenisKelamin: responseData.data.bioChildGender,
                    tempHeight: responseData.data.bioChildHeight,
                    tempWeight: responseData.data.bioChildWeight
                })
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    selectImageTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
                path: 'images'
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled video picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                var photo = {
                    uri: response.uri,
                    type: 'image/jpg',
                    name: 'Ruanganak.jpg',
                };
                console.log('liat dong', this.state.tempId)
                var uploadForm = new FormData()
                uploadForm.append('file', photo);
                this.setState({ photoAnak: true })
                fetch(GLOBAL.UPLOAD_PHOTO_URL + '/' + this.state.tempIdChild, {
                    // fetch('http://ruanganak.id:8090/media/v1/upload/photo-profile',{
                    method: 'POST',
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        // 'Authorization': '0',
                        'user-id': this.state.tempId
                    },
                    body: uploadForm
                }).then((response) => response.json()).then((responseData) => {
                    console.log('respose data kirim gambar', responseData)
                    if (responseData.status == true) {
                        this.setState({ mediaTrxCode: responseData.data.fileId, photoAnak: false });
                        // Alert.alert('Ruang Anak App', responseData.message)
                    } else {
                        Alert.alert('Ruang Anak App', responseData.message)
                    }
                }).done();
            }
        });
    }

    clickEdit() {
        // Alert.alert('Hello')
        if (this.state.statusEditabel == false) {
            this.setState({
                statusEditabel: true
            })
        } else {
            this.setState({
                statusEditabel: false
            })
        }
    }

    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };

    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    };

    handleDatePicked = date => {
        this.setState({
            tempTanggalLahir: moment(date).format('YYYY-MM-DD')
        })
        // console.log("A date has been picked: ", date);
        this.hideDateTimePicker();
    };

    clickUpdate() {
        // this.setState({isVisible:true})
        console.log('id parent', this.state.tempId)
        console.log('id child', this.state.tempIdChild)
        fetch(GLOBAL.EDITCHILD_URL + '/' + this.state.tempIdChild, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'user-id': this.state.tempId
                // 'UserCode': this.state.usercode
            },
            body: JSON.stringify({
                // "bioChildHeight": this.state.tempHeight,
                // "bioChildWeight": this.state.tempWeight

                "bioChildDob": this.state.tempTanggalLahir,
                "bioChildFullname": this.state.tempNamaLengkapAnak,
                "bioChildGender": this.state.tempJenisKelamin,
                "bioChildHeight": this.state.tempHeight,
                // "bioChildId": "string",
                "bioChildNickname": this.state.tempNamaPanggilanAnak,
                // "bioChildPhotoProfile": "string",
                "bioChildWeight": this.state.tempWeight,
                // "bioParentId": "string"
            })
        }).then((response) => response.json()).then((responseData) => {
            if (responseData.status == true) {
                this.setState({ isVisible: false });
                Actions.menuutama();
                Alert.alert('Ruang Anak App', responseData.message);
            } else {
                this.setState({ isVisible: false });
                Alert.alert('Ruang Anak App', responseData.message);
            }
            console.log('response dong mas', responseData)
        }).done();
    }

    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={require('../../Assets/Header.png')} style={{ width: width / 1, height: height / 3.3 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 10 }}>
                        <Text style={{ marginTop: 10, color: 'white', fontSize: width / 30, fontWeight: '800' }}>PROFILE ANAK</Text>
                        {
                            this.state.statusEditabel == true ?
                                <View>
                                    <View
                                        style={{
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            marginBottom: 10,
                                            width: width / 5,
                                            height: width / 5,
                                            borderRadius: width / 10,
                                            backgroundColor: '#eaeaea',
                                            shadowColor: "#000",
                                            shadowOffset: {
                                                width: 0,
                                                height: 3,
                                            },
                                            shadowOpacity: 0.27,
                                            shadowRadius: 4.65,
                                            elevation: 6,
                                            marginTop: width / 20
                                        }}>
                                        {
                                            this.state.photoAnak == true ?
                                                <ActivityIndicator size={25} color={"#A494EB"} /> :
                                                <Image source={{
                                                    uri: GLOBAL.GET_PHOTO_URL + '/' + this.state.tempIdChild,
                                                    method: 'GET',
                                                    headers: {
                                                        'Content-type': 'application/x-www-form-urlencoded',
                                                        'user-id': this.state.tempId
                                                        // 'user-id':'1f0dff24-c3ca-46b4-9531-779e2e66ac7d'
                                                    },
                                                    // cache: 'reload'
                                                }} style={{ width: width / 4.9, height: height / 8.5, borderRadius: width / 10 }} />
                                        }
                                        {/* {
                                this.state.photoAnak == false ?
                                    <Icon name="plus" /> : <Image source={require('../../Assets/rara.png')} style={{ width: width / 4.9, height: height / 8.5, borderRadius: width / 10 }} />
                            } */}
                                        {/* <Text>Home</Text> */}
                                    </View>
                                    <TouchableOpacity
                                        onPress={() => this.selectImageTapped()}
                                        style={{
                                            position: 'absolute',
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            width: width / 12,
                                            height: width / 12,
                                            borderRadius: width / 10,
                                            backgroundColor: '#eaeaea',
                                            shadowColor: "#000",
                                            shadowOffset: {
                                                width: 0,
                                                height: 3,
                                            },
                                            shadowOpacity: 0.27,
                                            shadowRadius: 4.65,
                                            elevation: 6,
                                            top: width / 7,
                                            left: width / 7,
                                            backgroundColor: '#A494EB'
                                        }}>
                                        <Icon name="plus" color="white" size={15} />
                                    </TouchableOpacity>
                                </View> :
                                <View>
                                    <View
                                        style={{
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            marginBottom: 10,
                                            width: width / 5,
                                            height: width / 5,
                                            borderRadius: width / 10,
                                            backgroundColor: '#eaeaea',
                                            shadowColor: "#000",
                                            shadowOffset: {
                                                width: 0,
                                                height: 3,
                                            },
                                            shadowOpacity: 0.27,
                                            shadowRadius: 4.65,
                                            elevation: 6,
                                            marginTop: width / 20
                                        }}>
                                        {
                                            this.state.photoAnak == true ?
                                                <ActivityIndicator size={25} color={"#A494EB"} /> :
                                                <Image source={{
                                                    uri: GLOBAL.GET_PHOTO_URL + '/' + this.state.tempIdChild,
                                                    method: 'GET',
                                                    headers: {
                                                        'Content-type': 'application/x-www-form-urlencoded',
                                                        'user-id': this.state.tempId
                                                        // 'user-id':'1f0dff24-c3ca-46b4-9531-779e2e66ac7d'
                                                    },
                                                    // cache: 'reload'
                                                }} style={{ width: width / 4.9, height: height / 8.5, borderRadius: width / 10 }} />
                                        }
                                        {/* {
                                this.state.photoAnak == false ?
                                    <Icon name="plus" /> : <Image source={require('../../Assets/rara.png')} style={{ width: width / 4.9, height: height / 8.5, borderRadius: width / 10 }} />
                            } */}
                                        {/* <Text>Home</Text> */}
                                    </View>
                                </View>
                        }
                        <TextInput style={{ color: 'white', fontSize: width / 20, fontWeight: '800' }} editable={this.state.statusEditabel} onChangeText={(text) => this.setState({ tempNamaPanggilan: text })} value={this.state.tempNamaPanggilan} />
                        {
                            this.state.statusEditabel == true ?
                            <View style={{justifyContent:'center', alignItems:'center', height:2, width:width/4, backgroundColor:'white'}}/>:
                            <View/>
                        }
                        <View style={{ position: 'absolute', right: width / 15, top: width / 3 }}>
                            <TouchableOpacity onPress={() => this.clickEdit()}>
                                <View>
                                    {
                                        this.state.statusEditabel == true ?
                                            <Icon name={'close'} color={'white'} size={25} /> :
                                            <Icon name={'pencil'} color={'white'} size={25} />
                                    }
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ImageBackground>
                <ScrollView style={{ width: width / 1 }} showsVerticalScrollIndicator={false}>
                    <View>
                        <View
                            style={{
                                width: width / 1,
                                height: height / 10,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                            }}>
                            <View style={{ marginTop: width / 30, marginLeft: width / 10, width:width/2 }}>
                                <Text>NAMA LENGKAP</Text>
                                <TextInput style={{ color: '#CC2F4D' }} onChangeText={(text) => this.setState({ tempNamaLengkap: text })} editable={this.state.statusEditabel} value={this.state.tempNamaLengkap} />
                            </View>
                        </View>
                        <View
                            style={{
                                width: width / 1,
                                height: height / 10,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                            }}>
                            <View style={{ marginTop: width / 30, marginLeft: width / 10 }}>
                                <Text>DOB</Text>
                                {
                                    this.state.statusEditabel == true ?
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <TextInput style={{ color: '#CC2F4D' }} editable={this.state.statusEditabel} value={this.state.tempTanggalLahir} />
                                            <TouchableOpacity onPress={this.showDateTimePicker}>
                                                <Icon name="calendar" size={25} color="#A494EB" style={{ marginRight: width / 10 }} />
                                            </TouchableOpacity>
                                        </View> :
                                        <View>
                                            <TextInput style={{ color: '#CC2F4D' }} editable={this.state.statusEditabel} value={this.state.tempTanggalLahir} />
                                        </View>
                                }
                            </View>
                            <DateTimePicker
                                isVisible={this.state.isDateTimePickerVisible}
                                onConfirm={this.handleDatePicked}
                                onCancel={this.hideDateTimePicker}
                            />
                        </View>
                        <View
                            style={{
                                width: width / 1,
                                height: height / 10,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                            }}>
                            <View style={{ marginTop: width / 30, marginLeft: width / 10, width:width/2 }}>
                                <Text>TINGGI</Text>
                                <TextInput style={{ color: '#CC2F4D' }} onChangeText={(text) => this.setState({ tempHeight: text })} editable={false} value={this.state.tempHeight + ' ' + 'CM'} />
                            </View>
                        </View>
                        <View
                            style={{
                                width: width / 1,
                                height: height / 10,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                            }}>
                            <View style={{ marginTop: width / 30, marginLeft: width / 10, width:width/2 }}>
                                <Text>BERAT</Text>
                                <TextInput style={{ color: '#CC2F4D' }} onChangeText={(text) => this.setState({ tempWeight: text })} editable={false} value={this.state.tempWeight + ' ' + 'KG'} />
                            </View>
                        </View>
                        <View
                            style={{
                                width: width / 1,
                                height: height / 10,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                            }}>
                            <View style={{ marginTop: width / 30, marginLeft: width / 10 }}>
                                <Text>GENDER</Text>
                                {
                                    this.state.statusEditabel == true ?
                                        <View>
                                            <RadioForm
                                                radio_props={radio_props}
                                                formHorizontal={false}
                                                labelHorizontal={true}
                                                buttonColor={'#A494EB'}
                                                animation={true}
                                                initial={0}
                                                radioStyle={{ paddingRight: 20 }}
                                                formHorizontal={true}
                                                labelStyle={{ color: '#A494EB' }}
                                                onPress={(value) => this.setState({ tempGender: value })}
                                            />
                                        </View> :
                                        <View>
                                            <TextInput style={{ color: '#CC2F4D' }} editable={this.state.statusEditabel} value={this.state.tempJenisKelamin} />
                                        </View>
                                }
                            </View>
                        </View>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        {
                            this.state.statusEditabel == true ?
                                <TouchableOpacity onPress={() => this.clickUpdate()} style={{ borderRadius: 20, marginTop: 20, backgroundColor: '#A494EB', width: width / 1.2, height: height / 15, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: 'white', fontSize: 20, fontWeight: '700' }}>UPDATE</Text>
                                </TouchableOpacity> :
                                <View />
                        }
                    </View>
                    <View style={{ height: 50 }} />
                </ScrollView>
                {
                    this.state.isVisible == true ?
                        <Modal isVisible={this.state.isVisible}>
                            <ActivityIndicator size={25} color={"#A494EB"} />
                        </Modal>
                        :
                        <View />
                }
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
});

//make this component available to the app
export default Profileanak;
