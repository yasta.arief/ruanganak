//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, ScrollView, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
import Modal from 'react-native-modal';

var moment = require('moment');
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const GLOBAL = require('../../../Config/Services');

// create a component
class Kognitif extends Component {
    constructor() {
        super();
        this.state = {
            aktivitas: [
                {
                    id: 0,
                    name: 'MEMBALAS SENYUM',
                    image: require('../../../Assets/check-aktivitas.png'),
                    tanggal: '12 April, 11:00 AM',
                    deskripsi: 'SI KECIL SUDAH BERHASIL MELAKUKAN GERAKAN SEIMBANG'
                },
                {
                    id: 1,
                    name: 'MENGAMATAI TANGAN',
                    image: require('../../../Assets/check-aktivitas.png'),
                    tanggal: '12 April, 11:00 AM',
                    deskripsi: 'SI KECIL SUDAH BERHASIL MENGANKAT KEPALA'
                },
                {
                    id: 2,
                    name: 'MAKAN SENDIRI',
                    image: require('../../../Assets/uncheck-aktivitas.png'),
                    tanggal: '12 April, 11:00 AM',
                    deskripsi: 'SI KECIL SUDAH BERHASIL MENGANKAT KEPALA'
                },
                {
                    id: 3,
                    name: 'MENYATAKAN KEINGINAN',
                    image: require('../../../Assets/uncheck-aktivitas.png'),
                    tanggal: '12 April, 11:00 AM',
                    deskripsi: 'SI KECIL SUDAH BERHASIL MENGANKAT KEPALA'
                },
                {
                    id: 4,
                    name: 'MENYANYIKAN LAGU',
                    image: require('../../../Assets/uncheck-aktivitas.png'),
                    tanggal: '12 April, 11:00 AM',
                    deskripsi: 'SI KECIL SUDAH BERHASIL MENGANKAT KEPALA'
                },
                {
                    id: 5,
                    name: 'MENOLEH KEARAH SUARA',
                    image: require('../../../Assets/uncheck-aktivitas.png'),
                    tanggal: '12 April, 11:00 AM',
                    deskripsi: 'SI KECIL SUDAH BERHASIL MENGANKAT KEPALA'
                },
            ],
            isVisible:false
        }
    }
    componentDidMount() {
        this.funcGetCognitif()
    }
    funcGetCognitif() {
        this.setState({isVisible:true})
        fetch(GLOBAL.DEVELOPMENT_COGNITIF_URL + '/' + this.props.getIdChild, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'user-id': this.props.getIdParent
                // 'UserCode': this.state.usercode
            },
        }).then((response) => response.json()).then((responseData) => {
            console.log('respon kognitif', responseData)
            if (responseData.status == true) {
                this.setState({
                    aktivitas: responseData.data,
                    isVisible:false
                })
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    alertItemName = (item) => {
        alert(item.name)
    }
    clickBack() {
        Actions.menuanak({ getTemp: 'kembali_perkembangan' });
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: width / 10 }}>
                        <View>
                            <TouchableOpacity onPress={() => this.clickBack()} style={{ marginLeft: width / 20 }}>
                                <Icon name='arrow-left' size={25} color="white" />
                                {/* <Image source={require('../../../Assets/left-arrow.png')} /> */}
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, color: 'white' }}>KOGNITIF</Text>
                        </View>
                        <View>
                            <TouchableOpacity>
                                {/* <Image source={require('../../../Assets/history.png')} /> */}
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 20 }}>
                    <Text style={{ fontSize: 15, color: '#FD8B7F' }}>PERKEMBANGAN KOGNITIF</Text>
                </View>
                <ScrollView>
                    <View>
                        {
                            this.state.aktivitas.map((item, index) => (
                                <View
                                    key={item.id}
                                    style={{
                                        width: width / 1,
                                        height: height / 7,
                                        marginTop: width / 20,
                                        backgroundColor: 'white',
                                        shadowColor: "#000",
                                        shadowOffset: {
                                            width: 0,
                                            height: 3,
                                        },
                                        shadowOpacity: 0.27,
                                        shadowRadius: 4.65,
                                        elevation: 6,
                                        flexDirection: 'row'
                                    }}
                                    onPress={() => this.alertItemName(item)}>
                                    <View style={{ marginLeft: width / 20 }}>
                                        {
                                            item.devStatus == true ?
                                                <Image source={require('../../../Assets/check-aktivitas.png')} style={{ marginTop: width / 30 }} />
                                                : <Image source={require('../../../Assets/uncheck-aktivitas.png')} style={{ marginTop: width / 30 }} />
                                        }
                                    </View>
                                    <View>
                                        <View style={{flexDirection:'row', justifyContent:'space-between', marginTop:width/30, marginLeft:width/20}}>
                                            <View>
                                                <Text style={{fontSize:width/27, color:'#FD8B7F'}}>{item.devName}</Text>
                                            </View>
                                            <View style={{marginLeft:width/20}}>
                                                <Text style={{color:'#FD8B7F', fontSize:width/27}}>{moment(item.devDate).format('YYYY MMMM DD')}</Text>
                                            </View>
                                        </View>
                                        <View style={{width:width/2, marginLeft:width/20, marginTop:5}}>
                                            <Text style={{fontSize:width/30}} numberOfLines={3}>{item.devNotes}</Text>
                                        </View>
                                    </View>
                                </View>
                            ))
                        }
                    </View>
                </ScrollView>
                {
                    this.state.isVisible == true ?
                        <Modal isVisible={this.state.isVisible}>
                            <ActivityIndicator size={25} color={"#A494EB"} />
                        </Modal>
                        :
                        <View />
                }
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default Kognitif;
