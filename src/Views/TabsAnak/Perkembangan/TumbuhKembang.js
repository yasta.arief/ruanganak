//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Table, Row, Rows, Col } from 'react-native-table-component';
import { Actions } from 'react-native-router-flux';
import { Item } from 'native-base';
var moment = require('moment');

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const GLOBAL = require('../../../Config/Services');

// create a component
class TumbuhKembang extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tableHead: ['TANGGAL', 'TINGGI', 'BERAT', 'STATUS'],
            tableData: [
                ['12 April 2019', '90 CM', '15 KG', 'NORMAL'],
                ['12 April 2019', '90 CM', '15 KG', 'NORMAL'],
                ['12 April 2019', '90 CM', '15 KG', 'NORMAL'],
                ['12 April 2019', '90 CM', '15 KG', 'NORMAL'],
                ['12 April 2019', '90 CM', '15 KG', 'NORMAL'],
            ],
            tabel: []
        }
    }
    componentDidMount() {
        this.funcGetGrowth()
    }
    funcGetGrowth() {
        fetch(GLOBAL.DEVELOPMENT_GROWTH_URL + '/' + this.props.getIdChild, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'user-id': this.props.getIdParent
                // 'UserCode': this.state.usercode
            },
        }).then((response) => response.json()).then((responseData) => {
            console.log('respon growth', responseData)
            if (responseData.status == true) {
                this.setState({
                    tabel: responseData.data
                })
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    clickBack() {
        Actions.menuanak({ getTemp: 'kembali_perkembangan' });
    }
    render() {
        const state = this.state;
        return (
            <View style={styles.container}>
                <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: width / 10 }}>
                        <View>
                            <TouchableOpacity onPress={() => this.clickBack()} style={{ marginLeft: width / 20 }}>
                                <Icon name='arrow-left' size={25} color="white" />
                                {/* <Image source={require('../../../Assets/left-arrow.png')} /> */}
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, color: 'white' }}>TUMBUH KEMBANG</Text>
                        </View>
                        <View>
                            <TouchableOpacity>
                                {/* <Image source={require('../../../Assets/history.png')} /> */}
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <ScrollView>
                    <View style={{ margin: 20 }}>
                        <View style={{ flexDirection: 'row', flex: 1, alignItems: 'stretch', justifyContent: 'space-around' }}>
                            <View>
                                <Text>Tanggal</Text>
                            </View>
                            <View>
                                <Text>Tinggi</Text>
                            </View>
                            <View>
                                <Text>Berat</Text>
                            </View>
                            <View>
                                <Text>Status</Text>
                            </View>
                        </View>
                        <View>
                            {
                                this.state.tabel.map((value, index) => (
                                    <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-between', marginTop: 10 }}>
                                        <Text style={{ marginLeft: width / 30 }}>{moment(value.growDate).format('YYYY MMMM DD')}</Text>
                                        <Text style={{ marginRight: width / 30 }}>{value.growHeight}</Text>
                                        <Text style={{ marginLeft: width / 30 }}>{value.growWeight}</Text>
                                        <Text style={{ marginRight: width / 20 }}>{value.growStatus}</Text>
                                    </View>
                                ))
                            }
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    head: { height: 40, backgroundColor: '#955BA5' },
    textHeader: { margin: 6, color: 'white', fontSize: width / 30 },
    text: { margin: 6, color: '#FD8B7F', fontSize: width / 30 }
});

//make this component available to the app
export default TumbuhKembang;
