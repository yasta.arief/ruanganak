//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, ScrollView, NetInfo, AsyncStorage, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
import QRCode from 'react-native-qrcode';
import QRCodeScanner from 'react-native-qrcode-scanner';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const GLOBAL = require('../Config/Services');

// create a component
class Scan extends Component {
    constructor() {
        super();
        this.state = {
            text: 'ARIEF ADVANI',
            qrcode: '',
            isVisible:false
        }
    }
    componentDidMount() {
        this.funcGetUserid();
    }
    funcGetUserid() {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {
                // console.log('Internet is connected');
                AsyncStorage.getItem(GLOBAL.APPUSERID).then((value) => {
                    var data = JSON.parse(value);
                    this.setState({ tempId: data });
                }).done();
            } else {
                this.setState({isVisible:false})
                Alert.alert('Ruang Anak App', 'Cek Koneksi Internet Anda');
            }
        })
    }
    clickBack() {
        Actions.menuutama();
    }
    onSuccess(e) {
        // this.setState({
        //     dataqr: this.state.qrcode + ', ' + e.data,
        //     status: 'Coba Lagi'
        // })
        // Alert.alert(
        //     'QR Code',
        //     'Code : ' + e.data,
        //     [
        //         { text: 'OK', onPress: () => console.log('OK Pressed') },
        //     ],
        //     { cancelable: false }
        // )
        this.setState({isVisible:true})
        fetch(GLOBAL.REGISTER_DEVICE_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                // 'Authorization': '0',
                'user-id': this.state.tempId
            },
            body: JSON.stringify({
                "trackerImei" : e.data
            })
        }).then((response) => response.json()).then((responseData) => {
            if (responseData.status == true) {
                this.setState({ isVisible: false });
                Alert.alert('Ruang Anak App', responseData.message);
                Actions.menuutama();
            } else {
                this.setState({ isVisible: false });
                Alert.alert('Ruang Anak App', responseData.message);
            }
        }).done();
    }
    clickQrcode(){
        Actions.qrcodeuser();
    }
    render() {
        return (
            <View style={styles.container}>
                {/* <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: width / 10 }}>
                        <View>
                            <TouchableOpacity onPress={() => this.clickBack()} style={{ marginLeft: width / 20 }}>
                                <Icon name='arrow-left' size={25} color="white" />
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, color: 'white' }}>SCAN</Text>
                        </View>
                        <View>
                            <TouchableOpacity>
                                <Image source={require('../Assets/history.png')} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Text>Arief ADVANI</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 20 }}>
                    <Text style={{ fontSize: 17, color: '#A494EB', fontSize: width / 25 }}>PERLIHATKAN QRCODE INI KEPADA</Text>
                    <Text style={{ fontSize: 17, color: '#A494EB', fontSize: width / 25 }}>PENJAGA DAYCARE UNTUK MENJEMPUT</Text>
                    <Text style={{ fontSize: 17, color: '#A494EB', fontSize: width / 25 }}>ATAU SCAN BARCODE KETIKA MENJEMPUT</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 2.5, height: height / 15 }}>
                    <QRCode
                        value={this.state.text}
                        size={width / 1.5}
                        bgColor='black'
                        fgColor='white' />
                </View> */}
                {/* <View style={{ position: 'absolute', bottom: 0 }}>
                    <TouchableOpacity style={{ width: width / 1, height: height / 12, backgroundColor: '#A494EB', justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ color: 'white', fontWeight: 'bold', fontSize: 20 }}>SCAN</Text>
                    </TouchableOpacity>
                </View> */}
                <View>
                    <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: width / 10 }}>
                            <View>
                                <TouchableOpacity onPress={() => this.clickBack()} style={{ marginLeft: width / 20 }}>
                                    <Icon name='arrow-left' size={25} color="white" />
                                </TouchableOpacity>
                            </View>
                            <View>
                                <Text style={{ fontSize: 20, color: 'white' }}>SCAN</Text>
                            </View>
                            <View style={{marginRight:width/15}}>
                                <TouchableOpacity onPress={()=>this.clickQrcode()}>
                                    {/* <Icon name={"qrcode"} size={25} color={"white"} /> */}
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
                <QRCodeScanner
                    onRead={this.onSuccess.bind(this)}
                // bottomContent={
                //     <TouchableOpacity style={styles.buttonTouchable}>
                //         <Text style={styles.buttonText}>OK. Got it!</Text>
                //     </TouchableOpacity>
                // }
                />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default Scan;
