//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import Icon from 'react-native-vector-icons/FontAwesome';
import Detail from './TabsDaycare/Detail';
import Review from './TabsDaycare/Review';
import { Actions } from 'react-native-router-flux';

const FirstRoute = () => (
    <Detail/>
);
const SecondRoute = () => (
    <Review/>
);

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

// create a component
class DaycareTerdekatDetail extends Component {
    // constructor(){
    //     super()
    //     this.state = {
    //         data:[]
    //     }
    // }
    state = {
        index: 0,
        routes: [
            { key: 'first', title: 'DETAIL' },
            { key: 'second', title: 'REVIEW' },
        ],
    };

    _handleIndexChange = index => this.setState({ index });

    _renderTabBar(props) {
        return (
            <View>
                <TabBar
                    {...props}
                    style={{ backgroundColor: '#fff', elevation: 0, height: 36 }}
                    indicatorStyle={{ backgroundColor: '#16ccff', height: 3 }}
                    renderLabel={({ route }) => (
                        <View>
                            <Text style={{
                                fontSize: 20, textAlign: 'center',
                                color: route.key === props.navigationState.routes[props.navigationState.index].key ?
                                    90 : 120
                            }}>
                                {route.title}
                            </Text>
                        </View>
                    )}
                />
            </View>
        );
    }

    clickBack(){
        Actions.daycareterdekat();
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: width / 10 }}>
                        <View>
                            <TouchableOpacity onPress={() => this.clickBack()} style={{ marginLeft: width / 20 }}>
                                <Icon name='arrow-left' size={25} color="white" />
                                {/* <Image source={require('../../../Assets/left-arrow.png')} /> */}
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, color: 'white' }}>DAYCARE</Text>
                        </View>
                        <View>
                            <TouchableOpacity>
                                {/* <Image source={require('../../../Assets/history.png')} /> */}
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{ height: Dimensions.get('window').height / 1, width: Dimensions.get('window').width / 1 }}>
                    <TabView
                        navigationState={this.state}
                        renderScene={SceneMap({
                            first: FirstRoute,
                            second: SecondRoute,
                        })}
                        renderTabBar={this._renderTabBar}
                        onIndexChange={index => this.setState({ index })}
                        initialLayout={{ width: Dimensions.get('window').width }}
                    />
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    scene: {
        flex: 1,
    },
    tabBar: {
        flexDirection: 'row',
        paddingTop: 10,
    },
    tabItem: {
        flex: 1,
        alignItems: 'center',
        padding: 16,
        color: 'grey'
    },
});

//make this component available to the app
export default DaycareTerdekatDetail;
