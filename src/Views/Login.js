//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, Image, TextInput, TouchableOpacity, AsyncStorage, Alert, KeyboardAvoidingView, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Modal from 'react-native-modal';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const GLOBAL = require('../Config/Services');

// create a component
class Login extends Component {
    constructor() {
        super();
        this.state = {
            tempUsername: '',
            tempUserpin: '',
            isVisible: false
        }
    }
    clickLogin() {
        this.setState({ isVisible: true })
        // Actions.menuutama();
        // AsyncStorage.setItem(GLOBAL.CHECKPAGE, 'masuk');
        // var tempUserpin2 = this.state.tempUserpin
        fetch(GLOBAL.LOGIN_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': '0',
                // 'UserCode': this.state.usercode
            },
            body: JSON.stringify({
                'userName': this.state.tempUsername,
                'userPin': parseInt(this.state.tempUserpin)
            })
        }).then((response) => response.json()).then((responseData) => {
            if (responseData.status == true) {
                this.setState({ isVisible: false });
                Alert.alert('Ruang Anak App', responseData.message);
                AsyncStorage.setItem(GLOBAL.APPUSERID, JSON.stringify(responseData.data.userId));
                AsyncStorage.setItem(GLOBAL.CHECKPAGE, "1");
                Actions.menuutama();
            } else {
                this.setState({ isVisible: false });
                Alert.alert('Ruang Anak App', responseData.message);
            }
        }).done();
    }
    clickBack() {
        Actions.daftarmasuk();
    }
    render() {
        return (
            <View style={styles.container}>
                <KeyboardAwareScrollView style={{ flex: 1 }} behavior="padding">
                    <View style={{ margin: width / 20 }}>
                        <TouchableOpacity onPress={() => this.clickBack()}>
                            <Icon name={"arrow-left"} size={25} color={'white'} />
                        </TouchableOpacity>
                    </View>
                    <Image source={require('../Assets/KeluargaLogin.png')} style={{ width: width / 1, height: height / 2.5 }} />
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ marginTop: 20, width: width / 1.1, marginLeft: width / 25 }}>
                            <Text style={{ fontSize: width / 10, color: 'white' }}>Hai, Ayah Bunda</Text>
                            <Text style={{ fontSize: 15, color: 'white' }}>Login Dulu Yuk, Untuk Mengakses Ruang Anak</Text>
                        </View>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <View style={{ flexDirection: 'row', marginTop: width / 30, backgroundColor: 'white', borderRadius: 20, borderWidth: 1, borderColor: '#FD8B7F', width: width / 1.2, height: height / 15 }}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', width: width / 10, height: height / 20, backgroundColor: '#FD8B7F', borderRadius: 50, marginTop: width / 60, marginLeft: width / 30 }}>
                                <Icon name="envelope" color="white" size={20} />
                            </View>
                            <View>
                                <TextInput onChangeText={(text) => this.setState({ tempUsername: text })} style={{ color: '#FD8B7F', marginLeft: 30, fontSize: width / 20 }} placeholder={'Username'} placeholderTextColor={'#FD8B7F'} />
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: width / 30, backgroundColor: 'white', borderRadius: 20, borderWidth: 1, borderColor: '#FD8B7F', width: width / 1.2, height: height / 15 }}>
                            <View style={{ justifyContent: 'center', alignItems: 'center', width: width / 10, height: height / 20, backgroundColor: '#FD8B7F', borderRadius: 50, marginTop: width / 60, marginLeft: width / 30 }}>
                                <Icon name="key" color="white" size={30} />
                            </View>
                            <View>
                                <TextInput secureTextEntry={true} onChangeText={(text) => this.setState({ tempUserpin: text })} style={{ color: '#FD8B7F', marginLeft: 30, fontSize: width / 20 }} placeholder={'PIN'} placeholderTextColor={'#FD8B7F'} keyboardType={'number-pad'} />
                            </View>
                        </View>
                        <TouchableOpacity style={{ marginLeft: width / 2, marginTop: 10 }}>
                            <Text style={{ color: 'white' }}>Lupa Password ?</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.clickLogin()} style={{ borderRadius: 20, marginTop: 20, backgroundColor: '#FD8B7F', width: width / 1.2, height: height / 15, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: 'white', fontSize: 20 }}>LOGIN</Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAwareScrollView>
                {
                    this.state.isVisible == true ?
                        <Modal isVisible={this.state.isVisible}>
                            <ActivityIndicator size={25} color={"#A494EB"}/>
                        </Modal>
                        :
                        <View />
                }
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#A494EB',
    },
});

//make this component available to the app
export default Login;
