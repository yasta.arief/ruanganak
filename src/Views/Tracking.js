// //import liraries
// import React, { Component } from 'react';
// import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, ScrollView, Animated } from 'react-native';
// import Icon from 'react-native-vector-icons/FontAwesome';
// import MapView from 'react-native-maps';
// import {Actions} from 'react-native-router-flux';

// const width = Dimensions.get('window').width
// const height = Dimensions.get('window').height

// const CARD_HEIGHT = height / 5;
// const CARD_WIDTH = width / 1.5;

// // create a component
// class Tracking extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             markers: [
//                 {
//                     coordinate: {
//                         latitude: 45.524548,
//                         longitude: -122.6749817,
//                     },
//                     title: "OWN DAYCARE",
//                     description: "Jl. Gerbang Pemuda Senayan Jakarta Pusat 10270",
//                     // image: Images[0],
//                 },
//                 {
//                     coordinate: {
//                         latitude: 45.524698,
//                         longitude: -122.6655507,
//                     },
//                     title: "LITTLE SUNSHINE DAYCARE",
//                     description: "Jl. Gerbang Pemuda Senayan Jakarta Pusat 10270",
//                     // image: Images[1],
//                 },
//                 {
//                     coordinate: {
//                         latitude: 45.5230786,
//                         longitude: -122.6701034,
//                     },
//                     title: "OPA DAYCARE",
//                     description: "Jl. Gerbang Pemuda Senayan Jakarta Pusat 10270",
//                     // image: Images[2],
//                 },
//                 {
//                     coordinate: {
//                         latitude: 45.521016,
//                         longitude: -122.6561917,
//                     },
//                     title: "KIDDY DAYCARE",
//                     description: "Jl. Gerbang Pemuda Senayan Jakarta Pusat 10270",
//                     // image: Images[3],
//                 },
//             ],
//             region: {
//                 latitude: 45.52220671242907,
//                 longitude: -122.6653281029795,
//                 latitudeDelta: 0.04864195044303443,
//                 longitudeDelta: 0.040142817690068,
//             },
//         };
//     }
//     componentWillMount() {
//         this.index = 0;
//         this.animation = new Animated.Value(0);
//     }
//     componentDidMount() {
//         // We should detect when scrolling has stopped then animate
//         // We should just debounce the event listener here
//         this.animation.addListener(({ value }) => {
//             let index = Math.floor(value / CARD_WIDTH + 0.3); // animate 30% away from landing on the next item
//             if (index >= this.state.markers.length) {
//                 index = this.state.markers.length - 1;
//             }
//             if (index <= 0) {
//                 index = 0;
//             }

//             clearTimeout(this.regionTimeout);
//             this.regionTimeout = setTimeout(() => {
//                 if (this.index !== index) {
//                     this.index = index;
//                     const { coordinate } = this.state.markers[index];
//                     this.map.animateToRegion(
//                         {
//                             ...coordinate,
//                             latitudeDelta: this.state.region.latitudeDelta,
//                             longitudeDelta: this.state.region.longitudeDelta,
//                         },
//                         350
//                     );
//                 }
//             }, 10);
//         });
//     }
//     clickBack() {
//         Actions.menuutama();
//     }
//     clickDaycareDetail(){
//         Actions.daycareterdekatdetail();
//     }
//     render() {
//         const interpolations = this.state.markers.map((marker, index) => {
//             const inputRange = [
//                 (index - 1) * CARD_WIDTH,
//                 index * CARD_WIDTH,
//                 ((index + 1) * CARD_WIDTH),
//             ];
//             const scale = this.animation.interpolate({
//                 inputRange,
//                 outputRange: [1, 2.5, 1],
//                 extrapolate: "clamp",
//             });
//             const opacity = this.animation.interpolate({
//                 inputRange,
//                 outputRange: [0.35, 1, 0.35],
//                 extrapolate: "clamp",
//             });
//             return { scale, opacity };
//         });
//         return (
//             <View style={styles.container}>
//                 <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
//                     <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: width / 10 }}>
//                         <View style={{marginLeft:width/30}}>
//                             <TouchableOpacity onPress={() => this.clickBack()} style={{ marginLeft: width / 20 }}>
//                                 <Icon name='arrow-left' size={25} color="white" />
//                                 {/* <Image source={require('../../../Assets/left-arrow.png')} /> */}
//                             </TouchableOpacity>
//                         </View>
//                         <View>
//                             <Text style={{ fontSize: 20, color: 'white' }}>TRACKING</Text>
//                         </View>
//                         <View>
//                             <TouchableOpacity>
//                                 {/* <Image source={require('../../../Assets/history.png')} /> */}
//                             </TouchableOpacity>
//                         </View>
//                     </View>
//                 </View>
//                 <View style={{ flex: 1, justifyContent: 'center' }}>
//                     <MapView
//                         ref={map => this.map = map}
//                         initialRegion={this.state.region}
//                         style={styles.container}
//                     >
//                         {this.state.markers.map((marker, index) => {
//                             const scaleStyle = {
//                                 transform: [
//                                     {
//                                         scale: interpolations[index].scale,
//                                     },
//                                 ],
//                             };
//                             const opacityStyle = {
//                                 opacity: interpolations[index].opacity,
//                             };
//                             return (
//                                 <MapView.Marker key={index} coordinate={marker.coordinate}>
//                                     <Animated.View style={[styles.markerWrap, opacityStyle]}>
//                                         <Animated.View style={[styles.ring, scaleStyle]} />
//                                         <View style={styles.marker} />
//                                     </Animated.View>
//                                 </MapView.Marker>
//                             );
//                         })}
//                     </MapView>
//                     <Animated.ScrollView
//                         horizontal
//                         scrollEventThrottle={1}
//                         showsHorizontalScrollIndicator={false}
//                         snapToInterval={CARD_WIDTH}
//                         onScroll={Animated.event(
//                             [
//                                 {
//                                     nativeEvent: {
//                                         contentOffset: {
//                                             x: this.animation,
//                                         },
//                                     },
//                                 },
//                             ],
//                             { useNativeDriver: true }
//                         )}
//                         style={styles.scrollView}
//                         contentContainerStyle={styles.endPadding}>
//                         {this.state.markers.map((marker, index) => (
//                             <View style={styles.card} key={index}>
//                                 {/* <Image
//                                 source={marker.image}
//                                 style={styles.cardImage}
//                                 resizeMode="cover"
//                             /> */}
//                                 <View style={styles.textContent}>
//                                     <Text numberOfLines={1} style={styles.cardtitle}>{marker.title}</Text>
//                                     <Text numberOfLines={2} style={styles.cardDescription}>
//                                         {marker.description}
//                                     </Text>
//                                 </View>
//                                 <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>
//                                     <View style={{ flexDirection: 'row' }}>
//                                         <Image source={require('../Assets/placeholder-merah-muda.png')}/>
//                                         <Text style={{ marginLeft: 5 }}>1,5 KM</Text>
//                                     </View>
//                                     <View style={{ flexDirection: 'row' }}>
//                                         <Image source={require('../Assets/home-merah-muda.png')}/>
//                                         <Text style={{ marginLeft: 5 }}>15 Anak</Text>
//                                     </View>
//                                     <View style={{ flexDirection: 'row' }}>
//                                         <Image source={require('../Assets/rp.png')}/>
//                                         <Text style={{ marginLeft: 5 }}>70000/Hari</Text>
//                                     </View>
//                                 </View>
//                                 <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 10 }}>
//                                     <TouchableOpacity onPress={() => this.clickDaycareDetail()} style={{ backgroundColor: '#FD8B7F', height: 30, width: width/1.9, justifyContent: 'center', alignItems: 'center', borderRadius: 20 }}>
//                                         <Text style={{color:'white'}}>DETAIL</Text>
//                                     </TouchableOpacity>
//                                 </View>
//                             </View>
//                         ))}
//                     </Animated.ScrollView>
//                 </View>
//             </View>
//         );
//     }
// }

// // define your styles
// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: '#fff',
//     },
//     map: {
//         flex: 1,
//         width: Dimensions.get('window').width / 1,
//         height: Dimensions.get('window').height / 10
//     },
//     inputView: {
//         backgroundColor: 'rgba(0,0,0,0)',
//         position: 'absolute',
//         top: 20,
//         left: 5,
//         right: 5
//     },
//     input: {
//         height: 40,
//         padding: 10,
//         marginTop: 20,
//         marginLeft: 10,
//         marginRight: 10,
//         fontSize: 18,
//         borderWidth: 1,
//         borderRadius: 10,
//         borderColor: '#48BBEC',
//         backgroundColor: 'white',
//     },
//     scrollView: {
//         position: "absolute",
//         bottom: 30,
//         left: 0,
//         right: 0,
//         paddingVertical: 10,
//     },
//     endPadding: {
//         paddingRight: width - CARD_WIDTH,
//     },
//     card: {
//         padding: 10,
//         elevation: 2,
//         backgroundColor: "#FFF",
//         marginHorizontal: 10,
//         shadowColor: "#000",
//         shadowRadius: 5,
//         shadowOpacity: 0.3,
//         shadowOffset: { x: 2, y: -2 },
//         height: CARD_HEIGHT,
//         width: CARD_WIDTH,
//         overflow: "hidden",
//     },
//     cardImage: {
//         flex: 3,
//         width: "100%",
//         height: "100%",
//         alignSelf: "center",
//     },
//     textContent: {
//         flex: 1,
//     },
//     cardtitle: {
//         fontSize: 12,
//         marginTop: 5,
//         fontWeight: "bold",
//     },
//     cardDescription: {
//         fontSize: 12,
//         color: "#444",
//         marginTop:10
//     },
//     markerWrap: {
//         alignItems: "center",
//         justifyContent: "center",
//     },
//     marker: {
//         width: 8,
//         height: 8,
//         borderRadius: 4,
//         backgroundColor: "rgba(130,4,150, 0.9)",
//     },
//     ring: {
//         width: 24,
//         height: 24,
//         borderRadius: 12,
//         backgroundColor: "rgba(130,4,150, 0.3)",
//         position: "absolute",
//         borderWidth: 1,
//         borderColor: "rgba(130,4,150, 0.5)",
//     },
// });

// //make this component available to the app
// export default Tracking;

//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, ScrollView, TextInput, Animated, PermissionsAndroid, Alert, Platform, AsyncStorage } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
import MapView, {
    Marker,
    AnimatedRegion,
    Polyline,
    PROVIDER_GOOGLE
} from 'react-native-maps';
import haversine from "haversine";
import Communications from 'react-native-communications';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

const CARD_HEIGHT = height / 5;
const CARD_WIDTH = width / 1.5;

const LATITUDE_DELTA = 0.009;
const LONGITUDE_DELTA = 0.009;
var LATITUDE = 0.00;
var LONGITUDE = -0.00;

const GLOBAL = require('../Config/Services');
class Tracking extends Component {
    alertItemName = (item) => {
        alert(item.name)
    }
    clickBack() {
        Actions.menuutama();
    }
    constructor(props) {
        super(props);

        this.state = {
            phoneNumber: '',
            battery:'',
            lastUpdate:'',
            latitude: LATITUDE,
            longitude: LONGITUDE,
            routeCoordinates: [],
            distanceTravelled: 0,
            prevLatLng: {},
            coordinate: new AnimatedRegion({
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: 0,
                longitudeDelta: 0
            }),
            markers:[],
            markerVisible: false
        };
    }
    componentDidMount() {
        this.funcGetUserid();
        this.funcGetGPS();
    }


    funcGetUserid() {
        AsyncStorage.getItem(GLOBAL.APPUSERID).then((value) => {
            var data = JSON.parse(value);
            this.funcGetGPS(data);
            this.setState({ tempId: data });
        }).done();
    }

    funcGetGPS(getId) {
        const { coordinate } = this.state

        fetch(GLOBAL.GPS_INFO_URL, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': '0',
                'user-id': getId
            },
        }).then((response) => response.json()).then((responseData) => {
            console.log('respon coy', responseData)
            if (responseData.status == true) {
                // LATITUDE = responseData.data.trackerLat;
                // LONGITUDE = responseData.data.trackerLon;
                this.setState({
                    latitude: responseData.data.trackerLat,
                    longitude: responseData.data.trackerLon,
                    phoneNumber: responseData.data.trackerPhoneNumber,
                    battery: responseData.data.trackerBattery,
                    lastUpdate: responseData.data.trackerLastUpdate,
                    markers: [
                        {
                            coordinate: {
                                latitude: responseData.data.trackerLat,
                                longitude: responseData.data.trackerLon,
                            },
                            description: "Kids Position",
                        }
                    ],
                    // routeCoordinates: routeCoordinates.concat([newCoordinate]),
                    // distanceTravelled:
                    //     distanceTravelled + this.calcDistance(newCoordinate),
                    // prevLatLng: newCoordinate
                });
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }

    funcMyLocation() {
        const { coordinate } = this.state;
        // this.requestCameraPermission();
        this.watchID = navigator.geolocation.watchPosition(
            position => {
                const { routeCoordinates, distanceTravelled } = this.state;
                const { latitude, longitude } = position.coords;

                const newCoordinate = {
                    latitude,
                    longitude
                };
                console.log({ newCoordinate });

                if (Platform.OS === "android") {
                    if (this.marker) {
                        this.marker._component.animateMarkerToCoordinate(
                            newCoordinate,
                            500
                        );
                    }
                } else {
                    coordinate.timing(newCoordinate).start();
                }

                this.setState({
                    latitude,
                    longitude,
                    routeCoordinates: routeCoordinates.concat([newCoordinate]),
                    distanceTravelled:
                        distanceTravelled + this.calcDistance(newCoordinate),
                    prevLatLng: newCoordinate
                });
                console.log('latitude state', this.state.latitude, 'longitude state', this.state.longitude)
            },
            error => console.log('error apa', error),
            {
                // enableHighAccuracy: true,
                timeout: 360000,
                maximumAge: 2000,
                distanceFilter: 10
            }
        );
    }

    // componentWillMount(){
    //     this.funcMyLocation();
    // }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchID);
    }

    getMapRegion = () => ({
        latitude: this.state.latitude,
        longitude: this.state.longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
    });

    calcDistance = newLatLng => {
        const { prevLatLng } = this.state;
        return haversine(prevLatLng, newLatLng) || 0;
    };

    requestCameraPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    title: "Location Access Permission",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use the camera");
            } else {
                console.log("Camera permission denied");
            }
        } catch (err) {
            console.warn(err);
        }
    };
    clickMyLocation() {
        this.funcMyLocation();
    }
    clickHistorical() {
        // this.setState({ marker: [] })
        Actions.historical();
    }
    clickPanic() {
        Alert.alert('Ruang Anak App', '');
    }
    clickAlarmList() {
        Actions.alarmlist();
    }
    clickTracking() {
        Actions.tracking();
    }
    viewMaps() {

    }
    render() {
        return (
            <View style={styles.container}>
                <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: width / 10 }}>
                        <View style={{ marginLeft: width / 30 }}>
                            <TouchableOpacity onPress={() => this.clickBack()}>
                                <Icon name='arrow-left' size={25} color="white" />
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, color: 'white' }}>TRACKER</Text>
                        </View>
                        <View>
                            <TouchableOpacity>
                                {/* <Image source={require('../../Assets/history.png')} /> */}
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <MapView
                        style={styles.map}
                        provider={this.props.provider}
                        loadingEnabled
                        // region={this.state.mapRegion}
                        showsUserLocation={true}
                        followUserLocation={true}
                        region={this.getMapRegion()}>
                        {this.state.markers.map((marker, index) => {
                            return (
                                <MapView.Marker
                                    visible={false}
                                    // onCalloutVisibleChange={visible => this.setState({ markerVisible:visible })} 
                                    key={index}
                                    coordinate={marker.coordinate}>
                                    <Animated.View style={[styles.markerWrap]}>
                                        <Animated.View style={[styles.ring]} />
                                        <View style={styles.marker} />
                                    </Animated.View>
                                    {/* <Image source={require('../Assets/placeholder-ungu.png')} /> */}
                                </MapView.Marker>
                            );
                        })}
                    </MapView>
                    <View style={{ position: 'absolute', marginLeft: Dimensions.get('window').width / 1.25 }}>
                        <TouchableOpacity onPress={() => this.clickTracking()} style={{ width: width / 7, height: height / 14, borderRadius: 30, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../Assets/realtime.png')} style={{ width: width / 12, height: width / 13 }} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.clickHistorical()} style={{ width: width / 7, height: height / 14, borderRadius: 30, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', marginTop: width / 30 }}>
                            <Image source={require('../Assets/historical.png')} style={{ width: width / 12, height: width / 12 }} />
                        </TouchableOpacity>
                        {/* <TouchableOpacity onPress={() => this.clickAlarmList()} style={{ width: width / 7, height: height / 14, borderRadius: 30, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', marginTop: width / 30 }}>
                            <Image source={require('../Assets/alarm.png')} style={{ width: width / 12, height: width / 13 }} />
                        </TouchableOpacity> */}
                    </View>
                    <View style={styles.inputView}>
                        <View style={{ width: width / 1.5, height: height / 6, backgroundColor: 'white' }}>
                            <View style={{ marginLeft: width / 20, marginTop: width / 30 }}>
                                <View>
                                    <Text>Status Baterai : {this.state.battery} %</Text>
                                    <Text>Last Update : {this.state.lastUpdate}</Text>
                                </View>
                            </View>
                            <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 20 - 10 }}>
                                <TouchableOpacity onPress={()=>Communications.phonecall(this.state.phoneNumber, true)} style={{ flexDirection: 'row', backgroundColor: '#955BA5', width: width / 2, height: height / 15, justifyContent: 'center', alignItems: 'center', borderRadius: 15 }}>
                                    <Image source={require('../Assets/phone-call-putih.png')} />
                                    <Text style={{ color: 'white', marginLeft: 5 }}>CALL</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    map: {
        flex: 1,
        width: Dimensions.get('window').width / 1,
        height: Dimensions.get('window').height / 10
    },
    inputView: {
        backgroundColor: 'rgba(0,0,0,0)',
        position: 'absolute',
        marginTop: width / 0.9,
        top: 20,
        left: 5,
        right: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        height: 40,
        padding: 10,
        marginTop: 20,
        marginLeft: 10,
        marginRight: 10,
        fontSize: 18,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#48BBEC',
        backgroundColor: 'white',
    },
    scrollView: {
        position: "absolute",
        bottom: 30,
        left: 0,
        right: 0,
        paddingVertical: 10,
    },
    endPadding: {
        paddingRight: width - CARD_WIDTH,
    },
    card: {
        padding: 10,
        elevation: 2,
        backgroundColor: "#FFF",
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: { x: 2, y: -2 },
        height: CARD_HEIGHT,
        width: CARD_WIDTH,
        overflow: "hidden",
    },
    cardImage: {
        flex: 3,
        width: "100%",
        height: "100%",
        alignSelf: "center",
    },
    textContent: {
        flex: 1,
    },
    cardtitle: {
        fontSize: 12,
        marginTop: 5,
        fontWeight: "bold",
    },
    cardDescription: {
        fontSize: 12,
        color: "#444",
        marginTop: 10
    },
    markerWrap: {
        alignItems: "center",
        justifyContent: "center",
    },
    marker: {
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: "rgba(130,4,150, 0.9)",
    },
    ring: {
        width: 24,
        height: 24,
        borderRadius: 12,
        backgroundColor: "rgba(130,4,150, 0.3)",
        position: "absolute",
        borderWidth: 1,
        borderColor: "rgba(130,4,150, 0.5)",
    },
});

//make this component available to the app
export default Tracking;

