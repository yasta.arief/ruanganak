//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, ScrollView, TextInput, Alert, AsyncStorage } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Drawer, Accordion } from 'native-base';
import DatePicker from 'react-native-date-picker';
import ImagePicker from 'react-native-image-picker';
import DateTimePicker from "react-native-modal-datetime-picker";
import { Actions } from 'react-native-router-flux';
var moment = require('moment');
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const GLOBAL = require('../Config/Services');

var radio_props = [
    { label: 'Laki-laki', value: 'L' },
    { label: 'Perempuan', value: 'P' }
];

// create a component
class FormInputAnak extends Component {
    constructor() {
        super();
        this.state = {
            photoAnak: false,
            tempNamaLengkapAnak: '',
            tempNamaPanggilanAnak: '',
            tempGender: 'L',
            tempHeight:0,
            tempWeight:0,
            tempUserid: '',
            tempPhoto:'',
            date: new Date(),
            showDatePicker: false
        }
    }
    selectImageTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
                path: 'images'
            }
        };

        ImagePicker.showImagePicker(options, (response) => {

            if (response.didCancel) {
                console.log('User cancelled video picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                if (this.state.statusConnection == true) {
                    var photo = {
                        uri: response.uri,
                        type: 'image/jpg',
                        name: 'MKFCastingApp.jpg',
                    };
                    var uploadForm = new FormData()
                    uploadForm.append('mediaFile', photo);
                    this.setState({ indicatorSendPhoto: true })
                    fetch(GLOBAL.SAVEPHOTOPROFILE_URL, {
                        method: 'POST',
                        headers: {
                            'Content-type': 'application/json',
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'Authorization': '0',
                            'UserCode': this.state.usercode
                        },
                        body: uploadForm
                    }).then((response) => response.json()).then((responseData) => {
                        if (responseData.status == true) {
                            this.setState({ mediaTrxCode: responseData.mediaTrxCode, indicatorSendPhoto: false });
                            Alert.alert('Ruang Anak App', responseData.message)
                        } else {
                            Alert.alert('Ruang Anak App', responseData.message)
                        }
                    }).done();
                } else {
                    Alert.alert('Ruang Anak App', 'Harap periksa kembali koneksi jaringan anda');
                }
            }
        });
    }
    getPicker() {
        try {
            return (
                <View style={{ backgroundColor: 'white', marginLeft: 15, marginRight: 15 }}>
                    <DatePicker
                        style={{ height: 150, color: 'white' }}
                        date={this.state.date} onDateChange={(date) => this.setState({ date })}
                        mode="date" />
                </View>
            )
        } catch {
            console.error(e.message);
        }
    }
    componentDidMount() {
        this.funcGetUserid();
    }
    funcGetUserid() {
        AsyncStorage.getItem(GLOBAL.APPUSERID).then((value) => {
            var data = JSON.parse(value);
            this.setState({ tempUserid: data });
        }).done();
    }
    clickSimpan() {
        if (this.state.tempGender == 1 || this.state.tempGender == '') {
            this.setState({ tempGender: 'L' });
        } else {
            this.setState({ tempGender: 'P' });
        }

        fetch(GLOBAL.ADDCHILD_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'user-id': this.state.tempUserid
                // 'UserCode': this.state.usercode
            },
            body: JSON.stringify({
                "bioChildFullname": this.state.tempNamaLengkapAnak,
                "bioChildNickname": this.state.tempNamaPanggilanAnak,
                "bioChildDob": this.state.date,
                "bioChildGender": this.state.tempGender,
                "bioChildHeight": this.state.tempHeight,
                "bioChildWeight": this.state.tempWeight,
                "bioChildPhotoProfile": this.state.tempPhoto
            })
        }).then((response) => response.json()).then((responseData) => {
            if (responseData.status == true) {
                this.setState({ isVisible: false });
                Actions.menuutama();
                Alert.alert('Ruang Anak App', responseData.message);
            } else {
                this.setState({ isVisible: false });
                Alert.alert('Ruang Anak App', responseData.message);
            }
        }).done();
    }
    render() {
        var showDatePicker = this.state.showDatePicker ?
            this.getPicker()
            :
            <View />
        return (
            <View style={styles.container}>
                <ImageBackground source={require('../Assets/Header.png')} style={{ width: width / 1, height: height / 3.3 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 10 }}>
                        <Text style={{ marginTop: 10, color: 'white', fontSize: width / 30, fontWeight: '800' }}>DAFTAR ANAK</Text>
                        {/* <Image source={require('../Assets/rara.png')} style={{ marginTop: 10 }} /> */}
                        <TouchableOpacity onPress={() => this.selectImageTapped()}
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginBottom: 10,
                                width: width / 5,
                                height: width / 5,
                                borderRadius: width / 10,
                                backgroundColor: '#eaeaea',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                                marginTop: width / 20
                            }}>
                            {
                                this.state.photoAnak == false ?
                                    <Icon name="plus" /> : 
                                    <Image source={{
                                        uri: GLOBAL.GET_PHOTO_URL+'',
                                        method: 'GET',
                                        headers: {
                                            'Content-type': 'application/json',
                                            'Authorization': '0',
                                            'UserCode': this.state.usercode
                                        },
                                        cache: 'reload'
                                    }}
                                        style={{ height: height/6, width: width/6, borderRadius: width/10 }} />
                                    // <Image source={require('../Assets/rara.png')} style={{ width: width / 4.9, height: height / 8.5, borderRadius: width / 10 }} />
                            }
                            {/* <Text>Home</Text> */}
                        </TouchableOpacity>
                        {/* <Text style={{ marginTop: 10, color: 'white', fontSize: width/20, fontWeight: '800' }}>FATIH</Text> */}
                    </View>
                </ImageBackground>
                <ScrollView style={{ width: width / 1 }} showsVerticalScrollIndicator={false}>
                    <View>
                        <View
                            style={{
                                width: width / 1,
                                height: height / 10,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                            }}>
                            <View style={{ marginTop: width / 30, marginLeft: width / 10 }}>
                                <Text>NAMA LENGKAP</Text>
                                <TextInput style={{ color: '#A494EB' }} onChangeText={(text) => this.setState({ tempNamaLengkapAnak: text })} placeholder={'Masukkan Nama Lengkap Anak'} />
                            </View>
                        </View>
                        <View
                            style={{
                                width: width / 1,
                                height: height / 10,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                            }}>
                            <View style={{ marginTop: width / 30, marginLeft: width / 10 }}>
                                <Text>NAMA PANGGILAN</Text>
                                <TextInput style={{ color: '#A494EB' }} onChangeText={(text) => this.setState({ tempNamaPanggilanAnak: text })} placeholder={'Masukkan Nama Panggilan Anak'} />
                            </View>
                        </View>
                        <View
                                style={{
                                    width: width / 1,
                                    height: height / 10,
                                    marginTop: width / 20,
                                    backgroundColor: 'white',
                                    shadowColor: "#000",
                                    shadowOffset: {
                                        width: 0,
                                        height: 3,
                                    },
                                    shadowOpacity: 0.27,
                                    shadowRadius: 4.65,
                                    elevation: 6,
                                }}>
                                <View style={{ marginTop: width / 30, marginLeft: width / 10 }}>
                                    <Text>TINGGI</Text>
                                    <TextInput style={{ color: '#A494EB' }} onChangeText={(text) => this.setState({ tempHeight: text })} placeholder={'Masukkan Tinggi Anak'} keyboardType={'number-pad'}  />
                                </View>
                            </View>
                            <View
                                style={{
                                    width: width / 1,
                                    height: height / 10,
                                    marginTop: width / 20,
                                    backgroundColor: 'white',
                                    shadowColor: "#000",
                                    shadowOffset: {
                                        width: 0,
                                        height: 3,
                                    },
                                    shadowOpacity: 0.27,
                                    shadowRadius: 4.65,
                                    elevation: 6,
                                }}>
                                <View style={{ marginTop: width / 30, marginLeft: width / 10 }}>
                                    <Text>BERAT</Text>
                                    <TextInput style={{ color: '#A494EB' }} onChangeText={(text) => this.setState({ tempWeight: text })} placeholder={'Masukkan Berat Anak'} keyboardType={'number-pad'} />
                                </View>
                            </View>
                        {showDatePicker}
                        <View
                            style={{
                                width: width / 1,
                                height: height / 10,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                            }}>
                            <View style={{ marginTop: width / 30, marginLeft: width / 10, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <View>
                                    <Text>TANGGAL LAHIR</Text>
                                    <TextInput style={{ color: '#A494EB' }} placeholder={'Masukkan Tanggal Lahir'} onChangeText={(text) => this.setState({ date: moment(text).format('YYYY-MM-DD') })} value={moment(this.state.date).format('YYYY-MM-DD')} />
                                </View>
                                <View style={{ marginTop: 5 }}>
                                    <TouchableOpacity onPress={() => this.setState({ showDatePicker: !this.state.showDatePicker })}>
                                        <Icon name="calendar" size={25} style={{ marginRight: width / 10 }} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View
                            style={{
                                width: width / 1,
                                height: height / 8,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                            }}>
                            <View style={{ marginTop: width / 30, marginLeft: width / 10 }}>
                                <Text style={{ marginBottom: 5 }}>JENIS KELAMIN</Text>
                                <RadioForm
                                    radio_props={radio_props}
                                    formHorizontal={false}
                                    labelHorizontal={true}
                                    buttonColor={'#A494EB'}
                                    animation={true}
                                    initial={0}
                                    radioStyle={{ paddingRight: 20 }}
                                    formHorizontal={true}
                                    labelStyle={{ color: '#A494EB' }}
                                    onPress={(value) => this.setState({ tempGender: value })}
                                />
                            </View>
                        </View>
                        <TouchableOpacity
                            onPress={() => this.clickSimpan()}
                            style={{
                                width: width / 1,
                                height: height / 10,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: '#A494EB'
                            }}>
                            <Text style={{ color: 'white', fontWeight: 'bold' }}>SIMPAN</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ height: 50 }} />
                </ScrollView>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
});

//make this component available to the app
export default FormInputAnak;
