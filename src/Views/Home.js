//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, ScrollView, AsyncStorage, ActivityIndicator, NetInfo, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Drawer, Accordion, ActionSheet } from 'native-base';
import SideBar from '../Components/Sidebar';
import { Actions } from 'react-native-router-flux';
import Accounting from '../Components/Accounting';
import Modal from 'react-native-modal';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const GLOBAL = require('../Config/Services');

// create a component
class Home extends Component {
    constructor() {
        super();
        this.state = {
            tempName: '-',
            tempPoint: 0,
            tempWallet: 0,
            tempId: '',
            tempOrderActive:false
        }
    }
    closeDrawer() {
        this._drawer._root.close()
    };
    openDrawer() {
        this._drawer._root.open()
    };
    clickDaycare() {
        Actions.daycareterdekat();
        // Actions.testmap();
    }
    clickDuniaAnak() {
        Actions.duniaanak();
    }
    clickQrcode() {
        Actions.scan();
    }
    clickToko() {
        Actions.toko();
    }
    clickTracking() {
        Actions.tracking();
    }
    formatValue(value) {
        return Accounting.formatMoney(parseFloat(value), "Rp ", 0);
    }
    componentDidMount() {
        this.funcGetUserid();
    }
    funcGetUserid() {
        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {
                // console.log('Internet is connected');
                AsyncStorage.getItem(GLOBAL.APPUSERID).then((value) => {
                    var data = JSON.parse(value);
                    this.funcUserinfo(data);
                    this.funcBiodataParent(data);
                    this.setState({ tempId: data });
                }).done();
            } else {
                this.setState({ isVisible: false })
                Alert.alert('Ruang Anak App', 'Cek Koneksi Internet Anda');
            }
        })
    }
    funcUserinfo(getId) {
        try {
            // this.setState({ isVisible: true });
            fetch(GLOBAL.USERINFO_URL, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': '0',
                    'user-id': getId
                    // 'UserCode': this.state.usercode
                },
            }).then((response) => response.json()).then((responseData) => {
                console.log('cek temp order', responseData)
                if (responseData.status == true) {
                    if (responseData.data.famiMstBioChildren.length <= 0) {
                        Actions.forminputanak();
                    } else {
                        if (responseData.data.saldo != null) {
                            this.setState({
                                isVisible: false,
                                tempWallet: responseData.data.saldo,
                                tempPoint: responseData.data.point,
                                tempIdChild: responseData.data.famiMstBioChildren[0].bioChildFullname,
                                // tempOrderActive: responseData.data
                            });
                        } else {
                            this.setState({
                                tempWallet: 0,
                                tempPoint: 0
                            })
                        }
                    }
                } else {
                    this.setState({ isVisible: false });
                }
            }).done();
        } catch(e) {
            console.error(e.message);
            Alert.alert('Ruang Anak App', e.message)
        }
    }
    funcBiodataParent(getId) {
        try{

        } catch(e) {
            console.error(e.message)
        }
        fetch(GLOBAL.BIODATA_PARENT_URL, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'user-id': getId
                // 'UserCode': this.state.usercode
            },
        }).then((response) => response.json()).then((responseData) => {
            console.log('respon coy', responseData)
            if (responseData.status == true) {
                this.setState({
                    tempName: responseData.data.bioParentFullname,
                    // tempNamaPanggilan: '',
                    // tempEmail: responseData.data.bioParentEmail,
                    // tempPhone: responseData.data.bioParentPhone,
                    // tempAddress: '',
                })
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    render() {
        return (
            <Drawer
                // type="displace"
                ref={(ref) => {
                    this._drawer = ref;
                }}
                content={<SideBar navigator={this.navigator} />}
                onClose={() => this.closeDrawer()}>
                <View style={styles.container}>
                    <ImageBackground source={require('../Assets/Header.png')} style={{ width: width / 1, height: height / 3.3 }}>
                        <View style={{ position: 'absolute', marginTop: width / 10, marginLeft: width / 12 }}>
                            <TouchableOpacity onPress={() => this.openDrawer()}>
                                <Icon name="bars" size={width / 15} color="white" />
                            </TouchableOpacity>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: width / 1.2 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Image source={require('../Assets/Point.png')} style={{ width: width / 17, height: height / 30, marginTop: 10 }} />
                                    <Text style={{ marginTop: width / 30, marginLeft: width / 45, color: 'white' }}>{this.state.tempPoint} POINT</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ marginTop: width / 30, marginRight: width / 45, color: 'white' }}>{this.formatValue(this.state.tempWallet)}</Text>
                                    <Image source={require('../Assets/wallet.png')} style={{ width: width / 17, height: height / 30, marginTop: 10 }} />
                                </View>
                            </View>
                            <Text style={{ color: 'white', fontSize: width / 15, marginTop: 20 }}>{this.state.tempName}</Text>
                        </View>
                    </ImageBackground>
                    <ScrollView style={{ width: width / 1 }} showsVerticalScrollIndicator={false}>
                        <View style={{ flex: 1 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', height: height / 10 }}>
                                <View style={{ alignItems: 'center', width: width / 3, height: height / 10 }}>
                                    <TouchableOpacity
                                        onPress={() => this.clickDaycare()}
                                        style={{
                                            width: width / 5,
                                            height: height / 10,
                                            // backgroundColor: 'white',
                                            // shadowColor: "#000",
                                            // shadowOffset: {
                                            //     width: 0,
                                            //     height: 5,
                                            // },
                                            // shadowOpacity: 0.36,
                                            // shadowRadius: 6.68,
                                            // elevation: 11,
                                            marginTop: width / 15,
                                            // borderRadius:20,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}
                                    >
                                        <Image source={require('../Assets/menudaycare.png')} style={{ width: width / 6, height: height / 10 }} />
                                    </TouchableOpacity>
                                    <Text style={{ marginTop: 10, fontSize: width / 30 }}>DAYCARE</Text>
                                </View>
                                <View style={{ alignItems: 'center', width: width / 3, height: height / 10 }}>
                                    <TouchableOpacity style={{
                                        width: width / 5,
                                        height: height / 10,
                                        // backgroundColor: 'white',
                                        // shadowColor: "#000",
                                        // shadowOffset: {
                                        //     width: 0,
                                        //     height: 5,
                                        // },
                                        // shadowOpacity: 0.36,
                                        // shadowRadius: 6.68,
                                        // elevation: 11,
                                        marginTop: width / 15,
                                        // borderRadius:20,
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}>

                                        <Image source={require('../Assets/menujemputasi.png')} style={{ width: width / 6, height: height / 10 }} />

                                    </TouchableOpacity>
                                    <Text style={{ marginTop: 10, fontSize: width / 30 }}>JEMPUT ASI</Text>
                                </View>
                                <View style={{ alignItems: 'center', width: width / 3, height: height / 10 }}>
                                    <TouchableOpacity
                                        onPress={() => this.clickToko()}
                                        style={{
                                            width: width / 5,
                                            height: height / 10,
                                            // backgroundColor: 'white',
                                            // shadowColor: "#000",
                                            // shadowOffset: {
                                            //     width: 0,
                                            //     height: 5,
                                            // },
                                            // shadowOpacity: 0.36,
                                            // shadowRadius: 6.68,
                                            // elevation: 11,
                                            marginTop: width / 15,
                                            // borderRadius:20,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                        <Image source={require('../Assets/menukiddyshop.png')} style={{ width: width / 6, height: height / 10 }} />
                                    </TouchableOpacity>
                                    <Text style={{ marginTop: 10, fontSize: width / 30 }}>TOKO</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: width / 10, height: height / 5 }}>
                                <View style={{ alignItems: 'center', width: width / 6, height: height / 10 }}>
                                    <TouchableOpacity
                                        onPress={() => this.clickDuniaAnak()}
                                        style={{
                                            width: width / 5,
                                            height: height / 10,
                                            // backgroundColor: 'white',
                                            // shadowColor: "#000",
                                            // shadowOffset: {
                                            //     width: 0,
                                            //     height: 5,
                                            // },
                                            // shadowOpacity: 0.36,
                                            // shadowRadius: 6.68,
                                            // elevation: 11,
                                            marginTop: width / 15,
                                            // borderRadius:20,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}
                                    >
                                        <Image source={require('../Assets/menuduniaanak.png')} style={{ width: width / 6, height: height / 10 }} />
                                    </TouchableOpacity>
                                    <Text style={{ marginTop: 10, fontSize: width / 30 }}>DUNIA ANAK</Text>
                                </View>
                                <View style={{ alignItems: 'center', width: width / 6, height: height / 10 }}>
                                    <TouchableOpacity
                                        onPress={() => this.clickTracking()}
                                        style={{
                                            width: width / 5,
                                            height: height / 10,
                                            // backgroundColor: 'white',
                                            // shadowColor: "#000",
                                            // shadowOffset: {
                                            //     width: 0,
                                            //     height: 5,
                                            // },
                                            // shadowOpacity: 0.36,
                                            // shadowRadius: 6.68,
                                            // elevation: 11,
                                            marginTop: width / 15,
                                            // borderRadius:20,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                        <Image source={require('../Assets/menutracker.png')} style={{ width: width / 6, height: height / 10 }} />
                                    </TouchableOpacity>
                                    <Text style={{ marginTop: 10, fontSize: width / 30 }}>TRACKER</Text>
                                </View>
                                <View style={{ alignItems: 'center', width: width / 6, height: height / 10 }}>
                                    <TouchableOpacity
                                        onPress={() => this.clickQrcode()}
                                        style={{
                                            width: width / 5,
                                            height: height / 10,
                                            // backgroundColor: 'white',
                                            // shadowColor: "#000",
                                            // shadowOffset: {
                                            //     width: 0,
                                            //     height: 5,
                                            // },
                                            // shadowOpacity: 0.36,
                                            // shadowRadius: 6.68,
                                            // elevation: 11,
                                            marginTop: width / 15,
                                            // borderRadius:20,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                        <Image source={require('../Assets/menuqrcode.png')} style={{ width: width / 6, height: height / 10 }} />
                                    </TouchableOpacity>
                                    <Text style={{ marginTop: 10, fontSize: width / 30 }}>QR CODE</Text>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                    {
                        this.state.isVisible == true ?
                            <Modal isVisible={this.state.isVisible}>
                                <ActivityIndicator size={25} color={"#A494EB"} />
                            </Modal>
                            :
                            <View />
                    }
                    {
                        this.state.tempOrderActive == true ?
                        <Modal isVisible={this.state.tempOrderActive}>

                        </Modal>
                        :
                        <View/>
                    }
                </View>
            </Drawer>
            // <CoordinatorLayout style={{ flex: 1 }}>
            //     <View style={{ flex: 1, backgroundColor: 'transparent' }}></View>
            //     <BottomSheetBehavior
            //         ref='bottomSheet'
            //         peekHeight={70}
            //         hideable={false}
            //         state={BottomSheetBehavior.STATE_COLLAPSED}>
            //         <View style={{ backgroundColor: '#4389f2' }}>
            //             <View style={{ padding: 26 }}>
            //                 <Text>BottomSheetBehavior!</Text>
            //             </View>
            //             <View style={{ height: 200, backgroundColor: '#fff' }} />
            //         </View>
            //     </BottomSheetBehavior>
            //     <FloatingActionButton autoAnchor ref="fab" />
            // </CoordinatorLayout>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
});

//make this component available to the app
export default Home;