//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Dimensions, ScrollView, AsyncStorage } from 'react-native';
import StarRating from 'react-native-star-rating';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const GLOBAL = require('../../Config/Services');

// create a component
class Review extends Component {
    constructor(){
        super();
        this.state = {
            names: [
                {
                    id: 0,
                    name: 'Ben',
                    rating: 3.5,
                    description: 'Parkirnya nyaman dan banyak fasilitas. Pemiliknya juga ramah. Kamar mandinya juga bersih dan makanannya enak-enak dan murah.'
                },
                {
                    id: 1,
                    name: 'Susan',
                    rating: 4.0,
                    description: 'Parkirnya nyaman dan banyak fasilitas. Pemiliknya juga ramah. Kamar mandinya juga bersih dan makanannya enak-enak dan murah.'
                },
                {
                    id: 2,
                    name: 'Robert',
                    rating: 5.0,
                    description: 'Parkirnya nyaman dan banyak fasilitas. Pemiliknya juga ramah. Kamar mandinya juga bersih dan makanannya enak-enak dan murah.'
                },
                {
                    id: 3,
                    name: 'Mary',
                    rating: 4.5,
                    description: 'Parkirnya nyaman dan banyak fasilitas. Pemiliknya juga ramah. Kamar mandinya juga bersih dan makanannya enak-enak dan murah.'
                }
            ],
            data:[]
        }   
    }
    onStarRatingPress(rating) {
        this.setState({
            starCount: ''
        });
    }
    componentDidMount() {
        this.getIdDaycare();
    }

    getIdDaycare() {
        AsyncStorage.getItem('daycareId').then((value) => {
            getValue = JSON.parse(value);
            this.setState({
                getIdDaycare: getValue
            })
            this.funcGetReview(getValue);
            // console.log('id ajah',getValue)
        }).done();
    }
    funcGetReview(getValue){
        fetch(GLOBAL.REVIEW_DAYCARE_URL + '/' + getValue, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((response) => response.json()).then((responseData) => {
            if (responseData.status == true) {
                this.setState({
                    isVisible: false,
                    data: responseData.data
                });
                console.log('response', responseData)
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View>
                        {
                            this.state.data.map((item, index) => (
                                <View
                                    // key={item.id}
                                    style={{ width: Dimensions.get('window').width / 1, height: Dimensions.get('window').height / 6, backgroundColor: '#fff', marginTop: width / 20 }}
                                    onPress={() => this.alertItemName(item)}>
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                        <View style={{ flexDirection: 'row' }}>
                                            <View style={{ height: Dimensions.get('window').height / 10, width: Dimensions.get('window').width / 5.5, borderRadius: 50, backgroundColor: '#16ccff' }}>

                                            </View>
                                            <View style={{ marginLeft: Dimensions.get('window').width / 20, marginTop: Dimensions.get('window').width / 15 }}>
                                                <Text style={{ fontWeight: 'bold', color: 'grey' }}>{item.reviewName}</Text>
                                            </View>
                                        </View>
                                        <View style={{ marginTop: Dimensions.get('window').width / 15 }}>
                                            <StarRating
                                                disabled={true}
                                                maxStars={5}
                                                rating={item.reviewRating}
                                                // selectedStar={(rating) => this.onStarRatingPress(rating)}
                                                fullStarColor={'#ffd816'}
                                                starSize={width / 25}
                                            />
                                        </View>
                                    </View>
                                    <View style={{ paddingLeft: 30, paddingRight: 20, paddingTop: 10 }}>
                                        <Text style={{ fontSize: width / 30 }}>{item.reviewNotes}</Text>
                                    </View>
                                </View>
                            ))
                        }
                    </View>
                </ScrollView>
                {/* <View style={{height:80}}/> */}
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default Review;
