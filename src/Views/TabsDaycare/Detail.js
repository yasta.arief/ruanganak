//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, Image, Dimensions, TouchableOpacity, AsyncStorage } from 'react-native';
import StarRating from 'react-native-star-rating';
import { Actions } from 'react-native-router-flux';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const GLOBAL = require('../../Config/Services');

// create a component
class Detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            starCount: 3.5,
            data: '',
            getIdDaycare: ''
        };
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: 3.5
        });
    }

    clickBook() {
        Actions.booknowdetail();
    }

    clickPesan() {
        Actions.detailpembayaran();
    }

    componentDidMount() {
        this.getIdDaycare();
    }

    getIdDaycare() {
        AsyncStorage.getItem('daycareId').then((value) => {
            getValue = JSON.parse(value);
            this.setState({
                getIdDaycare: getValue
            })
            this.funcGetData(getValue);
            // console.log('id ajah',getValue)
        }).done();
    }

    funcGetData(getValue) {
        fetch(GLOBAL.DETAIL_DAYCARE_URL + '/' + getValue, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((response) => response.json()).then((responseData) => {
            if (responseData.status == true) {
                this.setState({
                    isVisible: false,
                    data: responseData.data
                });
                console.log('response', responseData)
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }

    render() {
        return (
            <View style={[styles.scene, { backgroundColor: '#f7f7f7' }]}>
                <View>
                    {
                        this.state.data.daycarePhoto == null
                            ? <Image
                                source={require('../../Assets/noImage.jpeg')}
                                style={{ width: Dimensions.get('window').width / 1, height: Dimensions.get('window').height / 3 }}
                            />
                            : <Image
                                source={{ uri: this.state.data.daycarePhoto, reload: 'cache' }}
                            />
                    }
                </View>
                <ScrollView style={{ marginBottom: 10 }}>
                    <View style={{ marginTop: 20 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-evenly' }}>
                            <Text style={{ fontSize: width / 20, fontWeight: 'bold' }}>{this.state.data.daycareName}</Text>
                            <StarRating
                                disabled={true}
                                maxStars={5}
                                rating={this.state.data.daycareRating}
                                selectedStar={(rating) => this.onStarRatingPress(rating)}
                                fullStarColor={'#ffd816'}
                                starSize={width / 25}
                            />
                        </View>
                        <View style={{ paddingLeft: Dimensions.get('window').width / 12, paddingTop: Dimensions.get('window').width / 35, }}>
                            <Text style={{ color: '#a8a8a8' }}>{this.state.data.daycareAddress}</Text>
                        </View>
                        {/* <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 20 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image source={require('../../Assets/placeholder-merah-muda.png')} style={{ width: Dimensions.get('window').width / 30, height: 20 }} />
                                <Text style={{ marginLeft: 5 }}>1,5 KM</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <Image source={require('../../Assets/home-merah-muda.png')} style={{ width: Dimensions.get('window').width / 25, height: 20 }} />
                                <Text style={{ marginLeft: 5 }}>15 Anak</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <Image source={require('../../Assets/rp.png')} style={{ width: Dimensions.get('window').width / 25, height: 20 }} />
                                <Text style={{ marginLeft: 5 }}>70000/Hari</Text>
                            </View>
                        </View> */}
                        <View style={{ paddingLeft: 30, paddingTop: 20 }}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>DETAILS</Text>
                            <Text>{this.state.data.daycareDescription}</Text>
                        </View>
                        <View style={{ padding: 30 }}>
                            {/* <Text style={{ fontSize: 20, fontWeight: 'bold' }}>FACILITIES</Text> */}
                            {/* <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 20 }}>
                                <View style={{justifyContent:'center', alignItems:'center', width:width/3, height:height/10}}>
                                    <Image source={require('../../Assets/playground.png')} style={{ width: Dimensions.get('window').width / 10, height: 45 }} />
                                    <Text style={{ color: '#16ccff', marginTop: Dimensions.get('window').width / 40 }}>PLAY</Text>
                                </View>
                                <View style={{justifyContent:'center', alignItems:'center', width:width/3, height:height/10}}>
                                    <Image source={require('../../Assets/bed.png')} style={{ width: Dimensions.get('window').width / 10, height: 45, marginLeft: 10 }} />
                                    <Text style={{ color: '#16ccff', marginTop: Dimensions.get('window').width / 40 }}>BED</Text>
                                </View>
                                <View style={{justifyContent:'center', alignItems:'center', width:width/3, height:height/10}}>
                                    <Image source={require('../../Assets/breakfast.png')} style={{ width: Dimensions.get('window').width / 10, height: 45, marginLeft: 10 }} />
                                    <Text style={{ color: '#16ccff', marginTop: Dimensions.get('window').width / 40 }}>MEALS</Text>
                                </View>
                                <View style={{justifyContent:'center', alignItems:'center', width:width/3, height:height/10}}>
                                    <Image source={require('../../Assets/books.png')} style={{ width: Dimensions.get('window').width / 10, height: 45, marginLeft: 10 }} />
                                    <Text style={{ color: '#16ccff', marginTop: Dimensions.get('window').width / 40 }}>BOOKS</Text>
                                </View>
                            </View> */}
                            {/* <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 20 }}>
                                {
                                    this.state.data.daycareFacility.map((item, index) => {
                                        <View style={{ justifyContent: 'center', alignItems: 'center', width: width / 3, height: height / 10 }}>
                                            <Image source={require('../../Assets/meal.png')} style={{ width: Dimensions.get('window').width / 10, height: 45 }} />
                                            <Text style={{ color: '#16ccff', marginTop: Dimensions.get('window').width / 40 }}>{item.name}</Text>
                                        </View>
                                    })
                                }
                            </View> */}
                        </View>
                    </View>
                </ScrollView>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.clickPesan()} style={{ width: Dimensions.get('window').width / 1.2, backgroundColor: '#FD8B7F', height: Dimensions.get('window').height / 15, borderRadius: 20, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontWeight: 'bold', color: '#fff' }}>PESAN</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ height: height / 5 }} />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
    scene: {
        flex: 1,
    },
});

//make this component available to the app
export default Detail;
