//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, ScrollView, AsyncStorage, PermissionsAndroid } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

import Home from './Home';
import Notifikasi from './TabsMama/Notifikasi';
import Pesanan from './TabsMama/Pesanan';
import Tagihan from './TabsMama/Tagihan';
import Profileanak from './TabsAnak/Profileanak';
import Profilemama from './TabsMama/Profilemama';
import PesananHistory from './PesananHistory';

const GLOBAL = require('../Config/Services');

// create a component
class MenuUtama extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isActive: false,
            temp: '',
            photoChlid: false,
            child: false,
            tempIdChild: '',
            tempColorIndicator: 'grey'
        }
    }
    componentDidMount() {
        this.funcGetUserid();
        // this.requestCameraPermission();
    }

    // requestCameraPermission() {
    // try {
    //     const granted = PermissionsAndroid.request(
    //         PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    //         {
    //             title: 'Cool Photo App Camera Permission',
    //             message:
    //                 'Cool Photo App needs access to your camera ' +
    //                 'so you can take awesome pictures.',
    //             buttonNeutral: 'Ask Me Later',
    //             buttonNegative: 'Cancel',
    //             buttonPositive: 'OK',
    //         },
    //     );
    //     if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    //         console.log('You can use the camera');
    //     } else {
    //         console.log('Camera permission denied');
    //     }
    // } catch (err) {
    //     console.warn(err);
    // }
// }
funcView() {
    if (this.state.getTemp == 'pesanan') {
        return (
            <Pesanan />
        )
    } else if (this.state.getTemp == 'home' || this.props.getTemp == 'home') {
        // this.setState({tempColorIndicator:'#A494EB'})
        return (
            <Home />
        )
    } else if (this.state.getTemp == 'tagihan') {
        return (
            <Tagihan />
        )
    } else if (this.state.getTemp == 'notifikasi') {
        return (
            <Notifikasi />
        )
    } else if (this.state.getTemp == 'profile') {
        return (
            <Profileanak />
        )
    } else if (this.props.getTemp == 'profilemama') {
        return (
            <Profilemama />
        )
    } else if (this.props.getTemp == 'pesananhistory') {
        return (
            <PesananHistory />
        )
    } else if (this.props.getTemp == 'tagihan') {
        return (
            <Tagihan />
        )
    } else {
        return (
            <Home />
        )
    }
}
clickMenuAnak() {
    Actions.menuanak();
}

funcGetUserid() {
    AsyncStorage.getItem(GLOBAL.APPUSERID).then((value) => {
        var data = JSON.parse(value);
        this.funcUserinfo(data);
        this.setState({ tempId: data });
    }).done();
}

funcUserinfo(getId) {
    fetch(GLOBAL.USERINFO_URL, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': '0',
            'user-id': getId
            // 'UserCode': this.state.usercode
        },
    }).then((response) => response.json()).then((responseData) => {
        console.log('respon user menuutama', responseData)
        if (responseData.status == true) {
            if(responseData.data.famiMstBioChildren.length <= 0){
                Actions.forminputanak();
            } else {
                this.setState({
                    tempIdChild: responseData.data.famiMstBioChildren[0].bioChildId
                })
                AsyncStorage.setItem(GLOBAL.APPUSERIDCHILD, JSON.stringify(responseData.data.famiMstBioChildren[0].bioChildId))
            }
            // console.warn('cuy', responseData)
        } else {
            this.setState({ isVisible: false });
        }
    }).done();
}


render() {
    return (
        <View style={styles.container}>
            <View style={{ flex: 1 }}>
                {this.funcView()}
            </View>
            <View
                style={{
                    width: width / 1,
                    height: height / 11,
                    justifyContent: 'flex-end',
                    backgroundColor: 'white',
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 3,
                    },
                    shadowOpacity: 0.27,
                    shadowRadius: 4.65,
                    elevation: 6,
                }}>
                <View style={{ flexDirection: 'row', marginLeft: width / 30, marginRight: width / 30, justifyContent: 'space-around' }}>
                    <TouchableOpacity onPress={() => this.setState({ getTemp: 'home' })} style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 20 }}>
                        <Icon
                            name="home"
                            size={25}
                            color={this.state.tempColorIndicator}
                        // color='grey'
                        />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({ getTemp: 'pesanan' })} style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 20 }}>
                        <Image source={require('../Assets/pesanan.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.clickMenuAnak()}
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginBottom: 10,
                            width: width / 5,
                            height: width / 5,
                            borderRadius: width / 10,
                            backgroundColor: '#eaeaea',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 3,
                            },
                            shadowOpacity: 0.27,
                            shadowRadius: 4.65,
                            elevation: 6
                        }}>
                        {/* {
                                this.state.photoChlid == false ?
                                    <Icon name="user" size={20} /> 
                                    : <Image source={{
                                        uri: GLOBAL.GET_PHOTO_URL+'/'+this.state.tempIdChild,
                                        method: 'GET',
                                        headers: {
                                            'Content-type': 'application/json',
                                            'Authorization': '0',
                                            'UserCode': this.state.tempId
                                        },
                                        cache: 'reload'
                                    }}
                                        style={{ height: 100, width: 100, borderRadius: 50 }} /> 
                            } */}
                        <Image source={{
                            uri: GLOBAL.GET_PHOTO_URL + '/' + this.state.tempIdChild,
                            method: 'GET',
                            headers: {
                                'Content-type': 'application/json',
                                'user-id': this.state.tempId
                            },
                            cache: 'reload'
                        }}
                            style={{ height: width / 5.5, width: width / 5.5, borderRadius: width / 12.5 }} />
                        {/* <Image source={{
                                uri: 'https://reactnativecode.com/wp-content/uploads/2017/05/react_thumb_install.png',
                            }}
                                style={{ height: width/5.5, width: width/5.5, borderRadius: width/12.5 }} /> */}
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({ getTemp: 'tagihan' })} style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 20 }}>
                        <Image source={require('../Assets/tagihan.png')} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.setState({ getTemp: 'notifikasi' })} style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 20 }}>
                        <Icon name="bell" size={25} color="grey" />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 36
    }
});

//make this component available to the app
export default MenuUtama;
