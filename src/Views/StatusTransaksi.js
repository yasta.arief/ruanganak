//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, ScrollView, AsyncStorage } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
var moment = require('moment');
import Accounting from '../Components/Accounting';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const GLOBAL = require('../Config/Services');

// create a component
class StatusTransaksi extends Component {
    constructor() {
        super();
        this.state = {
            text: 'ARIEF ADVANI',
            orderStart: '',
            orderEnd: '',
            orderCode: '',
            orderPrizeTotal: '',
            orderStatus: ''
        }
    }
    componentDidMount() {
        this.funcGetUserid();
    }
    funcGetUserid() {
        AsyncStorage.getItem(GLOBAL.APPUSERID).then((value) => {
            var data = JSON.parse(value);
            this.funcGetDetailInvoice(data);
            this.setState({ tempId: data });
        }).done();
    }
    funcGetDetailInvoice(getData) {
        fetch(GLOBAL.DETAIL_INVOICE_URL + '/' + this.props.getOrderId, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': '0',
                'user-id': getData
            },
        }).then((response) => response.json()).then((responseData) => {
            console.log('detail list', responseData);
            if (responseData.status == true) {
                this.setState({
                    orderCode: responseData.data.orderCode,
                    orderStart: responseData.data.orderStart,
                    orderEnd: responseData.data.orderEnd,
                    orderPrizeTotal: responseData.data.orderPriceTotal,
                    orderStatus: responseData.data.orderStatus
                })
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    clickBack() {
        Actions.menuutama();
    }
    clickMenuUtama() {
        Actions.menuutama();
    }
    formatValue(value) {
        return Accounting.formatMoney(parseFloat(value), "Rp ", 0);
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: width / 10 }}>
                        <View>
                            {/* <TouchableOpacity onPress={() => this.clickBack()} style={{ marginLeft: width / 20 }}>
                                <Icon name='arrow-left' size={25} color="white" />
                            </TouchableOpacity> */}
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, color: 'white' }}>STATUS TRANSAKSI</Text>
                        </View>
                        <View>
                            <TouchableOpacity>
                                {/* <Image source={require('../Assets/history.png')} /> */}
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 20 }}>
                    {
                        this.state.orderStatus == 0 ?
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 25, color: '#FD8B7F' }}>PEMBAYARAN !</Text>
                                <Text style={{ fontSize: 15, color: '#FD8B7F', marginTop: width / 25 }}>Mohon untuk melakukan pembayaran</Text>
                                {/* <Image source={require('../Assets/selamat.png')} style={{ marginTop: width / 20 }} /> */}
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: width / 6, height: width / 6, borderRadius: width / 10, backgroundColor: '#ed2f2f', marginTop: width / 20 }}>
                                    <Icon color={"white"} size={25} name="times" />
                                </View>
                                {/* <Text style={{ marginTop: 15 }}>Pembayaran Bunda sudah kami terima.</Text>
                                <Text style={{ marginTop: 5 }}>Tunggu Notifikasi kami untuk</Text>
                                <Text style={{ marginTop: 5 }}>Pemesanan Daycare bunda ya</Text> */}
                            </View> :
                            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 25, color: '#FD8B7F' }}>SELAMAT !</Text>
                                <Text style={{ fontSize: 15, color: '#FD8B7F', marginTop: width / 25 }}>Transaksi Kamu Berhasil</Text>
                                {/* <Image source={require('../Assets/selamat.png')} style={{ marginTop: width / 20 }} /> */}
                                <View style={{ justifyContent: 'center', alignItems: 'center', width: width / 6, height: width / 6, borderRadius: width / 10, backgroundColor: '#18db2f', marginTop: width / 20 }}>
                                    <Icon color={"white"} size={25} name="check" />
                                </View>
                                {/* <Text style={{ marginTop: 15 }}>Pembayaran Bunda sudah kami terima.</Text>
                                <Text style={{ marginTop: 5 }}>Tunggu Notifikasi kami untuk</Text>
                                <Text style={{ marginTop: 5 }}>Pemesanan Daycare bunda ya</Text> */}
                            </View>
                    }
                    <View
                        style={{
                            width: width / 1.3,
                            height: height / 4.5,
                            marginTop: width / 10,
                            backgroundColor: 'white',
                            shadowColor: "#A494EB",
                            shadowOffset: {
                                width: 3,
                                height: 5,
                            },
                            shadowOpacity: 1.27,
                            shadowRadius: 2.65,
                            elevation: 6,
                            borderRadius: 30,
                        }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: width / 30 }}>
                            <Text style={{ color: '#A494EB' }}>Id Transaksi : </Text>
                            <Text style={{ color: '#A494EB' }}>{this.state.orderCode}</Text>
                        </View>
                        <View>
                            <View style={{ padding: width / 20, paddingTop: width / 30, paddingBottom: width / 30 }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: width / 50 }}>
                                    <Text style={{ color: '#FD8B7F' }}>Total Pembayaran</Text>
                                    <Text style={{ color: '#FD8B7F' }}>{this.formatValue(this.state.orderPrizeTotal)}</Text>
                                </View>
                                <View style={{ width: width / 1.5, height: 2, backgroundColor: '#A494EB' }} />
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: width / 50 }}>
                                    <Text style={{ color: '#FD8B7F' }}>Tanggal Pemesanan</Text>
                                    <Text style={{ color: '#FD8B7F' }}>{moment(this.state.orderStart).format('YYYY-MMMM-DD')}</Text>
                                </View>
                                <View style={{ width: width / 1.5, height: 2, backgroundColor: '#A494EB', marginTop: width / 30 }} />
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: width / 50 }}>
                                    <Text style={{ color: '#FD8B7F' }}>Tanggal Akhir Pesan</Text>
                                    <Text style={{ color: '#FD8B7F' }}>{moment(this.state.orderEnd).format('YYYY-MMMM-DD')}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 20 }}>
                    {/* <TouchableOpacity style={{ width: Dimensions.get('window').width / 1.3, backgroundColor: '#FD8B7F', height: Dimensions.get('window').height / 15, borderRadius: 20, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontWeight: 'bold', color: '#fff' }}>CHAT DAYCARE</Text>
                    </TouchableOpacity> */}
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 20 }}>
                    <TouchableOpacity onPress={() => Actions.menuutama()} style={{ width: Dimensions.get('window').width / 1.3, backgroundColor: '#FD8B7F', height: Dimensions.get('window').height / 15, borderRadius: 20, justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontWeight: 'bold', color: '#fff' }}>KEMBALI KE MENU UTAMA</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default StatusTransaksi;
