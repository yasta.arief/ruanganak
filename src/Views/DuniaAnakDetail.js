//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, Image, Dimensions, TouchableOpacity } from 'react-native';
import StarRating from 'react-native-star-rating';
import Icon from 'react-native-vector-icons/FontAwesome'
import { Actions } from 'react-native-router-flux';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

// create a component
class Detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            starCount: 3.5
        };
    }

    onStarRatingPress(rating) {
        this.setState({
            starCount: 3.5
        });
    }

    clickBack(){
        Actions.duniaanak();
    }

    render() {
        return (
            <View style={[styles.scene, { backgroundColor: '#f7f7f7' }]}>
                <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: width / 10 }}>
                        <View>
                            <TouchableOpacity onPress={()=>this.clickBack()} style={{marginLeft:width/30}}>
                                <Icon name='arrow-left' size={25} color="white" />
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, color: 'white' }}>DUNIA ANAK</Text>
                        </View>
                        <View>
                            <TouchableOpacity>
                                {/* <Image source={require('../../Assets/history.png')} /> */}
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View>
                    <Image
                        source={require('../Assets/gambar-besar-dunia-anak.png')}
                        style={{ width: Dimensions.get('window').width / 1, height: Dimensions.get('window').height / 3 }}
                    />
                </View>
                <ScrollView style={{marginBottom:10}}>
                    <View style={{ marginTop: 20 }}>
                        <View style={{ justifyContent:'center', alignItems:'center', width:width/1.2, marginLeft:width/13 }}>
                            <Text style={{ fontSize: width/15, fontWeight: 'bold', color:'#A494EB' }}>Disney on Ice - Mickey Super Celebretion</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: 20 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <Image source={require('../Assets/calendar-ungu.png')} style={{ width: Dimensions.get('window').width / 20, height: 20 }} />
                                <Text style={{ marginLeft: 5, fontSize:width/30 }}>24 - 28 April</Text>
                            </View>
                            <View style={{ flexDirection: 'row' }}>
                                <Image source={require('../Assets/placeholder-ungu.png')} style={{ width: Dimensions.get('window').width / 27, height: 20 }} />
                                <Text style={{ marginLeft: 5, fontSize:width/30 }}>ICE BSD, TANGGERANG</Text>
                            </View>
                        </View>
                        <View style={{ paddingLeft: 30, paddingTop: 20, paddingRight:20 }}>
                            <Text style={{ fontSize: 20, fontWeight: 'bold', color:'#A494EB' }}>DETAILS</Text>
                            <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 Ut malesuada nulla in ex sodales venenatis sed nec ipsum.
 Morbi tincidunt tortor eget arcu cursus, eget gravida nunc pharetra.  Praesent iaculis ultricies libero, commodo tempor erat rutrum eget. Morbi non mauris tempus, tincidunt purus aliquam, commodo turpis.  Cras ultricies arcu nibh. Nam et lacus quam. Suspendisse id volutpat arcu, vitae dictum diam. Mauris posuere risus ac arcu iaculis aliquet.</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
    scene: {
        flex: 1,
    },
});

//make this component available to the app
export default Detail;
