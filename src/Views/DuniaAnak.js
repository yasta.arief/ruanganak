//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, ScrollView, TextInput, Platform, Linking, PermissionsAndroid } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
import MapView, {
    Marker,
    AnimatedRegion,
    Polyline,
    PROVIDER_GOOGLE
} from 'react-native-maps';
import haversine from "haversine";

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

const GLOBAL = require('../Config/Services');

const LATITUDE_DELTA = 0.009;
const LONGITUDE_DELTA = 0.009;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;

// create a component
class DuniaAnak extends Component {
    state = {
        // aktivitas: [
        //     {
        //         id: 0,
        //         name: 'Disney on ice - Mickey Super Celebration',
        //         image: require('../Assets/gambar-besar-dunia-anak.png'),
        //         tanggal: '24 - 28 April',
        //         lokasi: 'ICE BSD, TANGGERANG'
        //     },
        //     {
        //         id: 1,
        //         name: 'Disney on ice - Mickey Super Celebration',
        //         image: require('../Assets/gambar-besar-dunia-anak.png'),
        //         tanggal: '24 - 28 April',
        //         lokasi: 'ICE BSD, TANGGERANG'
        //     },
        //     {
        //         id: 2,
        //         name: 'Disney on ice - Mickey Super Celebration',
        //         image: require('../Assets/gambar-besar-dunia-anak.png'),
        //         tanggal: '24 - 28 April',
        //         lokasi: 'ICE BSD, TANGGERANG'
        //     },
        // ]

        data: [],
        latitude: '',
        longitude: '',
        routeCoordinates: [],
        distanceTravelled: 0,
        prevLatLng: {},
        coordinate: new AnimatedRegion({
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: 0,
            longitudeDelta: 0
        })
    }
    alertItemName = (item) => {
        alert(item.name)
    }
    clickDuniaAnakDetail(getValue) {
        // Actions.duniaanakdetail();
        Linking.openURL(getValue);
    }
    clickBack() {
        Actions.menuutama();
    }
    componentDidMount = () => {
        var that = this;
        //Checking for the permission just after component loaded
        if (Platform.OS === 'ios') {
            this.callLocation(that);
        } else {
            async function requestLocationPermission() {
                try {
                    const granted = await PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
                            'title': 'Location Access Required',
                            'message': 'This App needs to Access your location'
                        }
                    )
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        //To Check, If Permission is granted
                        that.callLocation(that);
                    } else {
                        alert("Permission Denied");
                    }
                } catch (err) {
                    // alert("err", err);
                    console.warn(err)
                }
            }
            requestLocationPermission();
        }
    }
    callLocation(that) {
        //alert("callLocation Called");
        navigator.geolocation.getCurrentPosition(
            //Will give you the current location
            (position) => {
                const currentLongitude = JSON.stringify(position.coords.longitude);
                //getting the Longitude from the location json
                const currentLatitude = JSON.stringify(position.coords.latitude);
                //getting the Latitude from the location json
                that.setState({ latitude: currentLongitude });
                //Setting state Longitude to re re-render the Longitude Text
                that.setState({ longitude: currentLatitude });
                //Setting state Latitude to re re-render the Longitude Text
                that.funcNearbyDuniaanak(currentLatitude, currentLongitude)
            },
            (error) => console.log(error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
        that.watchID = navigator.geolocation.watchPosition((position) => {
            //Will give you the location on location change
            console.log(position);
            const currentLongitude = JSON.stringify(position.coords.longitude);
            //getting the Longitude from the location json
            const currentLatitude = JSON.stringify(position.coords.latitude);
            //getting the Latitude from the location json
            that.setState({ currentLongitude: currentLongitude });
            //Setting state Longitude to re re-render the Longitude Text
            that.setState({ currentLatitude: currentLatitude });
            //Setting state Latitude to re re-render the Longitude Text
            that.funcNearbyDuniaanak(currentLatitude, currentLongitude)
        });
    }
    componentWillUnmount = () => {
        navigator.geolocation.clearWatch(this.watchID);
    }
    funcNearbyDuniaanak(getLat, getLong){
        fetch(GLOBAL.MARKET_PLAYGROUND_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                // 'UserCode': this.state.usercode
            },
            body: JSON.stringify({
                // 'lat': tempLat,
                // 'lon': tempLon,
                // "lat": 37.785834,
                // "lon": -122.406417,
                'lat': getLat,
                'lon': getLong,
                'radius': 5000
            })
        }).then((response) => response.json()).then((responseData) => {
            if (responseData.status == true) {
                this.setState({
                    isVisible: false, 
                    data: responseData.data
                });
                console.log('respon cuyy', responseData)
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    textKM(getValue) {
        const tempValue = parseFloat(getValue) / 1000
        const tempValue2 = tempValue.toString().substr(0, 7)
        // const tempValue = getValue.substr(0, 7)
        return (
            <Text style={{ fontWeight: 'bold' }}>{tempValue2} KM</Text>
        )
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: width / 10 }}>
                        <View style={{ marginLeft: width / 30 }}>
                            <TouchableOpacity onPress={() => this.clickBack()}>
                                <Icon name='arrow-left' size={25} color="white" />
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, color: 'white' }}>DUNIA ANAK</Text>
                        </View>
                        <View>
                            <TouchableOpacity>
                                {/* <Image source={require('../../Assets/history.png')} /> */}
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                {/* <View style={{ alignItems: 'center' }}>
                    <View
                        style={{
                            width: width / 1.1,
                            height: height / 18,
                            backgroundColor: 'rgba(209, 209, 209, 0.5)',
                            borderRadius: width / 20 - 18,
                            marginTop: width / 30,
                            borderWidth: 1,
                            borderColor: '#bababa',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                        <View style={{ flexDirection: 'row', alignItems:'center' }}>
                            <Icon name="search" size={20} style={{ marginLeft: width / 20 }} color={'#bababa'} />
                            <TextInput style={{ width: width / 1.2, marginLeft: width / 30 }} placeholder={'Cari Dunia Anak'} />
                        </View>
                    </View>
                </View> */}
                <ScrollView>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        {
                            this.state.data.map((item, index) => (
                                <TouchableOpacity
                                    // key={item.id}
                                    onPress={() => this.clickDuniaAnakDetail(item.link)}
                                    style={{
                                        width: width / 1.1,
                                        height: height / 3,
                                        marginTop: width / 20,
                                        backgroundColor: 'white',
                                        shadowColor: "#000",
                                        shadowOffset: {
                                            width: 0,
                                            height: 3,
                                        },
                                        shadowOpacity: 0.27,
                                        shadowRadius: 4.65,
                                        elevation: 6,
                                        borderRadius: width / 20
                                    }}>
                                    <View style={{ alignItems: 'center', justifyContent: 'center', borderTopRightRadius: width / 20, borderTopLeftRadius: width / 20 }}>
                                        <Image source={require('../Assets/noImage.jpeg')} style={{ width: width / 1.1, height: height / 5 }} />
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ marginTop: width / 30, marginLeft: width / 30 }}>
                                            <Text style={{ width: width / 1.2, color: '#A494EB', fontSize: width / 30 }}>{item.marketName}</Text>
                                            <Text style={{ marginTop: 5, fontSize: width / 30 }}>{item.marketCategory}</Text>
                                            {/* <Text style={{ marginTop: 5, fontSize: width / 30 }}>{parseFloat(item.meter)/100} KM</Text> */}
                                            {this.textKM(item.meter)}
                                        </View>
                                        {/* <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                            <Image source={require('../Assets/more-dunia-anak.png')} />
                                        </View> */}
                                    </View>
                                </TouchableOpacity>
                            ))
                        }
                    </View>
                    <View style={{height:100}}/>
                </ScrollView>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default DuniaAnak;
