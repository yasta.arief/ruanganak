//daycare terdekat
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, ScrollView, Animated, AsyncStorage, Platform, TextInput, PermissionsAndroid } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import MapView, {
    Marker,
    AnimatedRegion,
    Polyline,
    PROVIDER_GOOGLE
} from 'react-native-maps';
import haversine from "haversine";
import StarRating from 'react-native-star-rating';
import { Actions } from 'react-native-router-flux';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

const CARD_HEIGHT = height / 5;
const CARD_WIDTH = width / 1.5;

const LATITUDE_DELTA = 0.009;
const LONGITUDE_DELTA = 0.009;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;

const GLOBAL = require('../Config/Services');

// create a component
class DaycareTerdekat extends Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         data: [],
    //         isStatus: false,
    //         mapRegion: null,
    //         lastLat: null,
    //         lastLong: null,
    //     }
    // }
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            latitude: LATITUDE,
            longitude: LONGITUDE,
            routeCoordinates: [],
            distanceTravelled: 0,
            prevLatLng: {},
            coordinate: new AnimatedRegion({
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: 0,
                longitudeDelta: 0
            }),
        };
        this.arrayholder = [];
    }

    componentDidMount = () => {
        var that = this;
        //Checking for the permission just after component loaded
        if (Platform.OS === 'ios') {
            this.callLocation(that);
        } else {
            async function requestLocationPermission() {
                try {
                    const granted = await PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
                            'title': 'Location Access Required',
                            'message': 'This App needs to Access your location'
                        }
                    )
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        //To Check, If Permission is granted
                        that.callLocation(that);
                    } else {
                        alert("Permission Denied");
                    }
                } catch (err) {
                    alert("err", err);
                    console.warn(err)
                }
            }
            requestLocationPermission();
        }
    }
    callLocation(that) {
        //alert("callLocation Called");
        navigator.geolocation.getCurrentPosition(
            //Will give you the current location
            (position) => {
                const currentLongitude = JSON.stringify(position.coords.longitude);
                //getting the Longitude from the location json
                const currentLatitude = JSON.stringify(position.coords.latitude);
                //getting the Latitude from the location json
                that.setState({ latitude: currentLongitude });
                //Setting state Longitude to re re-render the Longitude Text
                that.setState({ longitude: currentLatitude });
                //Setting state Latitude to re re-render the Longitude Text
                that.funcNearbyDaycare(currentLatitude, currentLongitude)
            },
            (error) => console.log(error.message),
            // { enableHighAccuracy: true, timeout: 360000, maximumAge: 1000 }
            {
                enableHighAccuracy: false,
                timeout: 5000,
                maximumAge: 10000
            }
        );
        that.watchID = navigator.geolocation.watchPosition((position) => {
            //Will give you the location on location change
            console.log(position);
            const currentLongitude = JSON.stringify(position.coords.longitude);
            //getting the Longitude from the location json
            const currentLatitude = JSON.stringify(position.coords.latitude);
            //getting the Latitude from the location json
            that.setState({ currentLongitude: currentLongitude });
            //Setting state Longitude to re re-render the Longitude Text
            that.setState({ currentLatitude: currentLatitude });
            //Setting state Latitude to re re-render the Longitude Text
            that.funcNearbyDaycare(currentLatitude, currentLongitude)
        });
    }
    componentWillUnmount = () => {
        navigator.geolocation.clearWatch(this.watchID);
    }

    funcGetUserid() {
        AsyncStorage.getItem(GLOBAL.APPUSERID).then((value) => {
            var data = JSON.parse(value);
        }).done();
    }

    // funcNearbyDaycare(getLat, getLong){
    funcNearbyDaycare(getLat, getLong) {
        // console.log('response', this.state.lastLat)
        console.log(' state latitude', this.state.latitude, ' state longitude', this.state.longitude)
        // var tempLat = parseFloat(getLat);
        // var tempLon = parseFloat(getLong);
        fetch(GLOBAL.NEARBY_DAYCARE_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                // 'UserCode': this.state.usercode
            },
            body: JSON.stringify({
                // 'lat': tempLat,
                // 'lon': tempLon,
                // "lat": 37.785834,
                // "lon": -122.406417,
                'lat': getLat,
                'lon': getLong,
                'radius': 10000
            })
        }).then((response) => response.json()).then((responseData) => {
            if (responseData.status == true) {
                this.setState({
                    isVisible: false,
                    data: responseData.data,
                    IdDaycare: responseData.data.daycareId
                },function() {
                    this.arrayholder = responseData;
                });
                console.log('response dong', responseData)
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }

    clickBack() {
        Actions.menuutama();
    }

    clickDaycareDetail(getValue) {
        AsyncStorage.setItem('daycareId', JSON.stringify(getValue));
        Actions.daycareterdekatdetail();
        // alert(getValue)
    }

    textKM(getValue) {
        const tempValue = parseFloat(getValue) / 1000
        const tempValue2 = tempValue.toString().substr(0, 7)
        // const tempValue = getValue.substr(0, 7)
        return (
            <Text style={{ fontWeight: 'bold' }}>{tempValue2} KM</Text>
        )
    }

    SearchFilterFunction(text) {
        const newData = this.arrayholder.filter(function (item) {
            const itemData = item.data.daycareName.toUpperCase()
            const textData = text.toUpperCase()
            return itemData.indexOf(textData) > -1
        })
        this.setState({
            data: newData,
            text: text
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: width / 10 }}>
                        <View>
                            <TouchableOpacity onPress={() => this.clickBack()} style={{ marginLeft: width / 20 }}>
                                <Icon name='arrow-left' size={25} color="white" />
                                {/* <Image source={require('../../../Assets/left-arrow.png')} /> */}
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, color: 'white' }}>DAYCARE</Text>
                        </View>
                        <View>
                            <TouchableOpacity>
                                {/* <Image source={require('../../../Assets/history.png')} /> */}
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={{ alignItems: 'center', marginLeft: width / 30, marginRight: width / 30 }}>
                    {/* <View
                        style={{
                            width: width / 1.1,
                            height: height / 18,
                            backgroundColor: 'rgba(209, 209, 209, 0.5)',
                            borderRadius: width / 20 - 18,
                            marginTop: width / 30,
                            borderWidth: 1,
                            borderColor: '#bababa',
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Icon name="search" size={20} style={{ marginLeft: width / 20 }} color={'#bababa'} />
                            <TextInput 
                                // onChangeText={(text) => this.SearchFilterFunction(text)} 
                                style={{ width: width / 1.2, marginLeft: width / 30 }} 
                                placeholder={'Cari daycare terdekat'} />
                        </View>
                    </View> */}
                    <ScrollView showsVerticalScrollIndicator={false} style={{ marginTop: width / 30 }}>
                        <View style={{ flex: 1, width: width / 1.1, height: height / 1 }}>
                            {
                                this.state.data.map((item, index) =>
                                    <TouchableOpacity
                                        onPress={() => this.clickDaycareDetail(item.daycareId)}
                                        style={{
                                            flexDirection: 'row',
                                            marginTop: width / 20,
                                            width: width / 1.2,
                                            height: height / 5,
                                            borderRadius: width / 20,
                                            shadowColor: "#000",
                                            shadowOffset: {
                                                width: 0,
                                                height: 2,
                                            },
                                            shadowOpacity: 0.25,
                                            shadowRadius: 2.84,
                                            elevation: 5,
                                            marginLeft: width / 25
                                        }}>
                                        <View style={{ justifyContent: 'center' }}>
                                            <Image source={require('../Assets/menudaycare.png')} style={{ width: width / 4, height: height / 10, borderRadius: width / 30, marginLeft: width / 25 }} />
                                        </View>
                                        <View style={{ marginLeft: width / 20, justifyContent: 'center' }}>
                                            <Text style={{ fontWeight: 'bold' }}>{item.daycareName}</Text>
                                            {/* <Text>{item.daycareName}</Text> */}
                                            <Text numberOfLines={2} style={{ width: width / 2.5 }}>{item.daycareAddress}</Text>
                                            <View style={{ width: width / 4 }}>
                                                <StarRating
                                                    disabled={true}
                                                    maxStars={5}
                                                    rating={item.rating}
                                                    // selectedStar={(rating) => this.onStarRatingPress(rating)}
                                                    fullStarColor={'#ffd816'}
                                                    starSize={width / 25}
                                                />
                                                {this.textKM(item.meter)}
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }
                        </View>
                        <View style={{ height: 100 }} />
                    </ScrollView>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    map: {
        flex: 1,
        width: Dimensions.get('window').width / 1,
        height: Dimensions.get('window').height / 10
    },
    inputView: {
        backgroundColor: 'rgba(0,0,0,0)',
        position: 'absolute',
        top: 20,
        left: 5,
        right: 5
    },
    input: {
        height: 40,
        padding: 10,
        marginTop: 20,
        marginLeft: 10,
        marginRight: 10,
        fontSize: 18,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#48BBEC',
        backgroundColor: 'white',
    },
    scrollView: {
        position: "absolute",
        bottom: 30,
        left: 0,
        right: 0,
        paddingVertical: 10,
    },
    endPadding: {
        paddingRight: width - CARD_WIDTH,
    },
    card: {
        padding: 10,
        elevation: 2,
        backgroundColor: "#FFF",
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: { x: 2, y: -2 },
        height: CARD_HEIGHT,
        width: CARD_WIDTH,
        overflow: "hidden",
    },
    cardImage: {
        flex: 3,
        width: "100%",
        height: "100%",
        alignSelf: "center",
    },
    textContent: {
        flex: 1,
    },
    cardtitle: {
        fontSize: 12,
        marginTop: 5,
        fontWeight: "bold",
    },
    cardDescription: {
        fontSize: 12,
        color: "#444",
        marginTop: 10
    },
    markerWrap: {
        alignItems: "center",
        justifyContent: "center",
    },
    marker: {
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: "rgba(130,4,150, 0.9)",
    },
    ring: {
        width: 24,
        height: 24,
        borderRadius: 12,
        backgroundColor: "rgba(130,4,150, 0.3)",
        position: "absolute",
        borderWidth: 1,
        borderColor: "rgba(130,4,150, 0.5)",
    },
});

//make this component available to the app
export default DaycareTerdekat;