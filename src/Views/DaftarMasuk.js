//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

// create a component
class DaftarMasuk extends Component {
    clickDaftar(){
        Actions.daftar();
    }
    clickMasuk(){
        Actions.login();
    }
    render() {
        return (
            <View style={styles.container}>
                <Image source={require('../Assets/KeluargaDaftar.png')} style={{width:width/1, transform: [{ scale: 1.1 }]}}/>
                <View style={{justifyContent:'center', alignItems:'center', marginTop:width/4}}>
                    <TouchableOpacity onPress={()=>this.clickDaftar()} style={{justifyContent:'center', alignItems:'center',borderWidth:1, borderColor:'#FD8B7F', borderRadius:20, width:width/1.5, height:height/16, backgroundColor:'#FD8B7F'}}>
                        <Text style={{color:'white', fontSize:15, fontWeight:'bold'}}>DAFTAR</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.clickMasuk()} style={{justifyContent:'center', alignItems:'center',borderWidth:1, borderColor:'#FD8B7F', borderRadius:20, width:width/1.5, height:height/16, backgroundColor:'white', marginTop:20}}>
                        <Text style={{color:'#FD8B7F', fontSize:15, fontWeight:'bold'}}>MASUK</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#A494EB',
    },
});

//make this component available to the app
export default DaftarMasuk;
