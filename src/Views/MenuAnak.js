//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, ScrollView, AsyncStorage } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

import Notifikasi from './TabsMama/Notifikasi';
import Aktifitas from './TabsAnak/Aktifitas';
import Perkembangan from './TabsAnak/Perkembangan';
import Profilemama from './TabsMama/Profilemama';
import Profileanak from './TabsAnak/Profileanak';
import FormInputAnak from './FormInputAnak';
// import FormInputAnak from './FormInputAnak';

const GLOBAL = require('../Config/Services');

// create a component
class MenuAnak extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isActive: false,
            temp: '',
            photoChlid: false,
            child: false
        }
    }

    componentDidMount() {
        this.funcGetUserid();
    }

    funcGetUserid() {
        AsyncStorage.getItem(GLOBAL.APPUSERID).then((value) => {
            var data = JSON.parse(value);
            this.funcUserinfo(data);
            this.setState({
                tempId:data
            })
        }).done();
    }

    funcUserinfo(getId) {
        fetch(GLOBAL.USERINFO_URL, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': '0',
                'user-id': getId
                // 'UserCode': this.state.usercode
            },
        }).then((response) => response.json()).then((responseData) => {
            if (responseData.status == true) {
                this.setState({
                    isVisible: false,
                    tempWallet: responseData.data.saldo,
                    child: responseData.data.child
                });
                this.funcView(responseData.data.child)
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }

    funcView(getChild) {
        if (this.state.getTemp == 'aktifitas') {
            return (
                <Aktifitas />
            )
        } else if (this.state.getTemp == 'perkembangan') {
            return (
                <Perkembangan />
            )
        } else if (this.state.getTemp == 'notifikasi') {
            return (
                <Notifikasi />
            )
        } else if (this.state.getTemp == 'profile') {
            Actions.menuutama({ getTemp: 'profilemama' });
        } else if (this.props.getTemp == 'kembali_perkembangan') {
            return (
                <Perkembangan />
            )
        } else {
            if (this.state.child == null) {
                return (
                    <FormInputAnak />
                )
            } else {
                return (
                    <Profileanak />
                )
            }
        }
    }
    clickMenuutama() {
        Actions.menuutama();
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{ flex: 1 }}>
                    {this.funcView()}
                </View>
                <View
                    style={{
                        width: width / 1,
                        height: height / 11,
                        justifyContent: 'flex-end',
                        backgroundColor: 'white',
                        shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 3,
                        },
                        shadowOpacity: 0.27,
                        shadowRadius: 4.65,
                        elevation: 6,
                    }}>
                    <View style={{ flexDirection: 'row', marginLeft: width / 30, marginRight: width / 30, justifyContent: 'space-around' }}>
                        <TouchableOpacity onPress={() => this.clickMenuutama()} style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 20 }}>
                            <Icon name="home" size={25} color="grey" />
                            {/* <Text>Beranda</Text> */}
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ getTemp: 'aktifitas' })} style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 20 }}>
                            <Image source={require('../Assets/kalender.png')} />
                            {/* <Text>Aktifitas</Text> */}
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ getTemp: 'profile' })}
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginBottom: 10,
                                width: width / 5,
                                height: width / 5,
                                borderRadius: width / 10,
                                backgroundColor: '#eaeaea',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6
                            }}>
                            {/* {
                                    this.state.photoChlid == false ?
                                    <Icon name="user" size={20} />:<Image source={require('../Assets/bunda.png')} style={{width: width / 4.9, height: height / 8.5, borderRadius:width/10}}/>
                                } */}
                            <Image source={{
                                uri: GLOBAL.GET_PHOTO_URL,
                                method: 'GET',
                                headers: {
                                    'Content-type': 'application/x-www-form-urlencoded',
                                    'user-id': this.state.tempId
                                },
                                cache: 'reload'
                            }}
                                style={{ height: width / 5.5, width: width / 5.5, borderRadius: width / 12.5 }} />
                            {/* <Text>Home</Text> */}
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ getTemp: 'perkembangan' })} style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 20 }}>
                            <Image source={require('../Assets/perkembangan.png')} />
                            {/* <Text>Perkembangan</Text> */}
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({ getTemp: 'notifikasi' })} style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 20 }}>
                            <Icon name="bell" size={25} color="grey" />
                            {/* <Text>Notifikasi</Text> */}
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
    bottom: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 36
    }
});

//make this component available to the app
export default MenuAnak;
