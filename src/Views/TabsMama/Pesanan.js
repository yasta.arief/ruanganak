//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Image, ScrollView, AsyncStorage, RefreshControl } from 'react-native';
import Icon from 'react-native-fontawesome';
import { Actions } from 'react-native-router-flux';
import PesananHistory from '../PesananHistory';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

const GLOBAL = require('../../Config/Services')

// create a component
class Pesanan extends Component {
    constructor(){
        super();
        this.state = {
            aktivitas: [
                {
                    id: 0,
                    name: 'JEMPUT ASI',
                    image: '../../Assets/menujemputasi.png',
                    tanggal: '12 April, 11:00 AM',
                    deskripsi: 'Driver sedang menuju lokasimu untuk mengambil Asi'
                },
                {
                    id: 1,
                    name: 'JEMPUT ANAK',
                    image: '../../Assets/menutracker.png',
                    tanggal: '12 April, 11:00 AM',
                    deskripsi: 'Driver sedang menjemputmu'
                },
                {
                    id: 2,
                    name: 'OWN DAYCARE',
                    image: '../../Assets/menudaycare.png',
                    tanggal: '12 April, 11:00 AM',
                    deskripsi: 'Pesanan kamu sedang di proses'
                },
            ],
            tempUserId: '',
            tempAktivitas: [],
            tempClickHistory: '',
            refreshing: false
        }
        this._onRefresh = this._onRefresh.bind(this);
    }
    componentDidMount() {
        this.funcGetUserid();
    }
    alertItemName = (item) => {
        alert(item.name)
    }
    funcGetUserid() {
        AsyncStorage.getItem(GLOBAL.APPUSERID).then((value) => {
            var tempValue = JSON.parse(value);
            this.setState({ tempUserId: tempValue })
            this.funcGetPesanan(tempValue);
        })
    }

    funcGetPesanan(getUserid) {
        fetch(GLOBAL.PESANAN_URL, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'user-id': getUserid
                // 'user-id':getUserid
                // 'UserCode': this.state.usercode
            },
        }).then((response) => response.json()).then((responseData) => {
            console.log('respon pesanan', responseData)
            if (responseData.status == true) {
                this.setState({
                    tempAktivitas: responseData.data
                })
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }

    _onRefresh() {
        this.setState({
            refreshing: true,
            isLoading: true,
            tempAktivitas: [],
        });
        setTimeout(() => {
            this.setState({ refreshing: false })
            this.funcGetPesanan(this.state.tempUserId)
        }, 100)
    }

    clickPesananHistory() {
        // Actions.menuutama({ getTemp: 'pesananhistory' });
        this.setState({
            tempClickHistory: '1'
        })
    }

    render() {
        if (this.state.tempClickHistory == '') {
            return (
                <View style={styles.container}>
                    <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: width / 10 }}>
                            <View>
                                <TouchableOpacity>
                                    <Icon name="arrow-left" size={30} color="white" style={{ marginTop: 40, marginLeft: 10 }} />
                                </TouchableOpacity>
                            </View>
                            <View>
                                <Text style={{ fontSize: 20, color: 'white' }}>PESANAN</Text>
                            </View>
                            <View>
                                <TouchableOpacity onPress={() => this.clickPesananHistory()}>
                                    <Image source={require('../../Assets/history.png')} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <ScrollView refreshControl={
                        <RefreshControl
                            refreshing={this.state.refreshing}
                            onRefresh={this._onRefresh}
                        />
                    }>
                        <View>
                            {
                                this.state.tempAktivitas.map((item, index) => (
                                    <View
                                        key={item.id}
                                        style={{
                                            width: width / 1,
                                            height: height / 6,
                                            marginTop: width / 20,
                                            backgroundColor: 'white',
                                            shadowColor: "#000",
                                            shadowOffset: {
                                                width: 0,
                                                height: 3,
                                            },
                                            shadowOpacity: 0.27,
                                            shadowRadius: 4.65,
                                            elevation: 6,
                                            flexDirection: 'row'
                                        }}
                                        onPress={() => this.alertItemName(item)}>
                                        <View style={{ marginLeft: width / 20 }}>
                                            <Image source={require('../../Assets/menudaycare.png')} style={{ marginTop: width / 30 }} />
                                        </View>
                                        <View>
                                            <View style={{ marginTop: width / 30, marginLeft: width / 20, width: width / 1.7 }}>
                                                <View>
                                                    <Text style={{ fontSize: width / 25, color: '#A494EB' }}>{item.daycareName}</Text>
                                                </View>
                                                {/* <View style={{marginLeft:width/20}}> */}
                                                <View>
                                                    <Text style={{ fontSize: width / 30 }}>{item.daycarePhone}</Text>
                                                </View>
                                            </View>
                                            <View style={{ width: width / 2, marginLeft: width / 20, marginTop: 5 }}>
                                                <Text style={{ fontSize: width / 30 }} numberOfLines={3}>{item.daycareAddress}</Text>
                                            </View>
                                            {/* <View style={{flexDirection:'row', marginLeft:width/20, marginTop:10}}>
                                                <TouchableOpacity style={{flexDirection:'row', backgroundColor:'#955BA5', width:width/6, height:height/35, justifyContent:'center', alignItems:'center', borderRadius:15}}>
                                                    <Image source={require('../../Assets/phone-call-putih.png')} style={{width:width/35, height:height/50}}/>
                                                    <Text style={{color:'white', marginLeft:5, fontSize:width/35}}>CALL</Text>
                                                </TouchableOpacity>
                                                <TouchableOpacity style={{marginLeft:20,flexDirection:'row', backgroundColor:'#FD8B7F', width:width/6, height:height/35, justifyContent:'center', alignItems:'center', borderRadius:15}}>
                                                    <Image source={require('../../Assets/chat-putih.png')} style={{width:width/35, height:height/60}}/>
                                                    <Text style={{color:'white', marginLeft:5, fontSize:width/35}}>CHAT</Text>
                                                </TouchableOpacity>
                                            </View> */}
                                        </View>
                                    </View>
                                ))
                            }
                        </View>
                    </ScrollView>
                </View>
            );
        } else {
            return (
                <PesananHistory />
            )
        }
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default Pesanan;
