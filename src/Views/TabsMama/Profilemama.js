//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, ScrollView, AsyncStorage, ActivityIndicator, TextInput, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Drawer, Accordion } from 'native-base';
import { Actions } from 'react-native-router-flux';
import ImagePicker from 'react-native-image-picker';
import Modal from 'react-native-modal';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const GLOBAL = require('../../Config/Services');

// create a component
class Profilemama extends Component {
    constructor() {
        super();
        this.state = {
            tempNamaLengkap: '',
            tempNamaPanggilan: '',
            tempEmail: '',
            tempPhone: '',
            tempAddress: '',
            photoBunda: false,
            statusEditabel: false,
            isVisible:false
        }
    }
    componentDidMount() {
        this.funcGetUserid();
    }
    selectImageTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,
                path: 'images'
            },
        };

        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                console.log('User cancelled video picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                var photo = {
                    uri: response.uri,
                    type: 'image/jpg',
                    name: 'Ruanganak.jpg',
                };
                console.log('liat dong', this.state.tempId)
                var uploadForm = new FormData()
                uploadForm.append('file', photo);
                this.setState({ photoBunda: true })
                fetch(GLOBAL.UPLOAD_PHOTO_URL, {
                    // fetch('http://ruanganak.id:8090/media/v1/upload/photo-profile',{
                    method: 'POST',
                    headers: {
                        'Content-Type': 'multipart/form-data',
                        // 'Authorization': '0',
                        'user-id': this.state.tempId
                    },
                    body: uploadForm
                }).then((response) => response.json()).then((responseData) => {
                    console.log('respose data kirim gambar', responseData)
                    if (responseData.status == true) {
                        this.setState({ mediaTrxCode: responseData.data.fileId, photoBunda: false });
                        // Alert.alert('Ruang Anak App', responseData.message)
                    } else {
                        Alert.alert('Ruang Anak App', responseData.message)
                    }
                }).done();
            }
        });
    }
    funcGetUserid() {
        AsyncStorage.getItem(GLOBAL.APPUSERID).then((value) => {
            var data = JSON.parse(value);
            console.log(data);
            this.funcBiodataParent(data);
            this.setState({ tempId: data });
        }).done();
    }
    funcBiodataParent(getId) {
        this.setState({ isVisible: true });
        fetch(GLOBAL.BIODATA_PARENT_URL, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': '0',
                'user-id': getId
            },
        }).then((response) => response.json()).then((responseData) => {
            console.log('profile mama', responseData);
            if (responseData.status == true) {
                this.setState({
                    isVisible: false,
                    tempNamaLengkap: responseData.data.bioParentFullname,
                    tempNamaPanggilan: '',
                    tempEmail: responseData.data.bioParentEmail,
                    tempPhone: responseData.data.bioParentPhone,
                    tempAddress: '',
                })
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    clickLogout() {
        AsyncStorage.removeItem(GLOBAL.CHECKPAGE);
        AsyncStorage.removeItem(GLOBAL.APPUSERID);
        AsyncStorage.removeItem(GLOBAL.APPUSERIDCHILD);
        Actions.login();
    }
    clickEdit() {
        // Alert.alert('Hello')
        if (this.state.statusEditabel == false) {
            this.setState({
                statusEditabel: true
            })
        } else {
            this.setState({
                statusEditabel: false
            })
        }
    }
    clickUpdate() {
        // this.setState({isVisible:true})
        fetch(GLOBAL.EDITPARENT_URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'user-id': this.state.tempId
                // 'UserCode': this.state.usercode
            },
            body: JSON.stringify({
                // "bioParentDob": "string",
                "bioParentEmail": this.state.tempEmail,
                "bioParentFullname": this.state.tempNamaLengkap,
                // "bioParentId": "string",
                // "bioParentMain": true,
                "bioParentPhone": this.state.tempPhone,
                // "bioParentType": "string",
                // "userId": "string"
            })
        }).then((response) => response.json()).then((responseData) => {
            if (responseData.status == true) {
                this.setState({ isVisible: false });
                Actions.menuutama();
                Alert.alert('Ruang Anak App', responseData.message);
            } else {
                this.setState({ isVisible: false });
                Alert.alert('Ruang Anak App', responseData.message);
            }
        }).done();
    }
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={require('../../Assets/Header.png')} style={{ width: width / 1, height: height / 3.3 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 10 }}>
                        <Text style={{ marginTop: 10, color: 'white', fontSize: width / 30, fontWeight: '800' }}>PROFILE PENGGUNA</Text>
                        <View>
                            {
                                this.state.statusEditabel == true ?
                                    <View>
                                        <View
                                            style={{
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                width: width / 5,
                                                height: width / 5,
                                                borderRadius: width / 10,
                                                backgroundColor: '#eaeaea',
                                                shadowColor: "#000",
                                                shadowOffset: {
                                                    width: 0,
                                                    height: 3,
                                                },
                                                shadowOpacity: 0.27,
                                                shadowRadius: 4.65,
                                                elevation: 6,
                                                marginTop: width / 20
                                            }}>
                                            {
                                                this.state.photoBunda == true ?
                                                    <ActivityIndicator size={25} color={"#A494EB"} /> :
                                                    <Image source={{
                                                        uri: GLOBAL.GET_PHOTO_URL,
                                                        method: 'GET',
                                                        headers: {
                                                            'Content-type': 'application/x-www-form-urlencoded',
                                                            'user-id': this.state.tempId
                                                            // 'user-id':'27e65130-d79a-4d8c-87c6-76e9b20ac2d1'
                                                        },
                                                        // cache: 'force-cache'
                                                    }} style={{ width: width / 4.9, height: height / 8.5, borderRadius: width / 10 }} />
                                            }
                                        </View>
                                        <TouchableOpacity
                                            onPress={() => this.selectImageTapped()}
                                            style={{
                                                position: 'absolute',
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                width: width / 12,
                                                height: width / 12,
                                                borderRadius: width / 10,
                                                backgroundColor: '#eaeaea',
                                                shadowColor: "#000",
                                                shadowOffset: {
                                                    width: 0,
                                                    height: 3,
                                                },
                                                shadowOpacity: 0.27,
                                                shadowRadius: 4.65,
                                                elevation: 6,
                                                top: width / 7,
                                                left: width / 7,
                                                backgroundColor: '#A494EB'
                                            }}>
                                            <Icon name="plus" color="white" size={15} />
                                        </TouchableOpacity>
                                    </View> :
                                    <View>
                                        <View
                                            // onPress={() => this.selectImageTapped()}
                                            style={{
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                                width: width / 5,
                                                height: width / 5,
                                                borderRadius: width / 10,
                                                backgroundColor: '#eaeaea',
                                                shadowColor: "#000",
                                                shadowOffset: {
                                                    width: 0,
                                                    height: 3,
                                                },
                                                shadowOpacity: 0.27,
                                                shadowRadius: 4.65,
                                                elevation: 6,
                                                marginTop: width / 20
                                            }}>
                                            {
                                                this.state.photoBunda == true ?
                                                    <ActivityIndicator size={25} color={"#A494EB"} /> :
                                                    <Image source={{
                                                        uri: GLOBAL.GET_PHOTO_URL,
                                                        method: 'GET',
                                                        headers: {
                                                            'Content-type': 'application/x-www-form-urlencoded',
                                                            'user-id': this.state.tempId
                                                            // 'user-id':'27e65130-d79a-4d8c-87c6-76e9b20ac2d1'
                                                        },
                                                        // cache: 'force-cache'
                                                    }} style={{ width: width / 4.9, height: height / 8.5, borderRadius: width / 10 }} />
                                            }
                                        </View>
                                    </View>
                            }
                        </View>
                        <TextInput style={{ marginTop: 3, color: 'white', fontSize: width / 20, fontWeight: '800' }} editable={this.state.statusEditabel} onChangeText={(text) => this.setState({ tempNamaLengkap: text })} value={this.state.tempNamaLengkap} />
                        {
                            this.state.statusEditabel == true ?
                            <View style={{justifyContent:'center', alignItems:'center', height:2, width:width/4, backgroundColor:'white'}}/>:
                            <View/>
                        }
                        <View style={{ position: 'absolute', right: width / 15, top: width / 3 }}>
                            <TouchableOpacity onPress={() => this.clickEdit()}>
                                <View>
                                    {
                                        this.state.statusEditabel == true ?
                                            <Icon name={'close'} color={'white'} size={25} /> :
                                            <Icon name={'pencil'} color={'white'} size={25} />
                                    }
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ImageBackground>
                <ScrollView style={{ width: width / 1 }} showsVerticalScrollIndicator={false}>
                    <View>
                        <View
                            style={{
                                width: width / 1,
                                height: height / 10,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                            }}>
                            <View style={{ marginTop: width / 30, marginLeft: width / 10, width:width/2 }}>
                                <Text>NAMA PANGGILAN</Text>
                                <TextInput style={{ color: '#CC2F4D' }} editable={this.state.statusEditabel} value={this.state.tempNamaPanggilan} />
                            </View>
                        </View>
                        <View
                            style={{
                                width: width / 1,
                                height: height / 10,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                            }}>
                            <View style={{ marginTop: width / 30, marginLeft: width / 10, width:width/2 }}>
                                <Text>EMAIL</Text>
                                <TextInput style={{ color: '#CC2F4D' }} editable={this.state.statusEditabel} keyboardType={'email-address'} value={this.state.tempEmail} />
                            </View>
                        </View>
                        <View
                            style={{
                                width: width / 1,
                                height: height / 10,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                            }}>
                            <View style={{ marginTop: width / 30, marginLeft: width / 10, width:width/2 }}>
                                <Text>TELEPON</Text>
                                <TextInput style={{ color: '#CC2F4D' }} editable={this.state.statusEditabel} keyboardType={'phone-pad'} value={this.state.tempPhone} />
                            </View>
                        </View>
                        <View
                            style={{
                                width: width / 1,
                                height: height / 8,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                            }}>
                            <View style={{ marginTop: width / 30, marginLeft: width / 10, width:width/2 }}>
                                <Text>ALAMAT</Text>
                                <TextInput style={{ color: '#CC2F4D', width: width / 1.5 }} editable={this.state.statusEditabel} numberOfLines={3} value={this.state.tempAddress} />
                            </View>
                        </View>
                        <TouchableOpacity
                            onPress={() => this.clickLogout()}
                            style={{
                                width: width / 1,
                                height: height / 20,
                                marginTop: width / 20,
                                backgroundColor: 'white',
                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 3,
                                },
                                shadowOpacity: 0.27,
                                shadowRadius: 4.65,
                                elevation: 6,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                            <Text>LOGOUT</Text>
                        </TouchableOpacity>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            {
                                this.state.statusEditabel == true ?
                                    <TouchableOpacity onPress={()=>this.clickUpdate()} style={{ borderRadius: 20, marginTop: 20, backgroundColor: '#A494EB', width: width / 1.2, height: height / 15, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ color: 'white', fontSize: 20, fontWeight: '700' }}>UPDATE</Text>
                                    </TouchableOpacity> :
                                    <View />
                            }
                        </View>
                        <View style={{ height: 50 }} />
                    </View>
                </ScrollView>
                {
                    this.state.isVisible == true ?
                        <Modal isVisible={this.state.isVisible}>
                            <ActivityIndicator size={25} color={"#A494EB"} />
                        </Modal>
                        :
                        <View />
                }
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
});

//make this component available to the app
export default Profilemama;
