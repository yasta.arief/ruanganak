//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ImageBackground, Dimensions, TouchableOpacity, Image, ScrollView, AsyncStorage, RefreshControl } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Drawer, Accordion } from 'native-base';
import Accounting from '../../Components/Accounting';
import { Actions } from 'react-native-router-flux';

var moment = require('moment');
const width = Dimensions.get('window').width
const height = Dimensions.get('window').height
const GLOBAL = require('../../Config/Services');

// create a component
class Tagihan extends Component {
    constructor() {
        super();
        this.state = {
            aktivitas: [
                {
                    id: 0,
                    name: 'INVOICE 0001',
                    image: require('../../Assets/menujemputasi.png'),
                    tanggal: '12 April, 11:00 AM',
                    deskripsi: 'BELUM DIBAYAR',
                    nilaiTotal: '250000',
                    bulan: 'APR',
                    tanggal: '18',
                    tahun: '2019'
                },
                {
                    id: 1,
                    name: 'INVOICE 0002',
                    image: require('../../Assets/menutracker.png'),
                    tanggal: '12 April, 11:00 AM',
                    deskripsi: 'BELUM DIBAYAR',
                    nilaiTotal: '200000',
                    bulan: 'APR',
                    tanggal: '9',
                    tahun: '2019'
                }
            ],
            tempAktivitas: [],
            tempTotalTagihan: '',
            refreshing: false
        }
        this._onRefresh = this._onRefresh.bind(this);
    }
    formatValue(value) {
        return Accounting.formatMoney(parseFloat(value), "Rp ", 0);
    }
    clickDetailPembayaran() {
        Actions.detailpembayaran();
    }
    componentDidMount() {
        this.funcGetUserid();
    }
    funcGetUserid() {
        AsyncStorage.getItem(GLOBAL.APPUSERID).then((value) => {
            var tempValue = JSON.parse(value);
            this.setState({ tempUserId: tempValue })
            this.funcGetTagihan(tempValue);
        })
    }

    funcGetTagihan(getUserid) {
        fetch(GLOBAL.INVOICE_URL, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                // 'user-id': '4a661786-dbb8-4e74-bff2-33b5c8f2e4bb'
                'user-id': getUserid
                // 'UserCode': this.state.usercode
            },
        }).then((response) => response.json()).then((responseData) => {
            console.log('respon pesanan', responseData)
            if (responseData.status == true) {
                this.setState({
                    tempTotalTagihan: responseData.data.totalInvoice,
                    tempAktivitas: responseData.data.listInvoice
                })
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    _onRefresh() {
        this.setState({
            refreshing: true,
            isLoading: true,
            data: [],
        });
        setTimeout(() => {
            this.setState({ refreshing: false })
            this.funcGetTagihan(this.state.tempUserId)
        }, 100)
    }
    render() {
        return (
            <View style={styles.container}>
                <ImageBackground source={require('../../Assets/Header.png')} style={{ width: width / 1, height: height / 3.3 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: width / 10 }}>
                        <Text style={{ marginTop: 10, color: 'white', fontSize: width / 22, fontWeight: '300' }}>TAGIHAN</Text>
                        <Text style={{ marginTop: 20, color: 'white', fontSize: width / 25, fontWeight: '300' }}>TOTAL TAGIHAN</Text>
                        <Text style={{ fontSize: width / 20, color: 'white' }}>{this.formatValue(this.state.tempTotalTagihan)}</Text>
                        <TouchableOpacity
                            onPress={() => this.clickDetailPembayaran()}
                            style={{
                                marginTop: 10,
                                justifyContent: 'center',
                                alignItems: 'center',
                                backgroundColor: '#FD8B7F',
                                width: width / 2.5,
                                height: height / 15.5,
                                borderRadius: 30
                            }}>
                            <Text style={{ color: 'white', fontSize: width / 20, fontWeight: '700' }}>BAYAR</Text>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
                <ScrollView style={{ width: width / 1 }} showsVerticalScrollIndicator={false} refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh}
                    />
                }>
                    <View>
                        {
                            this.state.tempAktivitas.map((item, index) => (
                                <TouchableOpacity
                                    onPress={() => Actions.statustransaksi({getOrderId:item.orderId})}
                                    key={item.id}
                                    style={{
                                        width: width / 1,
                                        height: height / 7,
                                        marginTop: width / 20,
                                        backgroundColor: 'white',
                                        shadowColor: "#A494EB",
                                        shadowOffset: {
                                            width: 0,
                                            height: 3,
                                        },
                                        shadowOpacity: 0.27,
                                        shadowRadius: 4.65,
                                        elevation: 6,
                                        flexDirection: 'row'
                                    }}>
                                    <View
                                        style={{
                                            marginLeft: width / 20,
                                            width: width / 7,
                                            height: height / 12,
                                            marginTop: width / 20,
                                            backgroundColor: 'white',
                                            shadowColor: "#A494EB",
                                            shadowOffset: {
                                                width: 0,
                                                height: 3,
                                            },
                                            shadowOpacity: 0.27,
                                            shadowRadius: 4.65,
                                            elevation: 6,
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                        <Text style={{ color: '#A494EB', fontSize: width / 25 }}>{moment(item.orderEnd).format('MMM')}</Text>
                                        <Text style={{ color: '#A494EB', fontSize: width / 28 }}>{moment(item.orderEnd).format('DD')}</Text>
                                        <Text style={{ color: '#A494EB', fontSize: width / 25 }}>{moment(item.orderEnd).format('YYYY')}</Text>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ marginTop: width / 15, marginLeft: width / 20 }}>
                                            <View>
                                                <Text style={{ fontSize: width / 25, color: '#A494EB' }}>{item.orderCode}</Text>
                                            </View>
                                            <View style={{ width: width / 2 }}>
                                                <Text style={{ color: '#FD8B7F' }} numberOfLines={3}>{item.orderStatus == "0" ? "BELUM DIBAYAR" : "SUDAH DIBAYAR"}</Text>
                                            </View>
                                        </View>
                                        <View style={{ marginTop: width / 12, marginRight: width / 25 }}>
                                            <Text style={{ color: '#A494EB', fontSize: width / 25 }}>{this.formatValue(item.orderPriceTotal)}</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            ))
                        }
                    </View>
                </ScrollView>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
    },
});

//make this component available to the app
export default Tagihan;
