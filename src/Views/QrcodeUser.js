//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, TextInput } from 'react-native';
import QRCode from 'react-native-qrcode';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
import UUIDGenerator from 'react-native-uuid-generator';

const width = Dimensions.get('window').width
const height = Dimensions.get('window').height

// create a component
class QrcodeUser extends Component {
    constructor() {
        super();
        this.state = {
            text: 'Arief Advani',
        }
    }
    clickBack() {
        Actions.scan();
    }

    clickTest() {
        UUIDGenerator.getRandomUUID((uuid) => {
            // console.log(uuid);
            this.setState({text:uuid})
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{ width: width / 1, height: height / 8, backgroundColor: '#A494EB' }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: width / 10 }}>
                        <View>
                            <TouchableOpacity onPress={() => this.clickBack()} style={{ marginLeft: width / 20 }}>
                                <Icon name='arrow-left' size={25} color="white" />
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Text style={{ fontSize: 20, color: 'white' }}>QRCODE</Text>
                        </View>
                        <View style={{ marginRight: width / 15 }}>
                            {/* <TouchableOpacity>
                                <Icon name={"qrcode"} size={25} color={"white"} />
                            </TouchableOpacity> */}
                        </View>
                    </View>
                </View>
                <View style={{ alignItems: 'center', marginTop:width/10 }}>
                    <QRCode
                        value={this.state.text}
                        size={width / 1.5}
                        bgColor='black'
                    // fgColor='white' 
                    />
                </View>
                <View style={{marginTop:width/10}}>
                    <Text>{this.state.text}</Text>
                    <TouchableOpacity onPress={() => this.clickTest()}>
                        <Text>Tekan saya massss</Text>
                    </TouchableOpacity>
                </View>
                {/* <View style={styles.container}>
                    <TextInput
                        style={styles.input}
                        onChangeText={(text) => this.setState({ text: text })}
                        value={this.state.text}
                    />
                    <QRCode
                        value={this.state.text}
                        size={200}
                        bgColor='purple'
                        fgColor='white' />
                </View> */}
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default QrcodeUser;
