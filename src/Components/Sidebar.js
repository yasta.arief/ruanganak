import React, { Component } from "react";
import { AppRegistry, Image, StatusBar, View, Dimensions, TouchableOpacity, AsyncStorage, Text } from "react-native";
import { Actions } from "react-native-router-flux";
import Icon from 'react-native-vector-icons/FontAwesome'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const GLOBAL = require('../Config/Services');

export default class SideBar extends Component {
    constructor() {
        super();
        this.state = {
            photoAnak: false,
            photomama: false,
            tempNameAnak: '-',
            tempNameMama: '-',
        }
    }
    componentDidMount() {
        this.funcGetUserid();
    }
    funcGetUserid() {
        AsyncStorage.getItem(GLOBAL.APPUSERID).then((value) => {
            var data = JSON.parse(value);
            this.funcUserinfo(data);
            this.funcBiodataParent(data);
            this.setState({ tempId: data });
        }).done();
    }
    funcBiodataParent(getId){
        fetch(GLOBAL.BIODATA_PARENT_URL, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization':'0',
                'user-id': getId
                // 'UserCode': this.state.usercode
            },
        }).then((response) => response.json()).then((responseData) => {
            console.log('respon coy', responseData)
            if (responseData.status == true) {
                this.setState({
                    tempFullNameParent:responseData.data.bioParentFullname,
                    tempNickNameParent:'',
                })
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    funcUserinfo(getId) {
        fetch(GLOBAL.USERINFO_URL, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': '0',
                'user-id': getId
                // 'UserCode': this.state.usercode
            },
        }).then((response) => response.json()).then((responseData) => {
            console.log('respon user menuutama', responseData)
            if (responseData.status == true) {
                if(responseData.data.famiMstBioChildren.length <= 0){
                    Actions.forminputanak();
                } else {
                    this.setState({
                        tempIdChild: responseData.data.famiMstBioChildren[0].bioChildId,
                        tempNameChild: responseData.data.famiMstBioChildren[0].bioChildNickname
                    })
                    AsyncStorage.setItem(GLOBAL.APPUSERIDCHILD, JSON.stringify(responseData.data.famiMstBioChildren[0].bioChildId))
                }
                // console.warn('cuy', responseData)
            } else {
                this.setState({ isVisible: false });
            }
        }).done();
    }
    
    clickKeluar() {
        Actions.login();
        AsyncStorage.removeItem(GLOBAL.CHECKPAGE);
    }
    clickMyreservation() {
        // Actions.myreservation();
    }
    clickMenuAnak() {
        Actions.menuanak();
    }
    clickMenuMama() {
        Actions.menuutama({ getTemp: 'profilemama' });
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#fff', width: width / 3, backgroundColor: '#A494EB' }}>
                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.clickMenuAnak()}
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginBottom: 10,
                            width: width / 5,
                            height: width / 5,
                            borderRadius: width / 10,
                            backgroundColor: '#eaeaea',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 3,
                            },
                            shadowOpacity: 0.27,
                            shadowRadius: 4.65,
                            elevation: 6,
                            marginTop: width / 8
                        }}>
                        {/* {
                            this.state.photoAnak == false ?
                                <Icon name="plus" /> : <Image source={require('../Assets/rara.png')} style={{ width: width / 4.9, height: height / 8.5, borderRadius: width / 10 }} />
                        } */}
                        <Image source={{
                            uri: GLOBAL.GET_PHOTO_URL+'/'+this.state.tempIdChild,
                            method: 'GET',
                            headers: {
                                'Content-type': 'application/x-www-form-urlencoded',
                                'user-id': this.state.tempId
                            },
                            cache: 'reload'
                        }} style={{ width: width / 4.9, height: height / 8.5, borderRadius: width / 10 }} />
                        {/* <Text>Home</Text> */}
                    </TouchableOpacity>
                    <Text style={{ marginTop: 10, color: 'white', fontSize: 15 }}>{this.state.tempNameChild}</Text>
                </View>
                <View style={{ alignItems: 'center' }}>
                    <TouchableOpacity onPress={() => this.clickMenuMama()}
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginBottom: 10,
                            width: width / 5,
                            height: width / 5,
                            borderRadius: width / 10,
                            backgroundColor: '#eaeaea',
                            shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 3,
                            },
                            shadowOpacity: 0.27,
                            shadowRadius: 4.65,
                            elevation: 6,
                            marginTop: width / 8
                        }}>
                        {/* {
                            this.state.photomama == false ?
                                <Icon name="plus" /> : <Image source={require('../Assets/rara.png')} style={{ width: width / 4.9, height: height / 8.5, borderRadius: width / 10 }} />
                        } */}
                        <Image source={{
                            uri: GLOBAL.GET_PHOTO_URL,
                            method: 'GET',
                            headers: {
                                'Content-type': 'application/x-www-form-urlencoded',
                                'user-id': this.state.tempId
                            },
                            cache: 'reload'
                        }} style={{ width: width / 4.9, height: height / 8.5, borderRadius: width / 10 }} />
                        {/* <Text>Home</Text> */}
                    </TouchableOpacity>
                    {
                        this.state.tempNickNameParent == '' ?
                        <Text style={{ marginTop: 10, color: 'white', fontSize: 15 }}>{this.state.tempFullNameParent}</Text>
                        :<Text style={{ marginTop: 10, color: 'white', fontSize: 15 }}>{this.state.tempNickNameParent}</Text>
                    }
                    {/* <Text style={{ marginTop: 10, color: 'white', fontSize: 15 }}>{this.state.tempFullNameParent}</Text> */}
                </View>
            </View>
        );
    }
}