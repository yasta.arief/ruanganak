export default class Selected_Items_Array {
    constructor() {
        selectedItemsArray = [];
    }

    pushItem(option) {
        selectedItemsArray.push(option);
    }

    getArray() {
        return selectedItemsArray;
    }
}