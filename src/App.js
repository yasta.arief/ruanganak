import React, { Component } from 'react';
// import { BackHandler, Platform, AsyncStorage } from 'react-native';
// import { Provider } from 'react-redux';
// import { createStore, applyMiddleware } from 'redux';
// import ReduxThunk from 'redux-thunk';
// import reducers from './reducers';
import {AsyncStorage} from 'react-native';
import Router from './Routes';
import { Actions } from 'react-native-router-flux';
const GLOBAL = require('./Config/Services');
checkUserLogin = () => {
    // AsyncStorage.getItem('firebaseUser').then(data => data ? JSON.parse(data) : null);
    AsyncStorage.getItem(GLOBAL.CHECKPAGE).then((value)=>{
        var data = JSON.parse(value);
        if(data == null){
            Actions.appintro();
        }else{
            Actions.menuutama();
        }
    }).done();
}
export default class App extends Component {
    render() {
        checkUserLogin();
        // const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
        return (
            // <Provider store={store}>
                <Router />
            // </Provider>
        );
    }
}